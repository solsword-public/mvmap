// cheftroid.js
//
// Game logic for a cooking-based exploration game

/* jshint esversion: 6 */
/* globals console, window, document */ 
"use strict";

// -------
// Globals
// -------

// Version number
const VERSION = "0.1";
document.getElementById("version").innerText = "v" + VERSION;
const CANVAS = document.getElementById("canvas");
const CTX = CANVAS.getContext("2d");

// Minimum zoom-out (in centimeters-per-cell)
const MIN_SCALE = 0.3;

// Maximum zoom-in (in centimeters-per-cell)
const MAX_SCALE = 4;

// Comfortable scale level
const COMFORTABLE_SCALE = 1.0;

// Speed at which to change scales (percentage of scale difference per second)
const ZOOM_IN_SPEED = 1.3;
const ZOOM_OUT_SPEED = 2.5;

// Speed at which to pan the origin (percentage of distance-to-ideal-origin per
// second)
const PAN_SPEED = 1.2;

// When does a touch-and-hold become a scroll (in milliseconds)?
var TOUCH_SCROLL_ONSET = 500;


// The current world, with the following keys:
// rooms - Dictionary mapping room IDs to room data.
var WORLD = {
    "rooms": {},
};

// Current room name & data
var CURRENT_ROOM_NAME = "";
var CURRENT_ROOM = undefined;

// ------
// Colors
// ------

// Background color for the canvas
var BACKGROUND = "#ffddee";

// Grid color
var GRID_COLOR = "#550022";

// Background color for the canvas
var AVATAR_COLOR = "#dd00aa";


// --------------------
// Conversion functions
// --------------------

// Page <-> viewport coordinates
function pgc__vc(ctx, pc) {
    return [
        (pc[0] - ctx.bounds.left) / ctx.bounds.width,
        (pc[1] - ctx.bounds.top) / ctx.bounds.height
    ];
}

function vc__pgc(ctx, vc) {
    return [
        ctx.bounds.left + ctx.bounds.width * vc[0],
        ctx.bounds.top + ctx.bounds.height * vc[1],
    ];
}

// Viewport <-> canvas coordinates
function vc__cc(ctx, vc) {
    return [
        vc[0] * ctx.cwidth,
        vc[1] * ctx.cheight
    ];
}

function cc__vc(ctx, cc) {
    return [
        cc[0] / ctx.cwidth,
        cc[1] / ctx.cheight
    ];
}

// Canvas <-> world coordinates
// Note that y-axis is flipped here so that the world coordinates are
// right-handed and y goes up/north
function cc__wc(ctx, cc) {
    return [
        ((cc[0] - ctx.cwidth/2)/ctx.cwidth) * ctx.cscale + ctx.origin[0],
        -((cc[1] - ctx.cheight/2)/ctx.cwidth) * ctx.cscale + ctx.origin[1]
            // cscale ignores canvas height
    ];
}

function wc__cc(ctx, wc) {
    return [
        ((wc[0] - ctx.origin[0]) / ctx.cscale) * ctx.cwidth + ctx.cwidth/2,
        -((wc[1] - ctx.origin[1]) / ctx.cscale) * ctx.cwidth + ctx.cheight/2
    ];
}


// Returns the length of one world-coordinate unit in canvas coordinates.
function canvasUnit(ctx) {
    return (ctx.cwidth / ctx.cscale);
}

// World <-> grid coordinates
function wc__gc(wc) {
    return [
        Math.floor(wc[0] + 0.5),
        Math.floor(wc[1] + 0.5)
    ];
}

function gc__wc(gc) {
    return [
        gc[0],
        gc[1]
    ];
}

// Page coordinates all the way to grid coordinates:
function pgc__gc(ctx, pgc) {
    return wc__gc(
        cc__wc(
            ctx,
            vc__cc(
                ctx,
                pgc__vc(ctx, pgc)
            )
        )
    );
}


// --------
// Geometry
// --------

// Returns the Euclidean distance between p1 and p2.
function dist(p1, p2) {
    let dx = p2[0] - p1[0];
    let dy = p2[1] - p1[1];
    return Math.sqrt(dx*dx + dy*dy);
}


// Catmull-Rom interpolation function using start, end, and t values
function cm_interpolate(x, y, start, end, t) {
    let span = end - start;
    if (span == 0) {
        // no distance between points, so just return the first one
        return x;
    }
    return x * (end - t) / span + y * (t - start) / span;
}


// Computes a centripetal Catmull-Rom spline position based on four
// points from a sequence of points that we'd like to interpolate, and a
// t value between 0 and 1 indicating how far we want to be between
// the second and third points of the four.
//
// Reference:
// https://en.wikipedia.org/wiki/Centripetal_Catmull%E2%80%93Rom_spline
function catmull_rom(points, t) {
    let d1 = dist(points[0], points[1]);
    let d2 = dist(points[1], points[2]);
    let d3 = dist(points[2], points[3]);
    let td = d1 + d2 + d3; // total distance

    if (td == 0) { // all points are the same, so we're not going anywhere
        return points[1];
    }

    // The interpolation positions on [0, 1] of our four points
    let t0 = 0;
    let t1 = d1 / td;
    let t2 = (d1 + d2) / td;
    let t3 = 1;

    // The interpolation value for t between the second and third points
    let tt = (d1 + t * d2) / td;

    let A1x = cm_interpolate(points[0][0], points[1][0], t0, t1, tt);
    let A1y = cm_interpolate(points[0][1], points[1][1], t0, t1, tt);
    let A2x = cm_interpolate(points[1][0], points[2][0], t1, t2, tt);
    let A2y = cm_interpolate(points[1][1], points[2][1], t1, t2, tt);
    let A3x = cm_interpolate(points[2][0], points[3][0], t2, t3, tt);
    let A3y = cm_interpolate(points[2][1], points[3][1], t2, t3, tt);

    let B1x = cm_interpolate(A1x, A2x, t0, t2, tt);
    let B1y = cm_interpolate(A1y, A2y, t0, t2, tt);
    let B2x = cm_interpolate(A2x, A3x, t1, t3, tt);
    let B2y = cm_interpolate(A2y, A3y, t1, t3, tt);

    return [
        cm_interpolate(B1x, B2x, t1, t2, tt),
        cm_interpolate(B1y, B2y, t1, t2, tt)
    ];
}


// Returns a point on a looped multi-point Catmull-Rom spline, using t as
// a continuous interpolation variable over the entire loop.
function cm_loop(points, t) {
    let n = points.length;
    let where = n * t;
    let tt = where % 1;
    let here_idx = Math.floor(where);
    let prev_idx = here_idx - 1;
    let next_idx = here_idx + 1;
    let next_next_idx = next_idx + 1;
    prev_idx = posmod(prev_idx, n);
    next_idx = posmod(next_idx, n);
    next_next_idx = posmod(next_next_idx, n);
    let prev = points[prev_idx];
    let here = points[here_idx];
    let next = points[next_idx];
    let next_next = points[next_next_idx];

    return catmull_rom([prev, here, next, next_next], tt);
}

// --------
// Viewport
// --------

// Inches <-> centimeters conversion constant
const CENTIMETERS_PER_INCH = 2.54;

// Logical "DPI" constant See:
// https://stackoverflow.com/questions/11263747/how-to-get-screens-physical-size-i-e-in-inches)
const WEB_DPI = 96;

// Updates the canvas size. Called on resize after a timeout.
function updateCanvasSize(canvas, context) {
    let bounds = canvas.getBoundingClientRect();
    let car = bounds.width / bounds.height;
    canvas.width = 800 * car;
    canvas.height = 800;
    context.cwidth = canvas.width;
    context.cheight = canvas.height;
    context.middle = [context.cwidth / 2, context.cheight / 2];
    context.bounds = bounds;
    updateScalingValue(context);
    let pgc = [ context.bounds.left, context.bounds.top ]
}


// Sets the scale for the given context (limited by the MIN_SCALE and
// MAX_SCALE values. The scale factor is expressed in
// logical-centimeters-per-cell.
function setScale(context, cellSizeInCm) {
    // Scale is in world-units-per-canvas-width
    if (cellSizeInCm < MIN_SCALE) {
        cellSizeInCm = MIN_SCALE;
    }
    if (cellSizeInCm > MAX_SCALE) {
        cellSizeInCm = MAX_SCALE;
    }
    context.cellSize = cellSizeInCm;
    updateScalingValue(context);
}


// Zooms in one step.
function zoomIn() {
    setScale(CTX, CTX.cellSize * 1/0.75);
}


// Zooms out one step.
function zoomOut() {
    setScale(CTX, CTX.cellSize * 0.75);
}


// Updates the actually-used-in-computation scale value for the given
// context based on a new canvas size or cellSize value.
function updateScalingValue(context) {
    let cmWide = context.bounds.width / WEB_DPI * CENTIMETERS_PER_INCH;
    let canvasDotsPerCm = context.cwidth / cmWide;
    let dotsPerCell = canvasDotsPerCm * context.cellSize;
    // We use cwidth here because height is ignored in favor of width
    // during scaling in cc__wc and wc__cc
    let cellsPerWidth = context.cwidth / dotsPerCell;
    context.cscale = cellsPerWidth;
}

// Sets the origin for the given context, which instantly updates to
// center that point
function setOrigin(context, origin) {
    context.origin = [...origin];
}

// Constants controlling smooth scrolling-to-target behavior
const ORIGIN_SNAP = 0.05;
const ORIGIN_GLIDE_FRACTION = 0.71;

// Sets the target origin for the given context, which we'll animate to
function setTargetOrigin(context, origin) {
    context.targetOrigin = [...origin];
}

// Number of cells within which to pan camera home instead of snapping
const NEARBY_ANIMATE_DISTANCE = 20;

// Cells to offset upon return from far away
const FAR_CUT_OFFSET = 5;

// Sets the origin to the target location, panning there smoothly if it's
// nearby or jump-cutting there if not. On a jump cut, leaves the camera
// offset by FAR_CUT_OFFSET if 'offset' is true.
function cutTo(where, offset) {
    if (offset === undefined) {
        offset = false;
    }
    let d = dist(where, CTX.origin);
    if (d > NEARBY_ANIMATE_DISTANCE) {
        let [ox, oy] = CTX.origin;
        let [wx, wy] = where;
        let near_x, near_y;
        if (offset) {
            near_x = FAR_CUT_OFFSET * (ox - wx) / d;
            near_y = FAR_CUT_OFFSET * (oy - wy) / d;
        }
        // Warp instantly nearby so we don't have a super long/choppy
        // animation for the return
        set_origin(CTX, [wx + near_x, wy + near_y]);
    } else {
        // Set target origin to animate there
        setTargetOrigin(CTX, where);
    }
}

// Resets the viewpoint to home (without the animation that cutTo
// entails, and also in a way that can be called to set up the view), and
// resets the scale to COMFORTABLE_SCALE.
// TODO: Remember zoom level?
function resetView() {
    setScale(CTX, COMFORTABLE_SCALE);
    CTX.origin = [0, 0];
    cutTo([0, 0], false);
}


// -----------
// Player Code
// -----------

// Resets player properties
function resetPlayer(ctx) {
    ctx.playerPos = [0, 0];
}


// ----------
// Level Data
// ----------

// Initiates loading of a new world
function loadWorld(url) {
    let req = new XMLHttpRequest();
    req.addEventListener(
        "load",
        function() { receiveWorld(this.responseText) }
    );
    req.open("GET", url);
    req.send();
}

// Handles world data received as text. Discards old world data and
// creates a new world from the text. Rooms are defined as 2D grids of
// characters, and are loaded from a multi-line string into a 2D array.
function receiveWorld(worldData) {
    // Create a new world, discarding the old one.
    WORLD = {
        "rooms": {}
    };
    // Parse file line by line
    for (let line of worldData.split('\n')) {
        // TODO: HERE
        console.log(line);
    }
}


// ---------------
// Event Listeners
// ---------------

// Listen for window resizes but wait until 20 ms after the last
// consecutive one to do anything.
let timerId = undefined;
window.addEventListener("resize", function() {
    if (timerId != undefined) {
        window.clearTimeout(timerId);
        timerId = undefined;
    }
    timerId = window.setTimeout(
        function () {
            timerId = undefined;
            updateCanvasSize(canvas, CTX);
        },
        20 // milliseconds
    );
});


// Handle keyboard input on window
function handleKey(evt) {
    console.log(evt);
    if (evt.key == 'w' || evt.key == "ArrowUp") {
        CTX.playerPos[1] += 1;
    } else if (evt.key == 'a' || evt.key == "ArrowLeft") {
        CTX.playerPos[0] -= 1;
    } else if (evt.key == 's' || evt.key == "ArrowDown") {
        CTX.playerPos[1] -= 1;
    } else if (evt.key == 'd' || evt.key == "ArrowRight") {
        CTX.playerPos[0] += 1;
    }
    setTargetOrigin(CTX, CTX.playerPos);
}

// Set up keyboard handler
window.addEventListener("keydown", handleKey);


// ----------------
// Drawing Routines
// ----------------

// Gets extrema of canvas in the grid. Returns an object with keys 'NW', 'NE',
// 'SW', and 'SE' for each of the four corners.
function gridExtrema(ctx) {
    return {
        'NW': pgc__gc(ctx, [ ctx.bounds.left, ctx.bounds.top ]),
        'NE': pgc__gc(ctx, [ ctx.bounds.right, ctx.bounds.top ]),
        'SW': pgc__gc(ctx, [ ctx.bounds.left, ctx.bounds.bottom ]),
        'SE': pgc__gc(ctx, [ ctx.bounds.right, ctx.bounds.bottom ]),
    };
}

// A grid box is drawn in edit mode
function drawGridBox(ctx, cc) {
    let cellSize = canvasUnit(ctx);

    ctx.strokeStyle = GRID_COLOR;
    let oldWidth = ctx.lineWidth;
    ctx.lineWidth /= 2;
    ctx.beginPath();
    ctx.moveTo(cc[0] - cellSize/2, cc[1] - cellSize/2);
    ctx.lineTo(cc[0] + cellSize/2, cc[1] - cellSize/2);
    ctx.lineTo(cc[0] + cellSize/2, cc[1] + cellSize/2);
    ctx.lineTo(cc[0] - cellSize/2, cc[1] + cellSize/2);
    ctx.closePath();
    ctx.stroke();
    ctx.lineWidth = oldWidth;
}

// Draws the visible portion of the world.
function drawWorld(ctx) {

    // Set line width:
    ctx.lineWidth = 0.05 * canvasUnit(ctx);

    // Iterate over visible (and a few invisible) cells at the base layer:
    let extrema = gridExtrema(ctx);

    for (let x = extrema['SW'][0] - 1; x <= extrema['SE'][0] + 1; ++x) {
        for (let y = extrema['SW'][1] - 1; y <= extrema['NW'][1] + 1; ++y) {
            // Grid and tile positions
            let gc = [x, y];

            // Canvas coordinates for this grid cell:
            let cc = wc__cc(ctx, gc__wc(gc));

            // Draw a box as part of a grid if we're in edit mode
            drawGridBox(ctx, cc);
        }
    }
}

// Draw the player's avatar
function drawAvatar(ctx, cc) {
    let cellSize = canvasUnit(ctx);

    ctx.strokeStyle = AVATAR_COLOR;
    ctx.beginPath();
    ctx.arc(cc[0], cc[1], cellSize * 0.3, 0, 2 * Math.PI);
    ctx.stroke();
}

// ---------
// Draw Loop
// ---------

// Self-queueing draw function
function draw() {
    CTX.fillStyle = "blue";
    CTX.strokeStyle = "black";

    // Get top left and bottom right coords w/ some margins
    let tl = vc__cc(CTX, [0.05, 0.05]);
    let br = vc__cc(CTX, [0.95, 0.95]);

    // Clear the canvas
    CTX.fillStyle = BACKGROUND;
    CTX.fillRect(0, 0, CTX.cwidth, CTX.cheight);
    CTX.rect(tl[0], tl[1], br[0], br[1]);
    CTX.fill();

    // Update our origin
    if (CTX.origin != undefined && CTX.targetOrigin != undefined) {
        let [ox, oy] = CTX.origin;
        let [tx, ty] = CTX.targetOrigin;
        if (ox != tx || oy != ty) {
            let d = dist(CTX.origin, CTX.targetOrigin);
            if (d < ORIGIN_SNAP) {
                CTX.origin = [tx, ty];
            } else {
                let vx = (ox - tx) * ORIGIN_GLIDE_FRACTION;
                let vy = (oy - ty) * ORIGIN_GLIDE_FRACTION;
                CTX.origin = [tx + vx, ty + vy];
            }
        }
    }

    // Draw the world
    drawWorld(CTX);

    // Draw the player's avatar
    drawAvatar(CTX, wc__cc(CTX, gc__wc(CTX.playerPos)));

    window.requestAnimationFrame(draw);
}


// -----
// Setup
// -----

// Set initial origin and scale
setOrigin(CTX, [0, 0]);

// Set initial canvas size & scale:
updateCanvasSize(CANVAS, CTX);

// Set up cursor + origin + scale
resetView();

// Refigure canvas size & scale:
updateCanvasSize(CANVAS, CTX);

// Reset the player
resetPlayer(CTX);

// Start our drawing loop
draw();

// Load world
loadWorld("cheftroid.wld");
