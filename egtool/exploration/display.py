"""
- Authors: Peter Mawhorter
- Consulted:
- Date: 2022-4-15
- Purpose: Code to support visualizing decision graphs and explorations.

Defines functions for graph layout and drawing for
`exploration.core.DecisionGraph` objects. See the `explorationViewer`
module for more info on how these are used. This module computes layout
positions, but actually displaying the graphs is done via HTML.

TODO: Anchor-free localization implementation?
"""

from typing import Dict, Tuple, Literal, TypeAlias, Sequence

import math

from . import base
from . import core

Position: 'TypeAlias' = Tuple[int, int]
"""
Represents an x/y coordinate pair, with unspecified units.
"""

BlockPosition: 'TypeAlias' = Tuple[int, int, int]
"""
A type alias: block positions indicate the x/y coordinates of the
north-west corner of a node, as well as its side length in grid units
(all nodes are assumed to be square).
"""

BlockLayout: 'TypeAlias' = Dict[base.DecisionID, BlockPosition]
"""
A type alias: block layouts map each decision in a particular graph to a
block position which indicates both position and size in a unit grid.
"""


def roomSize(connections: int) -> int:
    """
    For a room with the given number of connections, returns the side
    length of the smallest square which can accommodate that many
    connections. Note that outgoing/incoming reciprocal pairs to/from a
    single destination should only count as one connection, because they
    don't need more than one space on the room perimeter. Even with zero
    connections, we still return 1 as the room size.
    """
    if connections == 0:
        return 1
    return 1 + (connections - 1) // 4


def expandBlocks(layout: BlockLayout) -> None:
    """
    Modifies the given block layout by adding extra space between each
    positioned node: it triples the coordinates of each node, and then
    shifts them south and east by 1 unit each, by maintaining the nodes
    at their original sizes, TODO...
    """
    # TODO


#def blockLayoutFor(region: core.DecisionGraph) -> BlockLayout:
#    """
#    Computes a unit-grid position and size for each room in an
#    `exploration.core.DecisionGraph`, laying out the rooms as
#    non-overlapping square blocks. In many cases, connections will be
#    stretched across empty space, but no explicit space is reserved for
#    connections.
#    """
#    # TODO

GraphLayoutMethod: 'TypeAlias' = Literal["stacked", "square"]
"""
The options for layouts of a decision graph. They are:

- 'stacked': Assigns *all* nodes to position (0, 0). Use this if you want
    to generate an empty layout that you plan to modify. Doesn't require
    any attributes.
- 'square': Takes the square root of the number of decisions, then places
    them in order into a square with that side length (rounded up). This
    is a very simple but also terrible algorithm. Doesn't require any
    attributes.
- 'line': Lays out the decisions in a straight line. Doesn't require any
    attributes.
"""

def assignPositions(
    decisions: Sequence[base.DecisionID],
    attributes: Dict[base.DecisionID, dict] = None,
    method: GraphLayoutMethod = "square"
) -> Dict[base.DecisionID, Position]:
    """
    Given a sequence of decision IDs, plus optionally a dictionary
    mapping those IDs to attribute dictionaries, computes a layout for
    the decisions according to the specified method, returning a
    dictionary mapping each decision ID to its position in the layout.

    Different layout methods may required different attributes to be
    available.
    """
    if method == "stacked":
        return {d: (0, 0) for d in decisions}  #  all nodes at (0, 0)
    if method == "square":
        return assignSquarePositions(decisions)
    if method == "line":
        return assignLinePositions(decisions)
    else:
        raise ValueError(f"Invalid layout method {method!r}.")


def assignSquarePositions(
    decisions: Sequence[base.DecisionID]
) -> Dict[base.DecisionID, Position]:
    """
    Creates and returns a dictionary of positions for the given sequence
    of decisions using the 'square' layout: it arranges them into a big
    square.
    """
    result = {}
    # Figure out side length of the square that will fit them all
    side = int(math.ceil((len(decisions)**0.5)))
    # Put 'em in a square
    for i, d in enumerate(decisions):
        result[d] = (i % side, i // side)
    return result


def assignLinePositions(
    decisions: Sequence[base.DecisionID]
) -> Dict[base.DecisionID, Position]:
    """
    Creates and returns a dictionary of positions for the given sequence
    of decisions using the 'line' layout: it arranges them into a
    straight horizontal line.
    """
    result = {}
    # Put 'em in a line
    for i, d in enumerate(decisions):
        result[d] = (i, 0)
    return result


def setFinalPositions(
    exploration: core.Exploration,
    method: GraphLayoutMethod = "square"
) -> None:
    """
    Adds a "finalPositions" attribute to the given exploration which
    contains a dictionary mapping decision IDs to `Position`s. Every
    decision that ever existed over the course of the exploration is
    assigned a position.
    """
    exploration.finalPositions = assignPositions(
        exploration.allDecisions(),
        method=method
    )

def setPathPositions(exploration: core.Exploration) -> None:
    """
    Adds a "pathPositions" attribute to the given exploration which
    contains a dictionary mapping decision IDs to `Position`s. This
    includes every visited decision and all of their neighbors, but does
    NOT include decisions which were never visited and are not neighbors
    of a visited decision.
    """
    # Lay out visited decisions in a line:
    onPath = exploraiton.allVisitedDecisions()
    result = assignLinePositions(onPath)
    # Get the final graph to add neighbors from
    finalGraph = exploration.getSituation().graph
    # Track already-accounted-for neighbors
    seen = set()
    # Add neighbors to our layout
    for decision in onPath:
        # copy x of this decision
        x = result[decision]
        # Track y coordinates going down below the line
        y = -1
        try:
            neighbors = finalGraph.destinationsFrom(decision)
        except MissingDecisionError:
            continue
            # decision on path may have been merged or deleted by end of
            # exploration. We don't want to get neighbors in the step the
            # node was visited, because it's likely many of those will
            # have been explored by the time we get to the final graph,
            # and we also don't want to include any merged/deleted
            # neighbors in our graph, despite the fact we're including
            # merged or deleted path nodes.
        # TODO: Sort these by step added?
        for dID in neighbors.values():
            # We only include all neighbors which are not elsewhere on
            # the path. It's possible some of these may be confirmed, but
            # that's fine, they were not active at any step.
            if dID not in onPath:
                result[dID] = (x, y)
                y -= 1  # next node will be lower down

    exploration.pathPositions = result
