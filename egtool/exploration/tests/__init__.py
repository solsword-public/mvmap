"""
- Authors: Peter Mawhorter
- Consulted:
- Date: 2022-5-28
- Purpose: Runs tests via pytest.

Invoke using `python -m exploration.tests`.

See also `__main__.py`.
"""
