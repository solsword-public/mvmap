"""
- Authors: Peter Mawhorter
- Consulted:
- Date: 2022-5-28
- Purpose: Runs tests via pytest.

Invoke using `python -m exploration.tests`.
"""

import sys

import pytest

# Don't try to test if we're being imported (e.g. by pdoc).
if __name__ == "__main__":
    sys.exit(
        pytest.main(
            [
                "--pyargs",
                "exploration.utils",
                "exploration.base",
                "exploration.graphs",
                "exploration.commands",
                "exploration.parsing",
                "exploration.core",
                "exploration.geographic",
                "exploration.journal",
                "exploration.analysis",
                "exploration.main",
                "exploration.tests",
                "--doctest-modules"
            ]
          + sys.argv[1:]
        )
    )
