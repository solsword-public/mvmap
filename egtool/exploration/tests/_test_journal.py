"""
- Authors: Peter Mawhorter
- Consulted:
- Date: 2022-4-4
- Purpose: Tests for the journal parsing functionality.
"""

import json

from .. import journal


def test_simpleConversion():
    """
    A simple test of *simple* journal conversion.
    """
    simple = journal.convertJournal("""\
S First_Room
x Exit Second_Room Entrance
tt tag

o Onwards
E END
""")
    assert len(simple) == 3

    finalGraph = simple.currentGraph()

    assert list(finalGraph["First_Room"]) == ["Second_Room"]
    assert list(finalGraph["Second_Room"]) == [
        "First_Room",
        "Onwards",
        "_e:END"
    ]
    assert list(finalGraph.nodes) == [
        "First_Room",
        "Second_Room",
        "_u:1",
        "_e:END"
    ]
    assert len(finalGraph) == 4
    assert json.dumps(
        finalGraph.textMapObj(),
        indent=4
    ) == """\
{
    "First_Room::Exit": {
        "Second_Room::Entrance": "First_Room",
        "Second_Room::Onwards": {},
        "Second_Room::_e:END": {}
    }
}"""


def test_basicConversion():
    """
    A very simple test of journal conversion.
    """
    simple, continueFrom = journal.convertJournal("""\
[First_Room]
> Exit {tag}

[Second_Room]
< Entrance
$$ END
""")
    assert len(simple) == 3
    assert continueFrom is None

    finalGraph = simple.currentGraph()

    assert list(finalGraph["First_Room"]) == ["Second_Room"]
    assert list(finalGraph["Second_Room"]) == ["First_Room", "_e:END"]
    assert list(finalGraph.nodes) == ["First_Room", "Second_Room", "_e:END"]
    assert len(finalGraph) == 3
    assert json.dumps(
        finalGraph.textMapObj(),
        indent=4
    ) == """\
{
    "First_Room::Exit": {
        "Second_Room::Entrance": "First_Room",
        "Second_Room::_e:END": {}
    }
}"""


def test_startOfCotM():
    """
    A test for converting part of a journal from Castlevania: Circle of
    the Moon. This journal covers an experienced re-playthrough up to
    the first boss (Cerberus) and boss reward (double-jump).
    """
    expl, continueFrom = journal.convertJournal("""\
[Confrontation_Site]
< Top_Left
> Bottom {forced}

[Rubble_Tower_Base]
< Top
x< Crack (short_and_high)
# -Skeleton_Bomber-
. <Salamander>
" Literally first enemy I touched!
> Bottom_Right

[Catacombs_Entrance]
< Top_Left
x< up (height)
? mid
> First_Right

[Catacombs_Treasury]
< Left
x box (heavy)
> Left

[Catacombs_Entrance]
< First_Right
- mid
? lower
. <Mercury>
" Bone head along the way
>< Mid_Left [save]
- lower
. <dagger>
> Bot_Left

[Blockwall_Cave]
< Right
x< jump (height)
> Right

[Catacombs_Entrance]
< Bot_Left
> Bot_Right

[Green_Grates]
< Left
# -Earth_Demon-
x< jump (distance)
. <dash_boots> (/distance)
- jump
- climb
? below_right
> Upper_Right

[Catacombs_Treasury_2]
< Left
x block (crumble)
> Left

[Green_Grates]
< Upper_Right
- below_right
> Lower_Right

[Zombie_Corridor]
< Left
# -Poison_Worm- + - Zombie- + -Skeleton-
> Right

[Pinkstone_Tower]
< Left_Mid
? down
? up
? across
>< Left_Crumble [. <mp>]
@ down
> Bottom_Right

[Green_Shaft_Cave]
< Left
# -Earth_Demon-
. <ht>
x< up (tall_and_narrow)
> Left

[Pinkstone_Tower]
< Bottom_Right
- across
> Mid_Right

[Squeeze_Under]
< Right
. <hp>
> Right

[Pinkstone_Tower]
< Mid_Right
- up
? up2
> Upper_Left

[Pinkstone_Connector_Crack]
< Right
x< crack (short_and_high)
> Right

[Pinkstone_Tower]
< Upper_Left
- up2
? across2
? up3
>< Top_Left [. <mp>]
- up3
? Top
- across2
>< Top_Right [save]
> Top

[FlameMouth_Chamber]
< Bottom
? right
> Left

[Pinkstone_Tower_2]
< Entrance_Crack
. <axe>
? Right
>< Left [. <hp>]
> Right

[Pinkstone_Connector_Crack]
< Left
-> crack
> Right

[Pinkstone_Tower]
< Upper_Left
> Top

[FlameMouth_Chamber]
< Bottom
- right
? up
- right2
. <mp>
x< ledge (height)
- up
> Top_Left

[Muddy_Pinkstone]
< Right
? down
>< Top_Left [save]
- down
> Bottom_Left

[Greenstone_Double_Shaft]
< Top_Right
# -Gremlin-
>< Mid_Right [. <ht>]
? up
> Bottom_Left

[Mummies_Hallway]
< Right
# Mummy
. <holy water>
x< ledge (height)
> Right

[Greenstone_Double_Shaft]
< Bottom_Left
- up
?b Top_Left
> Mid_Left

[Hopper_Treasury]
< Right
# -Hopper-
. <mp>
> Right

[Greenstone_Double_Shaft]
< Mid_Left
> Top_Left {boss}

[Cerberus]
< Right {boss}
# -Cerberus-
>< Left [. <double> (/height|distance)]
> Right
""")
    assert len(expl) == 3
    assert continueFrom == ("Cerberus", "Right")

    firstGraph, firstPos, firstState, firstChoice = expl.situationAtStep(0)
    fifthGraph, fifthPos, fifthState, fifthChoice = expl.situationAtStep(5)
    finalGraph, finalPos, finalState, finalChoice = expl.currentSituation()

    assert len(firstGraph) == 1
    assert firstPos == "Confrontation_Site"

    assert len(fifthGraph) == 10
    assert fifthPos == "Catacombs_Entrance"

    assert len(finalGraph) == 100
    assert finalPos == "Cerberus"

# TODO: Specific case coverage tests...
