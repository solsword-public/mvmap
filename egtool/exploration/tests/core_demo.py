"""
Authors: Peter Mawhorter
Consulted:
Date: 2022-8-31
Purpose: Demos of core functionality.
"""

from .. import core


def graph_example():
    """
    Builds and returns a tiny `exploration.core.DecisionGraph`.
    """
    m = core.DecisionGraph()
    m.addDecision('a')
    m.addDecision('b')
    m.addDecision('c')

    m.addTransition('a', 'East', 'b', 'West')
    m.addTransition('a', 'Northeast', 'c')
    m.addTransition('c', 'Southeast', 'b')

    m.addUnexploredEdge('a', 'South')

    return m


def sm_demo():
    """
    Builds and returns a tiny `exploration.core.DiscreteExploration`
    representing just parts of the first two rooms of Super Metroid
    post-Ceres.
    """
    # Create first two decisions
    e = core.DiscreteExploration()
    e.start('1. on ship', ['left', 'right'])
    e.explore('right', '2. cliff', ['up cliff', 'through tunnel'], 'to ship')

    # Add requirements to cliff options we can't take yet
    g = e.currentGraph()
    g.setTransitionRequirement('2. cliff', 'up cliff', 'high_jump')
    g.setTransitionRequirement('2. cliff', 'through tunnel', 'block_breaker')

    # Add decision point before door
    e.retrace('to ship')
    e.explore('left', '3. cave entrance', ['through door'], 'to ship')

    # Add decision point for top room
    e.explore('through door', '4. cave',
              ['down blocked', 'down short', 'left', 'down pit'],
              'door to outside')

    # Set some requirements
    g = e.currentGraph()
    g.setTransitionRequirement('4. cave', 'down blocked',
                               'crawl&block_breaker')
    g.setTransitionRequirement('4. cave', 'down short', 'crawl')
    g.setTransitionRequirement('4. cave', 'left', 'block_breaker')

    # Explore down to the first ledge where a door is visible
    e.explore('down pit', '5. cave ledge', ['down', 'recessed door'], 'up')

    # Set some requirement for the door
    g = e.currentGraph()
    g.setTransitionRequirement('5. cave ledge', 'recessed door', 'crawl')

    # Only option remaining is to continue down, but we'll leave things
    # at this point for now.
    return e


if __name__ == "__main__":
    e = sm_demo()

    import networkx as nx  # type: ignore [import]

    # You'll need to install the 'matplotlib' package for this to work
    try:
        import matplotlib.pyplot as plt  # type: ignore [import]

        # This draws the graph in a new window that pops up. You have to close
        # the window to end the program.
        nx.draw(e.currentGraph())
        plt.show()
    except Exception:
        print(
            "Because matplotlib is not installed, we cannot demo drawing"
            " a graph."
        )

    # You'll need to install the 'pydot' package for this to work
    # This saves the graph in dot format to the file 'graph.dot'
    try:
        nx.nx_pydot.write_dot(e.currentGraph(), "sm_start.dot")
    except Exception:
        print("Skipped writing a DOT file; is 'pydot' installed?")
