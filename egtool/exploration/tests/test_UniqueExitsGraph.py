"""
Authors: Peter Mawhorter
Consulted:
Date: 2022-3-11
Purpose: Tests for the UniqueExitsGraph class.
"""

import pytest

import networkx as nx # type: ignore

from .. import graphs


def test_create():
    "Tests graph creation."
    _ = graphs.UniqueExitsGraph()
    assert (True)


def test_notImplemented():
    "Tests that not implemented things aren't."
    g = graphs.UniqueExitsGraph()
    with pytest.raises(NotImplementedError):
        assert (g.new_edge_key(1, 2) is not None)

    with pytest.raises(NotImplementedError):
        assert (g.reverse() is not None)


def test_addNode():
    "Tests adding a node."
    g = graphs.UniqueExitsGraph()
    g.add_node('A')
    assert (list(g) == ['A'])
    with pytest.raises(KeyError):
        assert (g['B'])


def test_edges():
    "Tests adding multiple edges between nodes."
    g = graphs.UniqueExitsGraph()
    g.add_node('A')
    g.add_node('B')
    g.add_node('C')
    g.add_edge('A', 'B', 0)
    g.add_edge('A', 'B', 1)
    g.add_edge('B', 'A', 'hi')
    assert (g['A']['B'] == {0: {}, 1: {}})
    assert (g['B']['A'] == {'hi': {}})
    with pytest.raises(KeyError):
        assert (g['A']['C'])


def test_destinations():
    "Tests getting destinations by edge name."
    g = graphs.UniqueExitsGraph()
    g.add_node('A')
    g.add_node('B')
    g.add_edge('A', 'B', 0)
    g.add_edge('A', 'B', 1)
    g.add_edge('B', 'A', 'hi')
    assert (g.destination('A', 0) == 'B')
    assert (g.destination('A', 1) == 'B')
    assert (g.destination('B', 'hi') == 'A')
    with pytest.raises(KeyError):
        assert (g.destination('A', 2))
    with pytest.raises(KeyError):
        assert (g.destination('B', 0))
    assert (g.getDestination('A', 0) == 'B')
    assert (g.getDestination('A', 1) == 'B')
    assert (g.getDestination('B', 'hi') == 'A')
    assert (g.getDestination('A', 2) is None)
    assert (g.getDestination('B', 0) is None)
    assert (g.destinationsFrom('A') == {0: 'B', 1: 'B'})
    assert (g.destinationsFrom('B') == {'hi': 'A'})


def test_clear():
    "Tests clearing a graph."
    g = graphs.UniqueExitsGraph()
    g.add_node('A')
    g.add_node('B')
    g.add_edge('A', 'B', 0)
    g.add_edge('A', 'B', 1)
    g.add_edge('B', 'A', 'hi')

    assert (len(g) == 2)
    g.clear_edges()
    assert (len(g) == 2)
    assert (g['A'] == {})
    assert (g['B'] == {})
    assert (g.getDestination('A', 0) is None)
    assert (g.getDestination('B', 'hi') is None)
    assert (g.destinationsFrom('A') == {})
    assert (g.destinationsFrom('B') == {})

    g.clear()
    assert (len(g) == 0)
    with pytest.raises(KeyError):
        assert (g['A'])
    with pytest.raises(KeyError):
        assert (g['B'])


def test_remove():
    "Tests removing edges by destination + name and by just name."
    g = graphs.UniqueExitsGraph()
    g.add_node('A')
    g.add_node('B')
    g.add_edge('A', 'B', 0)
    g.add_edge('A', 'B', 1)
    g.add_edge('A', 'B', 2)
    g.add_edge('A', 'B', 3)
    g.add_edge('B', 'A', 0)
    g.add_edge('B', 'A', 1)
    g.add_edge('B', 'A', 'hi')

    with pytest.raises(TypeError):
        g.remove_edge('A', 'B')

    with pytest.raises(nx.NetworkXError):
        g.remove_edge('A', 'B', 'hi')

    with pytest.raises(nx.NetworkXError):
        g.remove_edge('B', 'A', 2)

    assert (g.getDestination('A', 3) == 'B')
    g.remove_edge('A', 'B', 3)
    assert (g.getDestination('A', 3) is None)

    with pytest.raises(ValueError):
        assert (g.remove_edges_from([('A', 'B')]) is None)

    assert (g.getDestination('A', 2) == 'B')
    assert (g.getDestination('B', 1) == 'A')
    assert (g._byEdge['A'][2] == 'B')
    g.remove_edges_from([('A', 'B', 2), ('B', 'A', 1, {})])
    assert (g.getDestination('A', 2) is None)
    assert (g.getDestination('B', 1) is None)

    assert (g.getDestination('A', 0) == 'B')
    g.removeEdgeByKey('A', 0)
    assert (g.getDestination('A', 0) is None)

    assert (g.getDestination('B', 0) == 'A')
    g.removeEdgeByKey('B', 0)
    assert (g.getDestination('B', 0) is None)

    assert (g.getDestination('B', 'hi') == 'A')
    g.removeEdgesByKey([('B', 'hi'), ('A', 1)]) # spurious target ignored
    assert (g.getDestination('B', 'hi') is None)
