"""
- Authors: Nissi, Jada, Rachel, Kaitlyn, Kitty, Peter Mawhorter
- Consulted: Peter Mawhorter
- Date: 2022-12-5
- Purpose: Tests for exploration analysis.
"""

from .. import analysis, journal

#Part of Rachel's journal:
JOURNAL = """
S Start_room::Start
  zz Starting_region
  A gain attack
  o right
  o left
x left coin_room sr
  o up above_coin_room fall
    q _wall_kick_jumps  # TODO: '?' syntax
t sr
x right platforms rp
  o up
  o right
x up dangerplat ud
"""

BABY_JOURNAL = """
S Start
  A gain jump
  A gain attack
  n button check
  zz Wilds
  o up
    q _flight
  o left
x left left_nook right
a geo_rock
  At gain geo*15
  At deactivate
  o up
    q _tall_narrow
t right
  o right
    q attack
"""

BABY_JOURNAL_2 = """
S Start
  A gain jump
  A gain attack
  n button check
  zz Wilds
  o up
    q _flight
  o left
x left left_nook right
"""

REVISITS_JOURNAL = """
S A
x right B left
x down C up
x left D right
r up A down
t right
r down_left D up_right
t right
t up
t left
t down
"""


def test_CreateExploration() -> None:
    """
    Simple test to make sure we can create an exploration object for
    other tests in this file.
    """
    ex = journal.convertJournal(JOURNAL)
    assert len(ex) == 6
    assert ex.getActiveDecisions(0) == set()
    assert ex.getActiveDecisions(1) == {0}

    assert bool(ex.getSituation(1).graph) is True


def test_countActionsAtDecision() -> None:
    """
    Test to make sure the number of actions in the whole graph
    at each step is accurate.
    """
    ex = journal.convertJournal(BABY_JOURNAL)
    assert analysis.perSituation(
        analysis.sumOfResults(
            analysis.perDecision(
                analysis.analyzeGraph(analysis.countActionsAtDecision)
            )
        )
    )(ex) == [0, 0, 1, 1, 1]

    now = ex.getSituation()
    graph = now.graph
    startID = graph.resolveDecision("Start")
    nookID = graph.resolveDecision("left_nook")
    assert analysis.countActionsAtDecision(graph, startID) == 0
    assert analysis.countActionsAtDecision(graph, nookID) == 1
    # TODO: Further testing?


def test_describeProgress() -> None:
    """
    Tests the `describeProgress` function.
    """
    e1 = journal.convertJournal(BABY_JOURNAL)
    e2 = journal.convertJournal(BABY_JOURNAL_2)
    e3 = journal.convertJournal(REVISITS_JOURNAL)

    description = analysis.describeProgress(e1)
    assert description == """\
Start of the exploration
Start exploring domain main at 0 (Start)
  Gained capability 'attack'
  Gained capability 'jump'
At decision 0 (Start)
  In region Wilds
  There are transitions:
    left to unconfirmed
    up to unconfirmed; requires _flight
  1 note(s) at this step
Explore left from decision 0 (Start) to 2 (now Wilds::left_nook)
At decision 2 (left_nook)
  There are transitions:
    right to 0 (Start)
  There are actions:
    geo_rock
Do action geo_rock
  Gained 15 geo(s)
Take right from decision 2 (left_nook) to 0 (Start)
At decision 0 (Start)
  There are transitions:
    left to 2 (left_nook)
    right to unconfirmed; requires attack
    up to unconfirmed; requires _flight
Waiting for another action...
End of the exploration.
"""

    description2 = analysis.describeProgress(e2)
    assert description2 == """\
Start of the exploration
Start exploring domain main at 0 (Start)
  Gained capability 'attack'
  Gained capability 'jump'
At decision 0 (Start)
  In region Wilds
  There are transitions:
    left to unconfirmed
    up to unconfirmed; requires _flight
  1 note(s) at this step
Explore left from decision 0 (Start) to 2 (now Wilds::left_nook)
At decision 2 (left_nook)
  There are transitions:
    right to 0 (Start)
Waiting for another action...
End of the exploration.
"""

    description3 = analysis.describeProgress(e3)
    assert description3 == """\
Start of the exploration
Start exploring domain main at 0 (A)
At decision 0 (A)
  There are transitions:
    right to unconfirmed
Explore right from decision 0 (A) to 1 (now B)
At decision 1 (B)
  There are transitions:
    down to unconfirmed
    left to 0 (A)
Explore down from decision 1 (B) to 2 (now C)
At decision 2 (C)
  There are transitions:
    left to unconfirmed
    up to 1 (B)
Explore left from decision 2 (C) to 3 (now D)
At decision 3 (D)
  There are transitions:
    right to 2 (C)
    up to 0 (A)
Take up from decision 3 (D) to 0 (A)
At decision 0 (A)
  There are transitions:
    down to 3 (D)
    right to 1 (B)
Take right from decision 0 (A) to 1 (B)
At decision 1 (B)
  There are transitions:
    down to 2 (C)
    down_left to 3 (D)
    left to 0 (A)
Take down_left from decision 1 (B) to 3 (D)
At decision 3 (D)
  There are transitions:
    right to 2 (C)
    up to 0 (A)
    up_right to 1 (B)
Take right from decision 3 (D) to 2 (C)
At decision 2 (C)
  There are transitions:
    left to 3 (D)
    up to 1 (B)
Take up from decision 2 (C) to 1 (B)
At decision 1 (B)
  There are transitions:
    down to 2 (C)
    down_left to 3 (D)
    left to 0 (A)
Take left from decision 1 (B) to 0 (A)
At decision 0 (A)
  There are transitions:
    down to 3 (D)
    right to 1 (B)
Take down from decision 0 (A) to 3 (D)
At decision 3 (D)
  There are transitions:
    right to 2 (C)
    up to 0 (A)
    up_right to 1 (B)
Waiting for another action...
End of the exploration.
"""


def test_unexploredBranches() -> None:
    """
    Tests the `unexploredBranches` and related count functions.
    """
    ex = journal.convertJournal(JOURNAL)
    assert analysis.unexploredBranches(ex.getSituation(0).graph) == []
    g1 = ex.getSituation(1).graph
    assert g1.destinationsFrom(0) == {
        'right': 1,
        'left': 2
    }
    assert g1.nameFor(1) == '_u.0'
    assert g1.nameFor(2) == '_u.1'
    assert analysis.unexploredBranches(ex.getSituation(1).graph) == [
        (0, 'right'),
        (0, 'left'),
    ]
    assert analysis.unexploredBranches(ex.getSituation(2).graph) == [
        (0, 'right'),
        (2, 'up'),
    ]
    assert analysis.unexploredBranches(ex.getSituation(3).graph) == [
        (0, 'right'),
        (2, 'up'),
    ]
    g4 = ex.getSituation(4).graph
    assert g4.namesListing(set(g4.nodes)) == """\
  0 (Start_room::Start)
  1 (Start_room::platforms)
  2 (Start_room::coin_room)
  3 (Start_room::above_coin_room)
  4 (_u.3)
  5 (_u.4)
"""
    assert analysis.unexploredBranches(ex.getSituation(4).graph) == [
        (1, 'up'),
        (1, 'right'),
        (2, 'up'),
    ]
    assert analysis.unexploredBranches(ex.getSituation(5).graph) == [
        (1, 'right'),
        (2, 'up'),
    ]
    allPerStep = analysis.perSituation(analysis.countAllUnexploredBranches)
    traversablePerStep = analysis.perSituation(
        analysis.countTraversableUnexploredBranches
    )
    assert allPerStep(ex) == [0, 2, 2, 2, 3, 2]
    assert traversablePerStep(ex) == [0, 2, 1, 1, 2, 1]
    # TODO


def test_countBranches() -> None:
    """
    Tests the `countBranches` function.
    """
    ex = journal.convertJournal(BABY_JOURNAL)
    ex2 = journal.convertJournal(BABY_JOURNAL_2)

    # Note: as of v0.6, we can index an exploration to get a Situation,
    # and most analysis functions want Situations as input

    first = ex[0]
    second = ex[1]
    third = ex[2]
    fourth = ex[3]
    fifth = ex[4]

    ex2first = ex2[0]
    ex2second = ex2[1]
    ex2third = ex2[2]

    meanInStep = analysis.meanOfResults(
        analysis.perDecision(
            analysis.analyzeGraph(analysis.countBranches)
        )
    )
    assert first.graph.namesListing(set(first.graph.nodes)) == """\
  0 (Start)
"""
    assert meanInStep(first) == 0
    assert meanInStep(second) == 2
    assert meanInStep(third) == 1.5
    assert meanInStep(fourth) == 2
    assert meanInStep(fifth) == 2.5

    startID = fifth.graph.resolveDecision("Start")
    nookID = fifth.graph.resolveDecision("left_nook")
    assert analysis.countBranches(second.graph, startID) == 2
    assert analysis.countBranches(third.graph, startID) == 2
    assert analysis.countBranches(third.graph, nookID) == 1
    assert analysis.countBranches(fifth.graph, startID) == 3
    assert analysis.countBranches(fifth.graph, nookID) == 2

    assert meanInStep(ex2first) == 0
    assert meanInStep(ex2second) == 2
    assert meanInStep(ex2third) == 1.5


def test_countRevisits() -> None:
    """
    Tests the `countRevisits` function
    """

    babyExp = journal.convertJournal(BABY_JOURNAL)
    graph = babyExp.getSituation().graph
    startID = graph.resolveDecision("Start")
    nookID = graph.resolveDecision("left_nook")
    assert analysis.countRevisits(babyExp, startID) == 1
    assert analysis.countRevisits(babyExp, nookID) == 0
    assert analysis.countRevisits(babyExp, 28309823) == 0

    fullExp = journal.convertJournal(JOURNAL)
    graph = fullExp.getSituation().graph
    startID = graph.resolveDecision("Start")
    coinRoomID = graph.resolveDecision("coin_room")
    platformsID = graph.resolveDecision("platforms")
    dangerplatID = graph.resolveDecision("dangerplat")
    assert analysis.countRevisits(fullExp, startID) == 1
    assert analysis.countRevisits(fullExp, coinRoomID) == 0
    assert analysis.countRevisits(fullExp, platformsID) == 0
    assert analysis.countRevisits(fullExp, dangerplatID) == 0

    revExp = journal.convertJournal(REVISITS_JOURNAL)
    graph = revExp.getSituation().graph
    aID = graph.resolveDecision('A')
    bID = graph.resolveDecision('B')
    cID = graph.resolveDecision('C')
    dID = graph.resolveDecision('D')
    assert analysis.countRevisits(revExp, aID) == 2
    assert analysis.countRevisits(revExp, bID) == 2
    assert analysis.countRevisits(revExp, cID) == 1
    assert analysis.countRevisits(revExp, dID) == 2

    countAll = analysis.sumOfResults(
        analysis.perExplorationDecision(
            analysis.countRevisits
        )
    )
    assert countAll(babyExp) == 1
    assert countAll(fullExp) == 1
    assert countAll(revExp) == 7
