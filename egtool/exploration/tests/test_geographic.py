"""
Authors: Peter Mawhorter, Nissi Awosanya, Kitty Boakye
Consulted:
Date: 2023-6-9
Purpose: Tests for open-world (geographic) exploration representation.
"""

import pytest

from .. import parsing
from .. import geographic


def test_MetricSpace() -> None:
    """
    All-around test for various features of `geographic.MetricSpace`.
    """
    # TODO


@pytest.fixture
def pf():
    """
    Returns a default `parsing.ParseFormat`.
    """
    return parsing.ParseFormat()


@pytest.fixture
def chasmExample():
    """
    Uses the `example` class method to return a new instance of the
    'chasm' example graph for testing.
    """
    return geographic.FeatureGraph.example('chasm')


@pytest.fixture
def townExample():
    """
    Uses the `example` class method to return a new instance of the
    'town' example graph for testing.
    """
    return geographic.FeatureGraph.example('town')


@pytest.fixture
def intercomExample():
    """
    Uses the `example` class method to return a new instance of the
    'intercom' example graph for testing.
    """
    return geographic.FeatureGraph.example('intercom')


def test_BuildFeatureGraph(
    townExample,
    chasmExample,
    intercomExample,
    pf
) -> None:
    """
    Test basic relationships in the town, chasm, and intercom examples.
    """
    # Chasm example
    assert (
        chasmExample.listFeatures() == [
            (0, 'main', 'region'),
            (1, 'east', 'region'),
            (2, 'west', 'region'),
            (3, 'chasm', 'edge'),
            (4, 'bridge', 'path'),
            (5, 'bridgeLever', 'affordance'),
            (6, 'house', 'node'),
            (7, 'openChest', 'affordance'),
            (8, 'crossroads', 'node'),
            (9, 'windmill', 'landmark'),
            (10, 'housePath', 'path'),
            (11, 'startPath', 'path'),
            (12, 'startingGrove', 'node'),
            (13, 'bridgePath', 'path'),
            (14, 'bridgePath', 'path'),
            (15, 'house', 'node'),
            (16, 'basement', 'region'),
            (17, 'downstairs', 'region'),
            (18, 'upstairs', 'region'),
            (19, 'stairs', 'path'),
        ]
    )
    with pytest.raises(geographic.AmbiguousFeatureSpecifierError):
        chasmExample.resolveFeature('house')
    with pytest.raises(geographic.AmbiguousFeatureSpecifierError):
        chasmExample.resolveFeature('bridgePath')
    f = pf.parseFeatureSpecifier
    assert chasmExample.resolveFeature(f("east::house")) == 6
    assert chasmExample.resolveFeature(f("west::house")) == 15
    assert chasmExample.resolveFeature(f("east::bridgePath")) == 13
    assert chasmExample.resolveFeature(f("west::bridgePath")) == 14
    assert chasmExample.relations(7, 'within') == {6}
    assert chasmExample.relations(6, 'contains') == {7}
    assert chasmExample.relations(17, 'within') == {15}
    assert chasmExample.relations(17, 'touches') == {19}

    # Town example
    assert (
        townExample.listFeatures() == [
            (0, 'town', 'region'),
            (1, 'castleHill', 'region'),
            (2, 'castle', 'node'),
            (3, 'market', 'region'),
            (4, 'eastResidences', 'region'),
            (5, 'southResidences', 'region'),
            (6, 'marketSquare', 'node'),
            (7, 'ringRoad', 'path'),
            (8, 'mainRoad', 'path'),
            (9, 'wall', 'edge'),
            (10, 'outside', 'region'),
        ]
    )
    assert townExample.relations(7, 'touches') == {6}
    assert townExample.relations(8, 'touches') == {2, 6, 9}
    assert townExample.relations(7, 'within') == {0, 3, 4, 5}
    assert townExample.relations(8, 'within') == {0, 1, 3, 10}

    # Intercom example
    assert (
        intercomExample.listFeatures() == [
            (0, 'swamp', 'region'),
            (1, 'eastSwamp', 'region'),
            (2, 'westSwamp', 'region'),
            (3, 'midSwamp', 'region'),
            (4, 'house', 'node'),
            (5, 'inside', 'region'),
            (6, 'kitchen', 'region'),
            (7, 'intercom', 'affordance'),
        ]
    )
    assert intercomExample.relations(1, 'within') == {0}
    assert intercomExample.relations(4, 'within') == {2, 3}
    assert intercomExample.relations(3, 'touches') == {1, 2}
    assert intercomExample.relations(1, 'touches') == {3}
    assert intercomExample.relations(2, 'touches') == {3}
    assert intercomExample.relations(2, 'contains') == {4}
    assert intercomExample.relations(3, 'contains') == {4}
    assert intercomExample.relations(7, 'within') == {5, 6}


def test_FeatureGraph(chasmExample) -> None:
    """
    All-around test for various features of `geographic.FeatureGraph`.
    """
    # TODO


def test_FeatureDecision() -> None:
    """
    All-around test for various features of `geographic.FeatureDecision`.
    """
    # TODO


def test_FeatureAction() -> None:
    """
    All-around test for various features of `geographic.FeatureAction`.
    """
    # TODO


def test_GeographicExploration() -> None:
    """
    All-around test for various features of
    `geographic.GeographicExploration`.
    """
    # TODO
