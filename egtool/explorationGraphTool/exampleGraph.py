"""
Authors: Peter Mawhorter, Indira Ruslanova
Consulted:
Date: 2022-3-3
Purpose: Creates an example graph, available under the variable name
  example.
"""

# Import the library (note: it must be installed for this to work)
import networkx as nx  # type: ignore

# Create a graph
example = nx.MultiDiGraph()

# Add nodes
example.add_node('A')
example.add_node('B')
example.add_node('C')

# Add edges to form a triangle
example.add_edge('A', 'B')
example.add_edge('B', 'C')
example.add_edge('C', 'A')

# the rest of the example.
example.add_node('D')
example.add_node('E')
example.add_node('F')

# Add edges
example.add_edge('A', 'D')
example.add_edge('B', 'E')
example.add_edge('C', 'F')

# Add some unknown nodes
example.add_node('unknown:1')
example.add_node('unknown:2')
example.add_node('unknown:3')
example.add_node('unknown:4')

# Connect the unknown nodes
example.add_edge('unknown:1', 'A')
example.add_edge('A', 'unknown:1')
example.add_edge('unknown:2', 'B')
example.add_edge('C', 'unknown:2')
example.add_edge('unknown:3', 'E')
example.add_edge('E', 'unknown:3')
example.add_edge('unknown:4', 'F')
example.add_edge('F', 'unknown:4')
example.add_edge('unknown:4', 'B')
example.add_edge('B', 'unknown:4')


#---------------#
# Graph drawing #
#---------------#

# Only run this code if we're running this file, not importing it
if __name__ == "__main__":
    # You'll need to install the 'matplotlib' package for this to work
    import matplotlib.pyplot as plt  # type: ignore

    # This draws the graph in a new window that pops up. You have to close
    # the window to end the program.
    nx.draw(example)
    plt.show()

    # You'll need to install the 'pydot' package for this to work
    # This saves the graph in dot format to the file 'graph.dot'
    nx.nx_pydot.write_dot(example, "example.dot")
