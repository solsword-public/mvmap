"""
Authors: Peter Mawhorter, Indira Ruslanova
Consulted:
Date: 2022-2-24
Purpose: Example of how to use the networkx Python library.
"""

# TODO: Add yourself to the authors list in the docstring above!

# Import the library (note: it must be installed for this to work)
import networkx as nx

# Code adapted from the examples at:
# https://networkx.org/documentation/stable/tutorial.html

# Create a graph
graph = nx.DiGraph()

# Add nodes
graph.add_node('A')
graph.add_node('B')
graph.add_node('C')

# Add edges to form a triangle
graph.add_edge('A', 'B')
graph.add_edge('B', 'C')
graph.add_edge('C', 'A')

# TODO: Add 3 more nodes and a few more edges to connect them up with
# the rest of the graph.
graph.add_node('D')
graph.add_node('E')
graph.add_node('F')

# Add edges
graph.add_edge('A', 'D')
graph.add_edge('B', 'E')
graph.add_edge('C', 'F')

# You'll need to install the 'matplotlib' package for this to work
import matplotlib.pyplot as plt # noqa E402

# This draws the graph in a new window that pops up. You have to close
# the window to end the program.
nx.draw(graph)
plt.show()

# You'll need to install the 'pydot' package for this to work
# This saves the graph in dot format to the file 'graph.dot'
nx.nx_pydot.write_dot(graph, "example.dot")
