"""
- Authors: Peter Mawhorter
- Consulted:
- Date: 2023-7-14
- Purpose: Visualize exploration traces from the `exploration` library.

See ../README.md (or
[https://pypi.org/project/explorationViewer/](https://pypi.org/project/explorationViewer/))
for project README.
"""

__version__ = "0.1"

# Imports define what's available when you do `import explorationViewer`
from . import viewer # noqa
