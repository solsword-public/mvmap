<h1 id="help">Help</h1>
<h2 id="help:about">About</h2>
<p>This is a viewer for decision maps, which record decisions made and
places discovered during an exploration process. It has multiple tools for
visualizing the sequence of decision graphs that represents an exploration
process, including:</p>
<ul>
<li><a href="#help:overview">Overview</a>: This view shows a single decision graph, representing one step of the exploration. You can select which step to view, with the last step being the default. You can zoom in and out and pan around. The graph layout is based on force simulation, with a fixed starting layout.</li>
<li><a href="#help:focus">Focus</a>: This view shows information about a single decision, including all of its neighbors (but not the rest of the graph). Click on a neighbor to focus on that decision instead. All neighbors available at any point during the exploration are shown, with indicators for neighbors that were temporary.</li>
<li><a href="#help:path">Path</a>: This view shows each decision reached during the exploration as a continuous path, repeating entries for decisions that were revisited. Limited information about not-taken-at-that-step connections is shown, but the temporal structure is foregrounded over the spatial structure. TODO: This mode is not implemented yet.</li>
<li><a href="#help:histograms">Histograms</a>: This view provides a more abstract view of the exploration trace, showing histograms of one of several properties, like the number of transitions at each decision. It can also display two histograms simultaneously to compare them.</li>
</ul>
<p>At the top left, the controls area shows <a href="#help:controls">global controls</a> and view-specific controls (described in the help sections for each view).</p>
<p>At the top right, there is a legend that shows the styles used for the current view. See the <a href="#help:legend">legend</a> section for more details.</p>
<p>On the bottom left, the current view is displayed. In all views, you can click on nodes (or bars in the <a href="#help:histograms">histograms</a> view) to select them. Selections filter which edges are visible, and also control how the <a href="#help:focus">focus</a> and <a href="#help:path">path</a> views behave.</p>
<p>On the bottom right, the <a href="#help:listing">listing</a> displays the name of each decision, along with its assigned symbol (if any), and a few other stats about that decision.</p>

<h2 id="help:controls">Controls</h2>
<p>The global controls are:</p>
<ul>
<li>The “show” drop-down menu controls which view is displayed.</li>
<li>the “marker size” slider which controls how large each node is in the graph views.</li>
<li>The “clear selection” button allows you to deselect all currently selected nodes.</li>
<li>The “mark” menu controls which nodes are marked with a symbol in the current view and the <a href="#help:listing">listing</a>. The symbols can help you see which node is which without hovering over it, but can also be distracting.</li>
<li>The “transparency” check box controls whether node symbols are displayed using solid or transparent colors. Transparency helps the labels stand out a bit more, especially if you are going to display a visualization in grayscale.</li>
</ul>

<h2 id="help:overview">Overview</h2>
<p>The overview displays the connections among all nodes in the graph, using an automatic layout that tries to ensure most connections aren't too long but also that decisions don't get too close to each other.</p>
<p>You can drag around the nodes to adjust their position, but when you do that, the rest of the nodes will be allowed to move according to their constraints. By hovering over a node or selecting nodes (by clicking on them), you can limit which edges are shown, and hovering on a node or edge for a moment will display information about it.</p>
<p>The controls for this view are:</p>
<ul>
<li>The “layout” selector, which allows you to switch between the “strict,” “loose,” and “relaxed” layouts (see below).</li>
<li>The “reset layout” button, which puts nodes back to their original positions for the selected layout.</li>
<li>The “relax further” button, which allows the nodes to settle more under the physical simulation rules (see below).</li>
</ul>
<p>The position of the nodes is governed by two processes: the initial layout, and a physical simulation that includes several simulated forces.</p>
<h3 id="initial-layout">Initial Layout</h3>
<p>The initial layout results can be viewed without running the physical simulation at all by selecting the “strict” layout option. This layout places nodes on a triangular grid. To produce this layout, the following algorithm is used:</p>
<ol type="1">
<li>At each step, select a single decision to place on the graph, based on the order in which decisions were observed during exploration, breaking ties alphabetically by decisions name.</li>
<li>Next, place that node on the graph:
<ul>
<li>For the first step, place it at the origin (0, 0).</li>
<li>For subsequent nodes, examine all empty grid locations that are adjacent to an already-placed node: <ul>
    <li>At each location, compute the cost for that location as the sum of the link lengths of each link from this node to a node that’s already been placed.</li>
    <li>Pick the location that has the lowest total cost, and put the node there. Ties are broken by choosing locations that are closer to the origin, and double-ties are broken by choosing the edge position that was added to the list of edge positions first.</li>
</ul></li>
<li>Finally, repeat steps 1 and 2 until all nodes have been placed (including nodes not connected to the original node).</li>
</ol>
<p>This algorithm normally results in a roughly hexagonal group of nodes, even when connections aren’t thick, because it breaks ties towards the origin. The default listing sort order for this view shows the order in which nodes were added, which can help understand the layout.</p>
<p>Once the nodes are positioned in this grid, in the “loose” and “relaxed” layouts, relax those positions by simulating a few physical forces acting on the nodes in two dimensions.</p>

<h3 id="physical-simulation">Physical Simulation</h3>
<p>When the “relax” further button is clicked, when a node is dragged, or during layout construction for the “loose” and “relaxed” layouts, physical forces are simulated to position the nodes. The two main forces are:</p>
<ul>
<li>A repulsion force that pushes all nodes away from each other when they get too close. This prevents nodes from piling up and obscuring each other, but it also is the primary source of distortion that causes nodes <em>not</em> to be positioned as close to their neighbors as they would naturally be.</li>
<li>An attraction force along each link, that as long as the nodes aren’t overlapping, pulls them closer to each other, and gets stronger the farther the link is stretched.</li>
</ul>
<p>In addition to these two main forces, there are several more forces to help arrange the nodes:</p>
<ul>
<li><p>A gathering force pulls all nodes towards the origin. This ensures that disconnected nodes do not drift away from the main part of the graph. This force is fairly weak, so it does not introduce very much distortion.</p></li>
</ul>
<p>The “loose” and “relaxed” layouts apply different amounts of simulation to relax the graph. The “relax further” button can always be used to see the consequences of more simulation updates, and dragging nodes also activates the simulation system. The “loose” layout is an intermediate between the rigid grid of the “strict” layout and the more organic appearance of the “relaxed” layout.</p>

<h2 id="help:focus">Focus</h2>
<p>This view shows detailed information about all of the options at a single decision, as well as some info on how that decision changed over the course of the exploration. This focus node is in the center, and can be switched by picking from the listing on the right, or by clicking on another non-focus node (TODO: implement this). All nodes that are connected to the focus node are arrayed in a circle around the focus node, ordered by their order of appearance in the exploration, starting from 0 degrees on the right and proceeding clockwise.</p>
<p>The control area for this view displays the ID of the focus decision.</p>

<h2 id="help:path">Path</h2>
<p>The affinity plot shows the relative strength of relationship between one or more inner nodes and a set of at least two outer nodes. The outer nodes are arranged in a circle, and the inner nodes are placed within that circle according to the strength of connection they have with each outer node.</p>
<p>In fact, the position of each inner node is just a weighted average of the positions of the outer nodes, where the weights are the total weight of the edges in both directions between the inner node and each outer node.</p>
<p>In some cases this causes overlap issues, but the plots are most informative when you can see distinctions in how certain nodes are positioned within the plot. Decreasing the node size using the “marker size” slider may help in cases where there is a lot of overlap.</p>
<p>The controls for this view are:</p>
<ul>
<li>The “hide exterior links” and “hide interior links” check boxes allow you to hide certain links. “Exterior” links are links between two outside nodes, and “interior” links are links between two inside nodes. Since the focus of the affinity plot is the links between the interior and exterior nodes, hiding links within those groups often makes sense.</li>
<li>The “label edges” check box allows you to hide the numbers that show the strength of each edge (the placement of numbers also indicates which end of an edge had the higher initiated vs. received weight, although the exact breakdown is only shown in the <a href="#help:ego_network">ego network</a> or by hovering over an edge. This can be useful when things get cluttered.</li>
<li>The “update outside nodes” and “inside nodes” controls consist of four buttons that are normally disabled. When one or more nodes are selected, the first button in each group will be labeled with a ‘+’ followed by a number indicating how many nodes will be added to that group (outside or inside) when the button is clicked, and the second button will have a ‘-’, indicating how many nodes will be removed from the corresponding group. So for example, if you select a single node that’s at the edge of the graph (an outside node), the second “update outside nodes” button will display “-1”, and if you click it, that node will be removed from the outside nodes group. Because that node is already an outside node, the button to add outside notes will remain disabled, and the buttons for adding/removing inside nodes will also be unavailable (to make an outside node into an inside node, you must first remove it from the outside group and then add it to the inside group). To add nodes that aren’t already part of the plot, select them using the <a href="#help:listing">listing</a>. The “clear selection” button may be useful in this view, because even after removing nodes from the plot, they remain selected.</li>
</ul>
<p>In this view, the position of the outside node is fixed (they are sorted around the circle by total edge weight, with ties broken by initiated edge weight and then alphabetically by ID). The position of the inside nodes is determined by which outside nodes they are most strongly connected to, however, and the purpose of the affinity plot is to be able to compare these connection strengths. If there are more than three outside nodes, position on the interior is ambiguous: exactly which node on one edge of the circle pulled the node to that side should be inspected by looking at specific edges. So if two inside nodes are close to each other, that doesn’t necessarily mean that they have exactly the same pattern of relationships with the exterior nodes. However, if two nodes are placed differently, it <em>does</em> mean that they have different relationship patterns, and if there are only two or three outside nodes, then similar interior placement does imply similar relationships patterns.</p>
<h2 id="help:histograms">Histograms</h2>
<p>This view helps provide context for the edge weights used in the other views. By displaying a histogram of values (such as interactions, which is total initiated + received weight) this view gives a sense of how large or small a particular value is relative to all of the nodes in the graph. Each histogram displays a list of numbers along the x-axis, which are the distinct values present among all nodes, and for each value, there’s a bar indicating how many nodes had that much total edge weight, with a number on top indicating the precise count. So the x-axis is the combined edge weight (or # of neighbors) depending on which graph is selected, and the y-axis is the total number of nodes that have that combined edge weight (or # of neighbors).</p>
<p>This view can plot a histogram of the initiated weight, the received weight or the initiated + received weight (interactions). It can also plot a histogram of the number of neighbors each node has. In addition, you can select two histograms at once and it will plot one above the x-axis and one below so you can compare them visually (this really only makes sense for comparing among initiated, received and initiated + received graphs).</p>
<p>The width of each bar is determined by how many bars can fit in the graph area, but there is a minimum, and so if there are too many distinct values to display, you will have to scroll the graph horizontally to see them all. The height of the bars is determined such that the largest bar fills up the available vertical space, so only relative heights are relevant. When two graphs are shown, they are <em>not</em> scaled independently, so that the relative height of bars above and below the x-axis can be compared directly.</p>
<p>The controls for this view are:</p>
<ul>
<li>The “graph” and “compare” selectors, along with a check box for “compare.” The check box can be used to disable or enable comparison, and the selectors let you decide which values to plot.</li>
<li>The “collapse values” check box controls how the x-axis is laid out. If checked (the default) then the x-axis will be compressed and numbers for which there is no node that has that much total weight (or that many neighbors) are not shown. This view makes it easier to see all of the bars at once, but means that distance along the x-axis is an unreliable indicator of relative total weight. If this box is unchecked, every integer will be shown on the x-axis up to the number for the node with the largest total weight (or largest neighborhood), which will usually force the graph to be scrolled to view all bars. Numbers where there are no corresponding nodes will have no bar above them. In this view, scrolling is usually necessary, but horizontal comparisons of position are valid.</li>
</ul>
<p>In the histogram view, selection works a bit differently than in other views. You can still use the <a href="#help:listing">listing</a> to select individual nodes, and this will highlight the bar(s) that they belong to (hovering on a bar will also display the ID of all nodes associated with it, although in some cases the list may be too long to fit on the screen). However, clicking on a bar will select <em>all</em> nodes that belong to that bar, and any other bar(s) they belong to (in the comparison view). Whether all or only some nodes at a bar are selected (e.g., because they were selected using the listing), clicking on that bar deselects all of them. So if you want to know which nodes have a total outgoing weight of exactly 5, you can select the “initiated” histogram, and click on the bar above the number 5 on the x-axis.</p>

<h2 id="help:legend">Legend</h2>
<p>In the top-right of the window, there is a “Legend” panel which displays a legend for the current graph. In the <a href="#help:overview">overview</a>, <a href="#help:focus">focus</a>, and <a href="#help:path">path</a> views, this displays one style for each node group, along with the name of that group. In the <a href="#help:histogram">histogram</a> view, it displays the different styles used for each different value that can be used to plot a histogram, which is useful when comparing histograms to distinguish which is which.</p>
<p>TODO: group assignment tool in visualizer! The groups and their names are defined as part of the dataset. The default groups will be defined as follows:</p>
<ul>
<li>TODO: Groups!</li>
</ul>
<h2 id="help:listing">Listing</h2>
<p>The listing area is on the right of the screen, and contains a list of all decisions in the graph. It has controls for specifying which exploration step to focus on, and for sorting or filtering decisions, and also displays key statistics for each decision.</p>
<p>The stats displayed are the number of outgoing (→) and incoming (←) transitions.</p>
<p>The controls for the listing area are:</p>
<ul>
<li>The “sort by” drop-down menu allows you to pick a property to sort by. Each property includes various fall-backs for breaking ties, usually in terms of whatever hasn’t been considered in the initial sort. The “default” sort order depends on the current view:
<ul>
<li>For the <a href="#help:overview">overview</a> and <a href="#help:path">path</a> views, it sorts according to the order nodes were discovered during exploration.</li>
<li>For the <a href="#help:focus">focus</a> view, it sorts first by whether or not a decision is a neighbor of the current focus node, and then by step discovered.</li>
<li>For the <a href="#help:histograms">histograms</a> view the graph is sorted by the property used in the currently selected primary histogram.</li>
</ul></li>
<li>The “find” text field allows you to type part of a decision name and the listing will be filtered as you type to show only nodes that contain the fragment you’ve typed as part of their ID (ignoring case). The clear button directly after the filter input just clears what you’ve typed and resets the filter; deleting everything in the text box also effectively resets the filter.</li>
</ul>
