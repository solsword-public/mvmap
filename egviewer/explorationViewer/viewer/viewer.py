"""
This code is requested by the viewer index.html file and then run
directly in pyodide to set up and control the visualizer.

It both contains the initialization code and related support functions
for interfacing between the `exploration.viewer` module and the
javascript in the viewer page.
"""

import time

# Get start time so we can measure how long it takes to load pyodide T_T
start = time.time()

# Import pyodide stuff
import js
import pyodide_js
import micropip

js.console.log("Python setup running...")

# We'll load this module during setup & replace this None.
exploration = None

# Global variable to hold the current exploration. Accessible in
# Javascript via pyodide.globals.EXPLORATION
EXPLORATION = 7

def receiveData(dataStr):
    """
    Function to call when we are loading a new exploration. Gets the raw
    JSON string for the exploration file.
    """
    global EXPLORATION
    js.console.log("Receiving new data...")
    # Parse data
    parsed = exploration.parsing.fromJSON(dataStr)
    # If we got a DecisionGraph, make that into a 1-step exploration
    if isinstance(parsed, exploration.DecisionGraph):
        exp = exploration.DiscreteExploration.fromGraph(parsed)
    elif isinstance(parsed, exploration.DiscreteExploration):
        exp = parsed
    else:
        # TODO: Journal parsing option here?
        # Note: could check for initial '{' above... ?
        raise ValueError(
            f"Loaded a {type(parsed)} from incoming data, but we don't"
            f" know how to convert that into a DiscreteExploration."
        )

    # Load into global variable
    EXPLORATION = exp
    js.console.log("Loaded exploration object: " + str(EXPLORATION));

    # Perform setup to add extra info to exploration
    exploration.display.enrichProperties(exp)
    exploration.display.setFinalPositions(exp)
    exploration.display.setPathPositions(exp)

    # Let javascript know it has changed
    js.newExploration(exp)


async def setupExploration():
    """
    Setup function which loads initial data and gets things started.
    May take multiple *seconds* as it has to install the exploration
    module from its wheel.
    """
    # Ensure module is available throughout file when we import it
    global exploration
    js.console.log("Setting up exploration support...")
    await micropip.install("pyodide/exploration-0.7.4-py3-none-any.whl")
    js.console.log("Installed exploration module...")
    # await pyodide_js.loadPackage("exploration")
    import exploration
    elapsed = (time.time() - start)
    js.console.log("Done loading exploration module.")
    js.console.log("Python setup took " + str(elapsed) + " seconds.");
    js.doneLoading()
    js.PYODIDE_READY = True

    # Default target: example file
    dataFile = "example.exp"

    # If there's a ?t= search param, use that instead
    url = js.URL.new(js.window.location.href)
    target = url.searchParams.get('t')
    if target:
        dataFile = target

    # If inline data is defined in the file and there's no specified
    # target, use the inline data
    if not target and js.inlineData:
        js.console.log("Loading inline data by default...")
        receiveData(js.inlineData)

    else:
        # Otherwise load the default or specified target data file
        js.console.log(f"Loading data from {dataFile!r}...")

        # This is also async: fetch file & load data when it's fetched
        # TODO: Update example.exp datafile to current format!!!
        receiveData(await js.d3.text(dataFile))

# Actually call our setup function to kick things off
setupExploration()
