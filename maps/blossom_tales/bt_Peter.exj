# Macros

= jaunt where [
  p {where}
  g jaunt
]

= haveCoins howMany [
  A set coin*{howMany}
  g walletCheck
]

= grab what [
  a grab
    At { gain {what}; deactivate }
]

= toggleSwitch [
  m switch:off
  oa switch
    q sword
    e toggle switch on off
]

= seeChest [
  oa chest
    gt chest
    At deactivate
]

= seeAChest which [
  oa chest{which}
    gt chest
    At deactivate
]

= takeFromSeenChest what [
  ta chest
    At gain {what}
]

= takeFromChest what [
  > seeChest
  > takeFromSeenChest {what}
]

= takeFromASeenChest which what [
  ta chest{which}
    At gain {what}
]

= takeFromAChest which what [
  > seeAChest {which}
  > takeFromASeenChest {which} {what}
]

= shopItem what howMuch [
  oa buy{what}
    q coin*{howMuch}
    e { lose coin*{howMuch}; gain heartPiece*1; disable }
]

= talk who [
  a talkTo{who}
    gt conversation
]

= read what [
  a read{what}
    gt reading
]

= seeBoulder [
  oa boulder
    q ?passBigBoulder
]

= boss who level [
  oa fight{who}
    gt bossFight
    e [
      {
        <{level}>sum(combat, {who}){
          gain defeated{who};
          save;
          deactivate
        }{
          goto endings//death
        }
      }
    ]
]


= failBossRevert who [
  !ta fight{who}%f
  R
]

= beatBoss who [
  ta fight{who}%s
]

= enemy which [
  gd enemy{which}
]

= combat enemy level [
  m combat:unresolved
  oa fight
    e [
      {
        <{level}>sum(combat,{enemy}){
          set combat:victory; deactivate
        }{
          set combat:defeat;
          goto endings//death
        }
      }
    ]
]

= lose [
  !ta fight%f
]

= win [
  ta fight%s
]

# Set up death domain for goto effects
domain endings
@ death
@ @

# Journal

  g time: 2024-3-16:11:02
domain main
S GrandmasCabin::main
  zz BlossomCastle
  n [
    Today is a once-in-a-lifetime day for us, but we slept in. A note on
    the table hints that our grandma adopted us; our real parents
    died trying to investigate Crocus' betrayal. They didn't want us to
    become a Rose Knight. We need to head to the castle.
  ]

x leave CastleCourtyardSW::grandmasGarden enter
  gt leaveRoom
  gr enterRoom
x east centralIntersection west
  o north
  o stepsNorth
  o east
    gb zoneOut
  o south
x south pondCorner north
t north
  n at: centralIntersection
x north northPath south
  @ norhtEastYard
    o enter
      gt enterRoom
  @ @
  o east
  o north
    gb zoneOut
t south
  n at: centralIntersection
t west
  n at: grandmasGarden
w
  g scrutinize
  [
    Thought maybe there's a way around the edge here to get to the
    northWestYard, but nope.
  ]
t east
  n at: centralIntersection
t north
  n at: northPath
  o west norhtEastYard east
    qb ?passBigBoulder
x east elevatedPath west
  v south centralIntersection stepsNorth
  > talk Villager
  n [ Blacksmith makes best gear in the kingdom. ]
  o east
    gt zoneOut

x enterNorth GirlsHouse::main leave
  gt enterRoom
  gr leaveRoom
  > talk Girld
  n [
    Her dad says there's a temple somewhere with magical rock creatures.
  ]
  > takeFromChest ElissasScroll*1
  n [
    Bring it to Elissa in the chapel for a reward?
    This scroll explains how she visited her friends before sneaking out
    of the castle to go on an adventure.
  ]
  n [
    Kids Rule Adults Drool book: The adults won't let the kids play in the
    graveyard. They hear voices coming from the crypt. They'll check out
    the old tower Northwest of the graveyard instead.
  ]

t leave
  n at: CastleCourtyardSW::elevatedPath

x east CastleCourtyardS::blacksmithsYard west
  > talk Villager
  n Gasping Marshes are haunted & dangerous.
  o east
  o south

x enter Smithy::main leave
  gt enterRoom
  gr leaveRoom
  > talk Blacksmith
  n [
    We're wanted in the castle but we should come see him when we're
    done.
  ]

t leave
  n at: CastleCourtyardS::blacksmithsYard
x east northIntersection west
  o north
    gb zoneOut
  > talk KnightOfTheRose
  o east
  o south
x east shopYard west
  o enter Shop::main leave
    gt enterRoom
    gr leaveRoom
  o east
    gb zoneOut

x enter
  n at: Shop::main
  > haveCoins 4
  > shopItem HeartPiece*1 250
  > shopItem HealthPotion*1 100
  > shopItem SteelGear*1 200

t leave
  n at CastleCourtyardS::shopYard
t west
  n at: northIntersection
x south mainIntersection north
  v upWest blacksmithsYard south
  > talk Villager
  o east
    gb zoneOut
  v west CastleCourtyardSW::mainIntersection east
x south
  gb zoneOut
  e { ??(!?finishedBusinessInCastle){ bounce }{} }
  n at: CastleCourtyardS::mainIntersection
  o leapUp wallWalkWest jumpDown
    q ?jumpUpCliffs
    gt upCliff
    gr downCliff
  @ wallWalkWest
    > seeChest
    o west
      gb zoneOut
  @ @

x east CastleCourtyardSE::main west
  > seeBoulder
  o south southYard north
  > talk Villager
  > talk Vilager2
  o enter
    gt enterRoom
    gr leaveRoom
x north topPart south
  v west CastleCourtyardS::shopYard east
  o north
    gb zoneOut
t south
  n at: CastleCourtyardSE::main

x enter PostOffice::main leave
  > talk Postmaster
  n [ We got hired as a deliveryperson! ]
  a getLetterForCarmine
    At { gain letterForCarmine*1; deactivate }
  n [
    Carmine has red hair and lives just south of here.
  ]

t leave
  n at: CastleCourtyardSE::main
t south
  n at: southYard
x enter CarminesHouse::main leave
  gt enterRoom
  gr leaveRoom
  > talk Carmine
  a deliverLetter
    q letterForCarmine*1
    At { lose letterForCarmine*1; deactivate; gain deliveredCarminesLetter }

x stairsDown CarminesBasement::main stairsUp
  gt stairsDown
  gr stairsUp
  > takeFromAChest A coin*12
  > haveCoins 18
  > takeFromAChest B coin*6
  > haveCoins 24

> jaunt PostOffice::main
  a confirmCarmineDelivery
    q deliveredCarminesLetter
    At { deactivate; gain leterForJennifer }
    n [
      Jennifer lives with the druids in Golem's Haven.
    ]
  g time: 2024-3-16:12:04
  n [ Time to take a rest. ]

w
  g time: 2024-3-19:10:43
  n [
    Going to try to abstract things at a higher level from here, with 1
    decision per map tile wherever possible?
  ]

t leave
  n at: CastleCourtyardSE::main
t north
  n at: CastleCourtyardSE::topPart

x north CastleCourtyardNE::main
  > talk Villager1
  o enterHouse
    gt enterRoom
    gr leaveRoom
  o enterBouncingFlower
    gt enterRoom
    gr leaveRoom
  o topWest
  o bottomWest
  m gate:closed
  o north
    gt zoneOut
    qb gate:open

x enterBouncingFlower BouncingFlower::main leave
  > talk HoodedVillager
  > talk TavernLady
  > talk FlowerBard
  > talk HidingKnight
  > talk GraybeardVillager
  > talk YoungVillager
  > talk William
  n [ Wants a skull prop for his stories. ]

t leave
  n at: CastleCourtyardNE::main

x enterHouse FourSistersHouse::main leave
  > talk Ruby
  > talk Cerise
  > talk Auburn
  > talk Cerise
  > read ChiliChampionsSecret
  n [
    Chef Piquanto at the annual Chili festival. Secret ingredient might
    be Old Snapdragon from the Boiling Caverns.
  ]
  > talk Scarlet
  > takeFromChest HeartPiece*1

t leave
  n at: CastleCourtyardNE::main

x bottomWest CastleCourtyardCenter::main bottomEast
  v topEast CastleCourtyardNE::main topWest
  v south CastleCourtyardS::northIntersection north
  o bottomEast
    gb zoneOut
  > talk Villager
  > talk RoseKnight1
  > talk RoseKnight2
  o enterEsatHouse
    gt enterRoom
    gr leaveRoom
  o enterWestHouse
    gt enterRoom
    gr leaveRoom
  o topEast
    gb zoneOut
  > talk SleepyKnight
  > talk SuspiciousKnight
  o enterCastle
    gt enterRoom
    gr leaveRoom
  > talk DeterminedKnight
  > talk FavoriteKnight
  o wallEast
    gb zoneOut
  m eastGate:closed
  o enterEastGate
    qb eastGate:open
    gt enterRoom
    gr leaveRoom

x portalStone FastTravelMenu::menu toCastleCourtyard
  ...

t toCastleCourtyard
  n at: CastleCourtyardCenter::main
  > talk SpookedKnight
  > talk ProudKnight
  m westGate:closed
  o enterWestGate
     qb westGate:open
     gt enterRoom
     gr leaveRoom
  o wallWest
    gb zoneOut

x wallWest CastleCourtyardNW::onWall east
  > talk PowerfulKnight
  > talk LoyalKnight
  > talk BoredKnight
  > talk HungryKnight
  > takeFromChest coin*8
  > haveCoins 37
  > talk NorthKnight
  m gate:closed
  o enterTower
    qb gate:open
    gt enterRoom
    gr leaveRoom
  > talk AnotherKnight
  > talk AdventurousKnight

x south CastleCourtyardSW::onWall north
  > talk ReticentKnight
  > talk CloverFanKnight
  > talk RudeKnight

x east CastleCourtyardS::wallWalkWest west
  > talk FollowerKnight
  > takeFromSeenChest coin*8
  > haveCoins 45

t jumpDown
  n at: CastleCourtyardS::mainIntersection

> jaunt CastleCourtyardSW::northPath

x north CastleCourtyardNW::main south
  > talk PinkHairedVillager
  > talk ChildVillager
  > talk WanderlustKnight
  v topEast CastleCourtyardCenter::topWest
  v bottomEast CastleCourtyardCenter::bottomWest
  o enterHouse
    gt enterRoom
    gr leaveRoom
  > talk LeftDisciple
  > talk RightDisciple
  m gate:closed
  o north
    qb gate:open

x enterChapel CastleChapel::main leave
  gt enterRoom
  gr leaveRoom
  > talk WingsHairVillager
  > talk ElderVillager
  n [ Witches roam the land outside, but not all of them are mean. ]
  > talk RightDisciple
  n [ Dark magic is forbidden. ]
  > talk LeftDisciple
  n [ It's been a long time since Crocus shared any new spells. ]
  > talk Elissa
  n [ Her scroll case had a hole in it as she explored the world. ]
  oa returnAllScrolls
    q ElissasScroll*20
    e deactivate

t leave
  n at: CastleCourtyardNW::main

x enterHouse CourtyardNWHouse::main leave
  > talk Villager
  > takeFromChest coin*18
  > haveCoins 63
t leave
  n at: CastleCourtyardNW::main

t bottomEast
  n at: CastleCourtyardCenter::main

x enterWestHouse GamblingGabbys::main leave
  > talk GamblingGabby
  oa gambleWithGabby
    q ?finishedBusinessInCastle

t leave
  n at: CastleCourtyardCenter::main

x enterEastHouse AbandonedHouse::main leave
x fallDown AbandonedHouseBasement::main jumpUp
  qr ?jumpUpLevels
  gt downHole
  gr upHole
  > takeFromAChest A coin*12
  n has: coin*73
  > takeFromAChest B ElissasScroll*1
  > takeFromAChest C coin*18
  n has: coin*91
  > takeFromAChest D coin*9
  n has: coin*100
  > takeFromAChest E coin*18
  n has: coin*118
  > takeFromAChest F coin*13
  n has: coin*131
  A gain ascend
  F ascend ?jumpUpLevels
  n [ We can just ascend... ]
  # TODO: Notation that we could have done this all along?

t jumpUp
  n at: AbandonedHouse::main

t leave
  n at:CastleCourtyardCenter::main

x wallEast CastleCourtyardNE::onWall west
  > talk SpiritedKnight
  > talk NannyKnight
  > takeFromChest coin*11
  n has: coin*142
  > talk ShyKnight
  > talk HailKnight

x south CastleCourtyardSE::onWall north
  > talk QuestioningKnight
  > talk PreparedKnight
  > talk NosyKnight

x west CastleCourtyardS::wallWalkEast east
  > talk KnightOfTheRose
  > takeFromChest coin*14
  > has coin*156

r jumpDown CastleCourtyardS::mainIntersection leapUpEast
  qr ?jumpUpCliffs
  gt downCliff
  gr upCliff

> jaunt CastleCourtyardCenter::main

x enterCastle CastleThroneRoom::main leave
  a getKnighted
    e [ { gain sword; gain shield; deactivate } ]
    gt cutscene
    gt trigger
    n [
      We get volunteered to clear rats out of the dungeon, which is in
      the hallway left of the library.
    ]
  # TODO: Quest-from-action syntax
  > quest ClearDungeon
  > talk KingOrchid
  > talk WizardCrocus
  > talk KnightCommander
  m gate:closed
  o north
    qb gate:open
  o east
  o west

x east MessHall::Main west
  o south
  o east
  > talk GreenDisciple
  > talk Chef
  # TODO: Quest-from-action syntax
  > quest MushroomSoup
  > talk Knight1
  > talk Knight2
  > talk OrangeDisciple
  > talk Knight3
  > talk Knight4
  > talk Knight5
  > talk Knight6
  > talk BlueDisciple

x east LongEastHallway::main west
  m gate:closed
  o north
    qb gate:open
  o south
  o east
  > talk Knight1
  > talk Knight2

x east TrainingHall::main west
  > talk TrainingKnight1
  > talk TrainingKnight2
  > talk TrainingKnight3
  > talk TrainingKnight4
  > talk TrainingKnight5
  > talk TrainingKnight6
  > talk TrainingKnight7
  > talk TrainingKnight8
  > talk TrainingKnight9
  > talk TrainingKnight10
  > talk TrainingKnight11
  n [ We learn how to do jumping & charged attacks. ]
  > talk KnightCommander
  oa bountyRewards
  n [
    We need to kill more monsters to get bounty rewards here. Not sure
    how many though. Our kill count is 0 so far.
  ]
  # TODO: Syntax for this?
  F sword CastleCourtyardNE::onWall::gate:open

r leave CastleCourtyardNE::onWall enterTower
  F sword CastleCourtyardNE::main::gate:open
  n [ Time for a break. ]
  g time: 2024-3-19:11:55

w
  g time: 2024-3-19:15:37

t enterTower
  n at: TrainingHall::main

t west
  n at: LongEastHallway::main

r south CastleCourtyardNE::main north
  gt leaveRoom
  gr enterRoom

t north
  n at: LongEastHallway::main

t west
  n at: MessHall::main
  F sword CastleCourtyardCenter::main::eastGate:open

r south CastleCourtyardCenter::main enterEastGate
  F sword CastleCourtyardCenter::main::westGate:open

t enterEastGate
  n at: MessHall::main

t west
  n at: CastleThroneRoom::main

x west CastleLibrary::main esat
  v south CastleCourtyardCenter::enterWestGate
  o west
  > talk GreenDisciple
  > talk OrangeDisciple
  > talk BlueDisciple1
  > talk BlueDisciple2
  > talk BlueDisciple3
  > read CoolSwordFightingForDummies
  > read MothersIntuition
  > read AKingdomBlooms
  > read TheFirstDisciple
  > read GaspingMarshes
    n [
      Head as far east as you can and then north to reach the Boiling
      Caverns from the Gasping Marhes.
    ]
  > talk Villager
  > read TheGuardianBracelet
  > read TheGreatCastle
    n [ Use bombs to find secrets in the dungeon. ]
  > read SpellMedallions
  > read InTheKingdomYouMustStay

x west LongWstHallway::main east
  o stepsNorth
    gt stairsDown
    gr stairsUp
    n [ This should be the way to the dungeons. ]
  m gate
  o north
    qb gate:open
    gt leaveRoom
    gr enterRoom
  F sword CastleCourtyardNW::main::gate open
  v south CastleCourtyardNW::main north
  > talk Knight1
  > talk Knight2
  > talk Knight3

x west KnightsQuarters::main east
  v south CastleCourtyardNW::onWall enterTower
  F sword CastleCourtyardNW::onWall::gate:open
  > read KnightAdventurePack
  > talk Knight1
  > talk Knight2
  > talk Knight3

t east
  n at: LongWstHallway::main


x stepsNorth CastleDungeonEntrance::main stepsNorth
  zz CastleDungeon
  > enemy Rat
  > toggleSwitch
  ta switch
  o east
    qb switch:on

x east CastleDungeonChestPit::main
  > enemy Rat
  > takeFromChest HealthPotion*1
  m gate:closed
  o east
    qb gate:open
  a dragStatue
    At set gate:open
    gt hidden
    gt hinted
  g time: 2024-3-19:16:05
  n [ Time for a break. ]

w
