For each room/area note down:

1. How/where you entered.
2. What exits/items/mechanisms you see and whether they're currently
   reachable or not.
3. Each mechanism you trigger or item you pick up.
4. Any bosses or significant enemies you defeat.
5. How/where you exit.
