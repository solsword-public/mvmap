# TODO: domain settings
domain Main

# Macros
= cutsceneHappens name note [
    a {name}
      gt forced
      g cutscene
      At disable
      n [ note ]
]
= cutsceneTo where [
    p {where}
      g cutscene
]

= claimPower what [
  a claim{what}
    gt power {what}
    At gain {what}
    At deactivate
]

= seeItem type what [
  oa get{what}
    gt item {what}
    gt itemType {type}
    e gain {what}*1
    e deactivate
]

= claimItem type what [
  a get{what}
    gt item {what}
    gt itemType {type}
    At gain {what}*1
    At deactivate
]

= seeChest [
  oa openChest
    e deactivate
]

= openChest type what howmany [
  a openChest
    gt item {what}
    gt itemType {type}
    At gain {what}*{howmany}
    At deactivate
]

= levelUp level which [
  g levelUp {level}
  g levelIn {which}
]

# TODO: Challenge notation
# TODO: Room locks notation
= boss who level [
  a {who}
    gt bossFight
    gt challengeSkill bossFight
    gt challengeLevel {level}
    At gain {who}Defeated
    At disable
]

# TODO: Skill notation
= gainedLevel kind howMany [
  A gain skill{kind}*howMany
]

= enemy which [
  gd enemy:{which}
]

= friendly which [
  gd friendly:{which}
]

= checkpoint [
  oa checkpoint
    gt save
]

= setCheckpoint [
  > checkpoint
  a checkpoint
]

= fastTravelTo where decision [
  t fastTravel
    n at: menu//fastTravelMenu

  t exitTo{where}
    n at: {where}::{decision}
]

= jaunt where [
  p {where}
    g jaunt
]

= talk who [
  a talkTo{who}
    gt character:{who}
    gt conversation
]

= shopItem type what howmuch [
  oa purchase{what}
    q money*{howmuch}
    e lose money*{howmuch}
    e gain {what}*1
    gt item {what}
    gt itemType {type}
]

# Note: Must use shopItem first...
= buy what [
  a purchase{what}
]

# Instead of tracking every money picked up from enemies, I'll just
# observe it when it seems relevant.
= haveMoney howmuch [
  A set Money {howmuch}
  n has: Money*{howmuch}
]

# TODO: Challenge syntax!
= taxChallenge skill level price [
  gt challenge:{skill}:{level}
  gt failPrice:{price}
]

= rejectChallenge skill level price [
  gt challenge:{skill}:{level}
  gt failPrice:{price}
  gt rejectOnFail
]

# TODO: Challenge syntax!
= reciprocalTaxChallenge skill level price [
  gr challenge:{skill}:{level}
  gr failPrice:{price}
]

= reciprocalRejectChallenge skill level price [
  gr challenge:{skill}:{level}
  gr failPrice:{price}
  gr rejectOnFail
]

= seeSwitch [
  oa switch
    e deactivate
    e set switch:open
]

= hitSwitch [
  > seeSwitch
  a switch
]

= seeBreakableWall [
  oa breakWall
    e deactivate
    e set wall:broken
]

= breakWall [
  > seeBreakableWall
  a breakWall
]

= mysteriousTablet [
  oa readTablet
    gt mysteriousTablet
]

= note [
  oa note
    gt note
    e deactivate
]

= noted name content [
  > note
  a note
    At gain name
    nt [ content ]
]

= tagKey which [
  a tagKey
    At deactivate
    At gain {which}Key*1
]


# Start of journal
g time 2023-12-18>19:51
n [
  Playing w/ Jane & Aaron a bit.
]
n [
  A blob oozes along in The Abyss.
  Something feels missing. I don't understand but I have to go forward.
]
S main FlowerFields
  zz TheAbyss
x right claw left
  ...
  # TODO: Triggered transition
x claw clawSpot - EntranceAlley
  zz TaffyReef
  n [ Journaling is too boring for Jane & Aaron. ]
  ...

x left top right SleepingBird
  gt hidden
x down main up
  a inspectBird
  n [ It looks dead. ]
t up
  n at: SleepingBird::top

t right
  n at: EntranceAlley::clawSpot
x right fishView left
  o up
    q ?greenDiamond
x right spitterCorridors left
  > enemy Crawler
  > enemy Spitter
  > hazard Spikes
  o downRight

x right bottom left SeaHorses
  o up
    q ?diamondBooster

x right main left LucentiaFragmentChamber
  > cutsceneHappens meetFaerie
  n [
    Giant fragment of light was once part of Lucentia.
    Lucentia once ruled over Lumina, maintaining peace & order w/ light.
    But then Kradyrev broke free from their imprisonment seal.
    With immense darkness powers, Kradyrev destroyed Lumina, took her
    core, and kept it for himself.
    This fragment fell from Lucentia's throne in Polar Solium, down here
    to Lumina.
    It's been stained with Kradyrev's power.
    The corruption is affecting the environment and the nearby creatures.
    We want to help & restore the light.
    The fairy knows how to restore the fragments but can't do it alone.
    The Titan of this place is affected.
    First, we need to defeat the Titan.
    We get the name "Kimo" which means "may light protect."
    The faery's name is Wispy.
  ]
  A gain triggerSwitches
  o up ledge down
    q ?higherJump
  @ ledge
    > checkpoint
  @ @
  @ %left
    qb switch:on
    > seeSwitch
  @ @
  a switch
  @ %up
    q ?higherJump|switch:on
  @ @
x up
  n at: ledge
  a checkpoint
t down
  n at: LucentiaFragmentChamber::main
  o right
    q ?fragmentPurified
    n [ Or maybe the titan needs to be defeated? ]
t left
  n at: SeaHorses::bottom
  @ %up
    q switch:on
    > seeSwitch
  @ @
  a switch
x up leftLedge down
  o right rightLedge left
x left connectingCorridor right
  > enemy Spitter
  > enemy Crawler
  > hazard Spikes

x left bottom right FishViewBridge
  > seeSwitch
  a switch
  o breakWall
    e deactivate
    e set wall:broken
  v downLeft EntranceAlley::fishView up
    q wall:broken
    qr wall:broken&lowerSwitch:on
  > tagKey GreenDiamond
    n [ The green diamonds are collectibles, not movement devices? ]
x left leftLedge right
  q switch:on
  > setCheckpoint
  > talk Talya
  n [
    Carrying stuff on their back.
    Is trapped now that the way is blocked.
    Gate needs two keys to open it!
  ]
  oa insertKey1
    e cost GreenDiamondKey*1
    e deactivate
    e set: key1:in
  oa insertKey2
    q key1:in
    e cost GreenDiamondKey*1
    e deactivate
    e set: gate:open
  o left
    q gate:open
  a insertKey1
t right
  n at: FishViewBridge::bottom
  a breakWall
  oa lowerSwitch
    e deactivate
    e set lowerSwitch:on
  a lowerSwitch

t downLeft
  n at: EntranceAlley::fishView
  > tagKey GreenDiamond

t up
  n at: FishViewBridge::bottom
t left
  n at: FishViewBridge::leftLedge
  a insertKey2
  > cutsceneHappens TalyasThanks
    At gain map

x left main right JellyfishAlley
  > hazard Spikes
  > enemy Crawler
  > shrine
  n [ We can absorb energies from the shrine. Not sure what this does. ]
x left spikesFloor right
  > hazard Spikes
  > enemy Crawler
  n [ Wispy can control enemies!?! This is awesome! ]
  A gain Control
  A gain ControlEnergy*3
  n [
    Control costs energy to use though, which we need to absorb from
    shrines...
  ]
x left connector right
  q Control|switch:on
  e cost ControlEnergy*1
  # TODO: Effects annotation for cost only when switch not active.
  qr switch:on
  > hitSwitch
x left upperLeft downRight
  q ?higherJump|switch:on
  > setCheckpoint
  n [
    This refills health but NOT green energy.
  ]
  o left farLeftLedge right
    q ?slightlyHigherJump
  @ farLeftLedge
    o up
      q ?wallClimb|?flight
    o left leftNook right
      qb wall:broken
  @ leftNook
    > seeBreakableWall
    > mysteriousTablet
  @ @
x upRight leftSidePathUp downLeft
  > shrine
  > enemy Spitter
x right topPathMid left
  > enemy Spitter
  > needsControlBothOr ?slightlyHigherJump
  > shrine
  o right 
    > needsControlBoth
x up topNookBottom down
  > needsControlOr ?wallClimb
  o up topNook down
    q ?slightlyHigherJump|?wallClimb
  @ topNook
    > mysteriousTablet
  @ @
t down
  n at: JellyfishAlley::topPathMid
x right topPathMidLedge left
  > hazard Spikes
x up topPathRight down
  > needsControlOr ?slightlyHigherJump
  > enemy Crawler
  > hazard Spikes
  > shrine

x right onBridge left FishViewBridge
  > needsControlBoth
  > setCheckpoint
  n [ Time for a break! ]
  g time 2023-12-18>20:51
