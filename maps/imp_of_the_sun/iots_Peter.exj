# TODO: domain settings
domain Main

# Macros
= cutsceneHappens name note [
    a {name}
      gt forced
      g cutscene
      At disable
      n [ note ]
]
= cutsceneTo where [
    p {where}
      g cutscene
]

= claimPower what [
  a claim{what}
    gt power {what}
    At gain {what}
    At deactivate
]

= seeItem type what [
  oa get{what}
    gt item {what}
    gt itemType {type}
    e gain {what}*1
    e deactivate
]

= claimItem type what [
  a get{what}
    gt item {what}
    gt itemType {type}
    At gain {what}*1
    At deactivate
]

= seeChest [
  oa openChest
    e deactivate
]

= openChest type what howmany [
  a openChest
    gt item {what}
    gt itemType {type}
    At gain {what}*{howmany}
    At deactivate
]

= levelUp level which [
  g levelUp {level}
  g levelIn {which}
]

# TODO: Challenge notation
# TODO: Room locks notation
= boss who level [
  a {who}
    gt bossFight
    gt challengeSkill bossFight
    gt challengeLevel {level}
    At gain {who}Defeated
    At disable
]

# TODO: Skill notation
= gainedLevel kind howMany [
  A gain skill{kind}*howMany
]

= enemy which [
  gd enemy:{which}
]

= friendly which [
  gd friendly:{which}
]

= checkpoint [
  oa checkpoint
    gt save
]

= setCheckpoint [
  > checkpoint
  a checkpoint
]

= fastTravelTo where decision [
  t fastTravel
    n at: menu//fastTravelMenu

  t exitTo{where}
    n at: {where}::{decision}
]

= jaunt where [
  p {where}
    g jaunt
]

= talk who [
  a talkTo{who}
    gt character:{who}
    gt conversation
]

= shopItem type what howmuch [
  oa purchase{what}
    q money*{howmuch}
    e lose money*{howmuch}
    e gain {what}*1
    gt item {what}
    gt itemType {type}
]

# Note: Must use shopItem first...
= buy what [
  a purchase{what}
]

# Instead of tracking every money picked up from enemies, I'll just
# observe it when it seems relevant.
= haveMoney howmuch [
  A set Money {howmuch}
  n has: Money*{howmuch}
]

# TODO: Challenge syntax!
= taxChallenge skill level price [
  gt challenge:{skill}:{level}
  gt failPrice:{price}
]

= rejectChallenge skill level price [
  gt challenge:{skill}:{level}
  gt failPrice:{price}
  gt rejectOnFail
]

# TODO: Challenge syntax!
= reciprocalTaxChallenge skill level price [
  gr challenge:{skill}:{level}
  gr failPrice:{price}
]

= reciprocalRejectChallenge skill level price [
  gr challenge:{skill}:{level}
  gr failPrice:{price}
  gr rejectOnFail
]

= seeSwitch [
  oa switch
    e deactivate
    e set switch:open
]

= hitSwitch [
  > seeSwitch
  a switch
]

= seeBreakableWall [
  oa breakWall
    e deactivate
    e set wall:broken
]

= breakWall [
  > seeBreakableWall
  a breakWall
]

= mysteriousTablet [
  oa readTablet
    gt mysteriousTablet
]

= note [
  oa note
    gt note
    e deactivate
]

= noted name content [
  > note
  a note
    At gain name
    nt [ content ]
]

= tagKey which [
  a tagKey
    At deactivate
    At gain {which}Key*1
]

= seeLocked name [
  o {name}
    qb {name}Key*1|{name}Lock:unlocked
    eb ??!{name}Lock:unlocked{lose {name}Key*1; set {name}Lock:unlocked}{}
]

= seeKey name [
  oa takeKey{name}
    e deactivate
    e gain {name}Key*1
]

= claimKey name [
  a takeKey{name}
    At deactivate
    At gain {name}Key*1
]


# Start of journal
g time 2023-12-29>22:30
n [
  We need to stop the eternal eclipse by journeying to the four corners
  of the empire. We were sent by the sun?
]
S main VillageFields
  zz CentralRegion
  > talk Suyana
    n [ Names us "Nin." Loves collecting things. ]
    At gain SkullPhone *1
  o right
x left village right
  > talk Izhi
    n [
      Keep an eye out for Quipus.
      She knows how to read them.
    ]
    A gain Quipu-TheSun
  n [ Taking a break almost immediately to chat with Ross. ]
  ...
  g time 2023-12-29>22:35

w
  g time 2024-01-01>16:45
  > torch
  o crossCanyon
    qb ?veryFarJump
  o down
    qr ?climbPlatforms
    n [
      The platforms seem like they might let you climb up with normal
      jumps, but when you get down they're spaced slightly too far...
    ]

x down villagePit climbRight
  o right
    qb door:open
  oa hitSwitch
    q ?purplePower
  o down
  X down
  n [
    It's a death pit. T_T
  ]
  > pit

d VillageFields::village
  n [ Jumped down the pit. ]
  n [
    I fiddle around a bit more and discover I can double-jump... Like,
    just right off the bat.
  ]
  @ VillageFields::villagePit%climbRight
    q O
  @ @

t right
  n at: VillageFields::main

x right llamaField left
  n [ There's a Llama here, and a ball I can interact with. ]
  a playBall
  n [
    I can jump to get some sun sparks but I won't bother annotating those
    I don't think.
  ]
x right eastCliff left
  > torch
  o crossCanyon
    qb ?veryFarJump
x down eastCanyon upLeft
  > pit
  o left
    qb door:open
  oa hitSwitch
    q ?purplePower

d VillageFields::eastCliff
  n [ No, I can't jump across to hit the switch. ]

d VillageFields::eastCliff
  n [ Climbing up was too tricky.. ]

> jaunt VillageFields::main
  > talk Suyana

> jaunt VillageFields::village
  > talk

  n [
    I mess around with air combos and can hit the purple switches and
    then die, but there seems to be an invisible wall preventing me from
    jumping across even when I do that? WTF?!?
  ]

> jaunt VillageFields::llamaField
  > talk Quari
    n [
    ]
    At gain well:uncovered
    At gain villageMap
    n [
      Holy crap. I passed by this guy at least once, but he didn't pop
      out. Now that he popped out I can talk to him and progres...
    ]

  > buyLevel Attack 1 100

x down top up VillageTunnels
  q well:uncovered
  o downRight rightBelowTop up
    q ?warpThrough
  @ rightBelowTop
    v right VillageFields::eastCanyon left
    o down
  @ @
  o upLeft
    q ?higherJump|?wallClimb
x downLeft main upRight
  qr X
  a takeOrb
    At deactivate
    At gain bigOrb*1

/ menu
  S menu
  a getSpear
    cost bigOrb*1
    At deactivate
    At gain spear

/ main
  > torch
  > seeLocked tunnelsLeft
x right spearPuzzle left
x upRight rightBelowTop down
  At gain tunnelsLeftKey*1 *1
  qr X
t up
  n at: VillageTunnels::top
t downLeft
  n at: VillageTunnels::main
x left mainLedge right
x down lowerTunnel up
  qr ?wallClimb|?flight
  > enemy Goblin
  o left
    qb door:open
x down earthyChamber up
  gt puzzle
  n [ Requires some timed switch stuff but it's not worth annotating. ]
  qr X
  At gain bigOrb*1 *1
  > torch
  > enemy Goblin

/ menu
  a getSlide
    q spear&bigOrb*1
    At lose bigOrb*1
    At deactivate
    At gain slide
  oa upgradeAir1
    q slide&bigOrb*1
    At deactivate
    At gain airAttack1
  oa upgradeAir2
    q airAttack1&bigOrb*1
    At lose bigOrb*1
    At deactivate
    At gain airAttack2
  oa upgradeGround1
    q slide&bigOrb*1
    At deactivate
    At gain groundAttack1
  oa upgradeGround2
    q groundAttack1&bigOrb*1
    At lose bigOrb*1
    At deactivate
    At gain groundAttack2

/ main
  n at: VillageTunnels::earthyChamber
  o right
    qb ?breakBlueBarrier
x left bigDoor right
  qb slide
  a switch
    At deactivate
    At set villageBridges:deployed
  @ VillageFields::village%crossCanyon
    qb villageBridges:deployed
  @ @
  @ VillageFields::eastCliff%crossCanyon
    qb villageBridges:deployed
  @ @
  o enterDoor
    q stoneDoor:open
  o down
  o up
    q ?flight
  o upLeft
x upLeft lowerTunnelWest downLeft
  v right lowerTunnel left
  @ %right
    qb villageBridges:deployed
  @ @
  o up
r fallDown bigDoor up
  n [ Get some fire for this fall. ]
x down blueFloorTunnel up
  o left
    qb ?destroyInnerFireBlock
  > torch
  o right
    qb ?breakBlueBarrier
t up
  n at: VillageTunnels::bigDoor
t upLeft
  n at: VillageTunnels::lowerTunnelWest
x up figureLedge down
  > collect GoldFigure
x up upperLeft down
  @ VillageFields::villagePit%right
    qb villageBridges:deployed
  @ @
  @ VillageFields::eastCanyon%left
    qb villageBridges:deployed
  @ @
r left VillageFields::villagePit right
t right
  n at: VillageTunnels::upperLeft
  o upLeft
    q X
  o upRight
    q X
    n [ Some switch I can't see for this one... ]
r fallRight top upLeft
t up
  n at: VillageFields::llamaField
  > talk Quari
  > buyLevel Health 1 250
  > haveOrbs 179
t left
  n at: VillageFields::main
  > talk Suyana
  a tradeGoldenFigure
    cost GoldFigure*1
    At gain orb*500
  > haveOrbs 679
  > buyLevel Attack 2 600

t left
  n at: VillageFields::village

x crossCanyon eastSide crossCanyon WestFields
  > pit
  > enemy Goblin
x left puzzleTop right
  o downRight
    qb bigWood:open
  > torch
  o down
    qr X
  > seeLocked enterWestShrine
x down leftRightPuzzle up
x right leverNook left
  gt puzzle
  a pullLever
    At deactivate
    At set bigWood:open
x right underBigWood left
  qb bigWood:open
  v up puzzleTop downRight
  a grabKey
    gt puzzle
    At deactivate
    At gain enterWestShrineKey*1
    n [
      Says the Shrine of the West's key... Want to test if they're
      specific...
    ]

> jaunt VillageFields::eastCliff
x crossCanyon main crossCanyon EastFields
  o fallWest
  o downMid
  o fallEast
  > enemy Goblin
  > seeLocked enterEastShrine
x fallEast eastLedge flyUp
  qr X
  o left keyNook right
    qb keyNook:opened
  > pit
  @ keyNook
    > seeKey enterEastShrine
  @ @

d EastFields::main
x downMid upperMid up
  o upRight keyNook down
    q X
  o down leverNook up
    qb keyNook:opened
  > pit
x left underWest right
  > pit
  qr ?slightlyHigherJump
  > torch
  o rideUnder leverNook rideUnder

d upperMid
x jumpToLedge leverNook flyUp
  qr ?flight
  a hitLever
    At deactivate
    At set keyNook:opened
t up
  n at: upperMid
t up
  n at: main
t fallEast
  n at: eastLedge
t left
  n at: keyNook
  > claimKey enterEastShrine
t down
  n at: upperMid
t up
  n at: main

x enterEastShrine entrance left EastShrine
  o right
  o up
  > haveOrbs 608
  > Quari
  > buyLevel InnerFire 1 200
  n [ Time for a break. ]
  g time 2024-01-01>18:18

w
