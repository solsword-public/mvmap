# Macros

= haveMoney n [
  A set money*{n}
  n has: money*{n}
]

= jaunt where [
  p {where}
  g jaunt
]

= bossDoor name [
  o {name}
    gt bossDoor
]

= boss who level [
  oa fight{who}
    gt bossFight
    e [
      {
        <{level}>sum(combat, {who}){
          gain defeated{who};
          save;
          deactivate
        }{
          goto endings//death
        }
      }
    ]
]


= failBossRevert who [
  !ta fight{who}%f
  R
]

= beatBoss who [
    ta {who}%s
    # This means that further 'At's after > beatBoss will correctly only
    # be available if you beat the boss:
    a claimReward
      q {who}Defeated
      At deactivate
      gt trigger
]

= useSavePoint [
  a save
    At save
]

= enemy which [
  gd enemy{which}
]

= seeSwitch [
  m switch:unhit
  oa hitSwitch
    q Apino
    At { set switch:hit; deactivate }
]

= switch [
  m switch:unhit
  a hitSwitch
    q Apino
    At { set switch:hit; deactivate }
]

= namedSwitch name [
  m switch{name}:unhit
  a hitSwitch{name}
    q Apino
    At { set switch{name}:hit; deactivate }
]

= seeNamedSwitch name [
  m switch{name}:unhit
  oa hitSwitch{name}
    q Apino
    e { set switch{name}:hit; deactivate }
]

= combat enemy level [
  m combat:unresolved
  oa fight
    e [
      {
        <{level}>sum(combat,{enemy}){
          set combat:victory; deactivate
        }{
          set combat:defeat;
          goto endings//death
        }
      }
    ]
]

= lose [
  !ta fight%f
]

= win [
  ta fight%s
]

= grab what [
  a grab
    At { gain {what}; deactivate }
]

= seeItem [
  oa grab
    At deactivate
]

= quest which [
  g quest{which} active
  At gain quest{which}Progress*1
]

= questProgress which [
  g quest{which} advanced
  At gain quest{which}Progress*1
]

= questComplete which [
  g quest{which} complete
  At gain quest{which}Complete
]

= upgradeMenu which [
  # TODO: Should this be a v?
  o menu{which} {which}UpgradeMenu::menu backTo{__zone__}
    e gain cameFrom{__zone__}
    qr cameFrom{__zone__}
    er lose cameFrom{__zone__}
  t menu{which}
    n at: {which}UpgradeMenu::menu
    gd menu
]

= leaveMenu zone [
  t backTo{zone}
    n at: {zone}::main
]

= logVisits [
  a visit
    e [ { gain visited{__zone__}; deactivate } ]
    gt trigger
]

= hazardHere which [
  # Using oa means we don't trigger it immediately but we will if a
  # second or subsequent action ends up in this position. This makes
  # sense (although is not precise) because hazards have a timer before
  # they actually hurts us.
  oa {which}hazard
    q !?{which}Resist
    e lose health*30
    gt trigger
]

# Note: we can't add '!hazard:suppressed' to the normal hazard macro,
# because mechanism search would find distant mechanisms and read their
# state. But using this one in a zone where a 'hazard' mechanism has been
# set up is safe.
= suppressibleHazardHere which [
  oa {which}hazard
    q !?{which}Resist&!{which}:suppressed
    e lose health*30
    gt trigger
]

# Set up death domain for goto effects
domain endings
@ death
@ @

# Journal

  g time: 2024-3-21:14:07
domain main
S StartingHallway::main
  zz Home
  n [ We've locked ourselves out of our house. ]
  o left OurHouse::rightSide right
    qb blueKey
  @ OurHouse::rightSide
    o down
    x up attic down
    x downLeft leftSide up
    o left
  @ @
  o right
    qb swim
  o downRight
    qb light
x aroundLeft OurHouse::rightSide down

x up
  n at: OurHouse::attic
  n [
    Tutorial shows map scroll keys; we can see our ship to the right.
  ]

x downLeft
  n at: OurHouse::leftSide

x left OurYard::main right
  n [ There's grass here but it doesn't require a special tool. ]
  gd grass
  > grab torch
  F torch light

> jaunt StartingHallway::main

x downRight SecondHallway::main underLeft
  v left StartingHallway::main right
x right launchPad left
  qb torch

x blastOff StartingQuadrant::main -
  gt blastOff
  zz Galaxy
  n [
    We blast off in our ship, but there's no option to return to land
    back at home.
  ]
  n [
    We meet a traveler whose space ship needs a tyre, maybe we can find a
    spare?
  ]
  o rockyPlanet
  o welcomingPlanet
  n [
    We also see a third planet or other celestial body that we can't
    quite access.
  ]

x rockyPlanet RP_RockyLanding::main blastOff
  zz RockyPlanet
  gt landing
  gr blastOff
  o right
  o topLeft
  o bottomLeft
  o downRight
  o downLeft
    qb blueKey
x topLeft upperLeftHallway right
  > grab glasses
  n [ Increases vision radius. ]

x south RP_LeftJunction::main upLeft
  o up RP_InnerUpperLeftHallway::main down
  @ RP_InnerUpperLeftHallway::main
    v topRight RP_RockyLanding::main bottomLeft
  @ @
  o down
  o right
    qb yellowKey

x up
  n at: RP_InnerUpperLeftHallway::main
  o downRight
    qb swim

t down
  n at: RP_LeftJunction::main

x down RP_BottomV::main upLeft
  o right
    qb blueKey

> jaunt RP_RockyLanding::main
x downRight deadEnd up
t up
  n at: RP_RockyLanding::main

x right rightCooridor left
  o downLeft
    q ?tomatoRed

t left
  n at: RP_RockyLanding::main

t blastOff
  n at: StartingQuadrant::main

x welcomingPlanet WP_GrassyLanding::main blastOff
  zz WelcomingPlanet
  gt landing
  gr blastOff
  o topRight grassyNook left
    qb blueKey
  @ WP_GrassyLanding::grassyNook
    > seeItem
  @ @
  o bottomRight
    qb blueKey

x left WP_ShallowTunnel::main up
  o left
  o downMid
    qb ?throughWalls
x right rightEnd left
  o down
    qb ?throughWalls
  > grab blueKey
t left
  n at: WP_ShallowTunnel::main
x left zigZags right
  o upLeft
    qb ?darkBrown

x left WP_LeftTunnels::main right
  o upperLeft
    qb ?darkBrown
  o down
    qb swim

> jaunt WP_GrassyLanding::main
x topRight
  n at: WP_GrassyLanding::grassyNook
  ta grab
  n [ It's just a note about hiding something in a tree house. ]
t left
  n at: WP_GrassyLanding::main

x bottomRight WP_RightBeach::leftSide left
  o right
    qb swim

t left
  n at: WP_GrassyLanding::main
t blastOff
  n at: StartingQuadrant::main
  o right asteroids left
    qb ?traverseAsteroids
  @ asteroids
    o concentricArcs
    > seeItem
  @ @

t rockyPlanet
  n at: RP_RockyLanding::main

x downLeft RP_MiddleTunnel::main up
  o down
    qb swim
  > grab shipShields
  F shipShields ?traverseAsteroids

> jaunt StartingQuadrant::main

x right
  n at: StartingQuadrant::asteroids
  @t concentricArcs
    q ?exploreSpaceStations
    n [ For now this is "too hostile with your current equipment" ]
  @ @
x right rightSide left
  qb shipShields
  o waterWorld
    q ?exploreWaterWorld
  o sateliteColony
    q ?exploreSpaceStations

x signsOfCivilization BP_BarrenLanding::main blastOff
  zz BarrenPlanet
  gt landing
  gr blastOff
  o down
    qb swim
x right rightFork left
  o right
  o down
t left
  n at: BP_BarrenLanding::main
x left leftFork upRight
  o downLeft
  o downRight

x downLeft BP_LeftSideArc::main up
  o right
    qb swim

x down BP_BottomSide::main left
  o down redStrip up
    q ?tomatoRed
  @ redStrip
    > seeItem
  @ @
  o right
  o up

x right BP_RightSide::main down
  o left
    q blueKey
  o up upperPart down
    qb ?tomatoRed
  @ upperPart
    o left
      qb blueKey
  @ @

x left BP_MetalHallways::bottomRight
x up midRight down
  v right BP_RightSide::upperPart left
  o upRight
  o left
t down
  n at: BP_MetalHallways::bottomRight
x left bottomMid right
  o left

x teleporter RP_RockyNook::main teleporter
  gb teleport
  n [
    Based on compeltion percentages changing, this destination is on
    another planet.
  ]
  o left
    qb ?throughWalls
  o up
    qb ?throughWalls
  o down
    qb ?tomatoRed

t teleporter
  n at: BP_MetalHallways::bottomMid
  > grab EVASuit
  F EVASuit ?tomatoRed
x left bottomLeft right
  > grab RPGCartrige
  n [ Unlocks a galaxy variation? ]
  o left
    gt ?throughWalls

> jaunt BP_BottomSide::main
x down
  n at: BP_BottomSide::redStrip
  ta grab
    At gain UnnamedPioneerJournal
t up
  n at: BP_BottomSide::main

x up corridor down
  o right
    qb swim
  o left
    qb swim
  o up
    qb swim

> jaunt BP_RightSide::main
x up
  n at: BP_RightSide::upperPart

x right BP_BarrenLanding::rightEnd left
  qb EVASuit
  o down
    qb greenKey
r topLeft rightFork right
x down lowerRightEnd up
  o down
    qb redKey

> jaunt BP_RightSide::upperPart
t left
  n at: BP_MetalHallways::midRight
x upRight topRight downRight
  > grab LabReport42
  n [ They discovered teleportation! ]
  v up BP_BarrenLanding::rightEnd down
  o left
    gt ?throughWalls
r down midRight upLeft
  o leftThrough
    gt ?throughWalls
  o up lockedRoom down
    qb blueKey&redKey&greenKey&yellowKey

> jaunt BP_BarrenLanding::leftFork
x downRight midLeftTunnel up
  > grab EncyclopediumGalaxiumPtIVRexII
  n [ This was the second-chance home of humanity. ]

> jaunt BP_BarrenLanding::main

t blastOff
  n at: StartingQuadrant::rightSide
  F EVASuit ?exploreSpaceStations
  n [ Going to take a break here. ]
  g time: 2024-3-21:15:32

w
  g time: 2024-3-22:08:58

x sateliteColony SC_EVADock::main blastOff
  gt landing
  gr blastOff
  zz SateliteColony
  o downRight rightCorner up
  o midRight rightFork up
  o down centralJunction up
    qb blueKey
x downLeft leftFork up
  o right centralJunction left
    qb redKey
  o down SC_LeftTeleporterRoom::main up
    qb blueKey
  @ SC_LeftTeleporterRoom::main
    o teleport
      gb teleport
  @ @
t up
  n at: SC_EVADock::main
x midRight
  n at: SC_EVADock::rightFork
  o left centralJunction right
    qb redKey
  o down SC_RightTeleporterRoom::main up
  @ SC_RightTeleporterRoom::main
    o teleport
      gb teleport
  @ @
t up
  n at: SC_EVADock::main
x down
  n at: SC_EVADock::centralJunction
  o down SC_CenterRoom::main up
    qb redKey
  @ SC_CenterRoom::main
    > seeItem
  @ @
t up
  n at: SC_EVADock::main
x downRight
  n at: SC_EVADock::rightCorner

x left SC_CenterRoom::bottom right
  o up main down
  o down
    qb greenKey
x left leftEnd right
t right
  n at: SC_CenterRoom::main
x up
  n at: SC_CenterRoom::main
  ta grab
    At gain redKey
t up
  n at: SC_EVADock::centralJunction
  t right
  n at: SC_EVADock::rightFork

x down
  n at: SC_RightTeleporterRoom::main

x teleport BP_FromSCRight::main topTeleporter
  zz BarrenPlanet

x bottomTeleporter SS_SateliteColonyAccess::main -
  gb teleport
  zz SpaceStations
  o teleportBack
    gb teleport
x left connector right
  qb blueKey&EVASuit
x left snorkelStorage right
  qb blueKey&EVASuit
  > grab snorkel
  F snorkel swim
t right
t right
  n at: SS_SateliteColonyAccess::main

r teleportBack BP_MetalHallways::bottomMid -

> jaunt BP_BarrenLanding::lowerRightEnd
x down BP_TeleportersComplex::topHalf up
  o left
    qb greenKey
  o down bottomHalf up
  o tpTopRight
    gb teleport
  o tpBotRight
    gb teleport
  o tpTopLeft
    gb teleport
  o tpBotLeft
    gb teleport

x tpTopRight WP_TreeTop::main teleport
  zz Natura
  > grab EncyclopediumGalaxiumPtIINatura
t teleport
  n at: BP_TeleportersComplex::topHalf

x tpBotRight WW_IsolatedHallway::main -
  zz WaterWorld
  o tpTop
    gb teleport
  o tpBot
    gb teleport
x tpTop WW_AtollEdge::main tp
  ...
t tp
  n at: WW_IsolatedHallway
r tpBot BP_TeleportersComplex::topHalf -

t tpBotRight
  n at: WW_IsolatedHallway::main
t tpTop
  n at: WW_AtollEdge::main
  > grab EncyclopediumGalaxiumPtVYorethera
  o goIn WW_AtollInterior::main goOut
    qb snorkel&?throughWalls
  @ WW_AtollInterior::main
    > seeItem
    o up
      qb redKey&snorkel
  @ @
t tp
  n at: WW_IsolatedHallway
t tpBot
  n at: BP_TeleportersComplex::topHalf

x tpTopLeft RP_HiddenNook::main tp
  > grab EncyclopediumGalaxiumPtIIIRift

t tp
  n at: BP_TeleportersComplex::topHalf

x tpBotLeft SS_TeleportersComplexAccess::rightSide tp
x left connector right
  qb blueKey&EVASuit
  o left leftSide right
    qb EVASuit&yellowKey
t right
  n at: SS_TeleportersComplexAccess::rightSide

t tp
  n at: BP_TeleportersComplex::topHalf

> jaunt BP_BarrenLanding::main
x down BP_LeftTPCAccess::main up
x right BP_SecondTeleporterComplex::main left
  qb blueKey
  > grab ShipTeleporter
  n [ Now we can teleport back here from the menu. ]

domain menu
S menu
  oa menuTeleport
    q ShipTeleporter
    e goto main//BP_SecondTeleporterComplex::main
  ta menuTeleport

domain main
  n at: menu//menu
  n at: BP_SecondTeleporterComplex::main
  v topRight BP_TeleportersComplex::topHalf left
  o tpTopRight
    gb teleport
  o tpMidRight
    gb teleport
  o tpBotRight
    gb teleport
  o tpMidLeft
    gb teleport
  o tpBotLeft
    gb teleport

r tpTopRight RP_RockyLanding::deadEnd -
? active
ta menuTeleport
  n at: BP_SecondTeleporterComplex::main

x tpMidRight SS_EVADock::main -
  zz SpaceStations
  q EVASuit
  v blastOff StartingQuadrant::asteroids concentricArcs
    gt blastOff
    gr landing
  > grab telescope
    n [ Increases vision range again. ]
  o right
x left SS_LeftSide::main down
  qb blueKey&EVASuit
  > grab EncyclopediumGalaxiumPtVIRing
  o up SS_LeftNook::main down
    qb redKey&EVASuit&yellowKey
  @ SS_LeftNook::main
    > seeItem
  @ @
t down
  n at: SS_EVADock::main
  @t right
    qb greenKey
  @ @

t blastOff
  n at: StartingQuadrant::asteroids

t right
  n at: StartingQuadrant::rightSide

  F snorkel ?exploreWaterWorld

x waterWorld WW_WaterLanding::main blastOff
  zz WaterWorld
  gt landing
  gr blastOff
x down WW_OpenOcean::main up
  qb snorkel
  ...
x landNorthWest northWestIsle launch
  qr snorkel
t launch
  n at: WW_OpenOcean::main
  @ WW_TopIsland::main
    o tp
      gb teleport
    > seeItem
  @ @
  @ WW_WestIsland::main
    o tp
      gb teleport
    > seeItem
  @ @
  o enterCenterEdge WW_CentralIsland::edge leave
    qb yellowKey&snorkel
  @ WW_CentralIsland::edge
    > seeItem
  @ @
  o enterCenter WW_CentralIsland::center leave
  @ WW_CentralIsland::center
    > seeItem
  @ @
  o enterAtoll WW_AtollInterior::main up
  .

x enterAtoll
  n at: WW_AtollInterior::main
  ta grab
  n [
    Well I already explored all of the water so this wasnt useful, but it
    would have revealed all the water...
  ]

t menuTeleport
  n at: BP_SecondTeleporterComplex::main

r tpBotRight WW_OpenOcean::northWestIsle -

t menuTeleport
  n at: BP_SecondTeleporterComplex::main

x tpMidLeft WP_RightBeach::leftEnd -
  o down
    qb ?throughWalls
x right WP_RightBeach::main left
  q snorkel
  v left leftSide right
x right rightSide left
  qr snorkel
  o up
    q ?darkBrown
  o right
    q ?throughWalls

> jaunt WP_LeftTunnels::main
x down WP_LeftTunnels::water
  q snorkel
x lowerLeft WP_LeftTunnels::roots right
  qr snorkel
  > grab RexSoftWiskipediaDiskNatura
t right
  n at: WP_LeftTunnels::water
x upperLeft WP_LeftTunnels::leftEnd bottomRight
  qr snorkel
  o topRight
    q ?darkBrown

ta menuTeleport
  n at: BP_SecondTeleporterComplex::main

x tpBotLeft SC_EVADock::centralJunction -

t left
  n at: SC_EVADock::leftFork
x down
  n at: SC_LeftTeleporterRoom::main

x teleport BP_FromSCLeft::main tpTop

x tpBot WW_WestIsland::main -
  gb teleport
  ta grab
    At gain spikeShoes
    F spikeShoes ?darkBrown

x tp BP_MetalHallways::bottomMid -
  gb teleport

t teleporter
  n at: RP_RockyNook::main

x down RP_LongEVATunnel::main bottomRight
  > grab LostEncyclopediumPageRift
  o downFromTopRight
    qb ?throughWalls

ta menuTeleport
  n at: BP_SecondTeleporterComplex::main

t tpMidLeft
  n at: WP_RightBeach::leftEnd
t rght
t right
  n at: WP_RightBeach::rightSide

x up WP_RightBeachTree::main down
  qb spikeShoes
  o right
    qb spikeShoes&?climbBridges

> jaunt WP_ShallowTunnel::zigZags

x upLeft WP_ShallowTunnelsTree::main down
  qb spikeShoes
  > grab GameToy
  n [ Cosmetic. ]
  > grab greenKey
  o left
    qb spikeShoes&?climbBridges

> jaunt WP_LeftTunnels::main
x upperLeft WP_LeftTunnelsTree::bottom downRight
  qb spikeShoes
  o up top down
    qb spikeShoes&?throughWalls
  v downLeft WP_LeftTunnels::leftEnd topRight
    qb spikeShoes

ta menuTeleport
  n at: BP_SecondTeleporterComplex::main

t topRight
  n at: BP_TeleportersComplex::topHalf
x down
  n at: BP_TeleportersComplex::bottomHalf
  o tpTopRight
    gb teleport
  o tpBotRight
    gb teleport
  o tpTopLeft
    gb teleport
  o tpBotLeft
    gb teleport

x tpTopRight SS_MidWestFragment::top -
  o tp
    gb teleport
x down mid up
  qb blueKey&EVASuit

x teleport SS_BotWestFragment::mid teleport
  qb redKey&EVASuit
  gb teleport
x right rightSide left
  qb redKey&EVASuit
  > grab LostEncyclopediumPageSpaceStations

> jaunt SS_MidWestFragment::top
x tp BP_TeleportersComplex::topHalf -

t down
  n at: BP_TeleportersComplex::bottomHalf

x tpBotRight WW_TopIsland::main -
  > grab LostEncyclopediumPageYorethera
  o tp
    gb teleport

x tp BP_TeleportersComplex::topHalf -

t down
  n at: BP_TeleportersComplex::bottomHalf

x tpTopLeft RP_TPPair::main -
  q EVASuit
  v tp BP_TeleportersComplex::topHalf -
    gb teleport

x tpOnwards RP_TPPair2::main tpBack
  qb EVASuit
  gb teleport

x tpOnwards RP_EVAFragment::main tpBack
  qb EVASuit
  gb teleport
  > grab FinalNote
  n [ A few survivors of the rift were abandoned. ]

t tpBack
t tpBack
t tp
  n at: BP_TeleportersComplex::topHalf

t down
  n at: BP_TeleportersComplex::bottomHalf

x tpBotLeft SC_TeleportersChain::main -
  > grab ExplorersNote
  n [
    Someone has been exploring this galaxy for a while with no signs of
    life.
  ]
x tpBack BP_TeleportersComplex::topHalf -
  gb teleport

> jaunt BP_LeftSideArc::main
x right BP_Waterway::main left
> grab ropeBridge
  F ropeBridge ?climbBridges
r south BP_BottomSide::corridor up
x left BP_BottomWaters::leftEnd right
t right
  n at: BP_BottomSide::corridor
x right BP_BottomWaters::rightEnd left
  > grab RexSoftWiskipediaDiskRexII

ta menuTeleport
  n at: BP_SecondTeleporterComplex::main

t tpMidLeft
  n at: WP_RightBeach::leftEnd

> jaunt WP_RightBeachTree::main

x right WP_RightmostTree::main left
  > grab wallPhaser
  F wallPhaser ?throughWalls
x down WP_BottomRight::main up
  qb wallPhaser
r left WP_RightBeach::rightSide right
  qb ?throughWalls

> jaunt WP_RightBeach::leftEnd

x down WP_MidConnector::water up
  qb wallPhaser&snorkel
x left main right
  qb wallPhaser&snorkel
  > grab Laptop
    n [ We can read the data disks? ]

> jaunt WP_ShallowTunnelsTree::main

x left WP_LeftTunnelsTree::main right
  qb ropeBridge&spikeShoes
  > grab AnthroJournal
    q wallPhaser&spikeShoes
r down WP_LeftTunnelsTree::bottom up

> jaunt WP_GrassyLanding::main
t blastOff
  n at: StartingQuadrant::main

t rockyPlanet
  n at: RP_RockyLanding::main
t right
  n at: RP_RockyLanding::rightCooridor

x downLeft RP_RightEVAConnector::main up
x down RP_MidTunnel::main up
  > grab RexSoftWiskipediaDiskX
r left RP_BottomV::main right

> jaunt RP_InnerUpperLeftHallway::main
x downRight RP_Waterway::main upLeft
  v up RP_MiddleTunnel::main down
x down RP_WaterNook::main up
  > grab RexSoftWiskipediaDiskRift

ta menuTeleport
  n at: BP_SecondTeleporterComplex::main

t topRight
  n at: BP_TeleportersComplex::topHalf
x down
  n at: BP_TeleportersComplex::bottomHalf

x down BP_MetalHallways::bottomMid
  qb wallPhaser

t teleporter
  n at: RP_RockyNook::main

x up RP_RightSide::main down
  > grab RexSoftWiskipediaDiskYorethera

ta menuTeleport
  n at: BP_SecondTeleporterComplex::main

t tpBotLeft
  n at: SS_EVADock::centralJunction

r phaseDown SC_CenterRoom::bottom phaseUp
  qb wallPhaser
x downLeft SC_BottomLeft::main up
  qb wallPhaser|greenKey
  > grab EncyclopediumGalaxiumPtIx
x right SC_BottomRight::main left
  qb wallPhaser
  a grabA
    q wallPhaser|yellowKey
    At [ { gain FloobitsKube; deactivate } ]
    n [ Unlocks a new game mode. ]
  > grab File436c

> jaunt SC_EVADock::main
t blastOff
  n at: StartingQuadrant::main

t waterWorld
  n at: WW_WaterLanding::main
t down
  n at: WW_OpenOcean::main

x enterCenterEdge WW_CentralIsland::edge
  qb yellowKey|wallPhaser
  ta grab
    At gain yellowKey

t leave
  n at: WW_OpenOcean::main

x enterCenter
  n at: WW_CentralIsland::center
  ta grab
    At gain Shovel
    n [ Unlocks moletroid ]

ta menuTeleport
  n at: BP_SecondTeleporterComplex::main

> jaunt BP_MetalHallways::midRight
x up
  n at: BP_MetalHallways::lockedRoom
  A save
  n [ Some kind of auto-save here? ]
  m galaxySelfDestruct:off
  > grab spareTyre
  n [ Finally found it! We should get back to our new friend. ]
    At set galaxySelfDestruct:on
  n [
    But this triggered the galaxy self-destruct mechanism!! We need to gt
    back to our ship, but teleportation and key cards are now disabled
    and lots of areas are sealed off.
    We're on a ~3-minute timer.

    I'm not going to bother annoating the escape sequence, although it
    does have a few choices and I'm not sure what their rammifications
    are.
  ]
  @ menu//menu menuTeleport
    q !galaxySelfDestruct:on
  @ @
  # TODO: Annotate this?
  a doEscape
    q galaxySelfDestruct:on
    At goto:endings//victory

R
p StartingQuadrant::main
  n When loading to continue we get put back into the StartingQuadrant.
  # TODO: Better notation for this?

t rockyPlanet
  n at: RP_RockyLanding::main

> jaunt RP_LeftJunction::main
x right RP_LeftTunnel::main left
  > grab DiscoBall
  n [ Cosmetic ]

> jaunt RP_RockyLanding::main
t blastOff
  n at: StartingQuadrant::main

t right
  n at: StartingQuadrant::asteroids

t concentricArcs
  n at: SS_EVADock::main

x right SS_WaterSections::main
  qb EVASuit&snorkel&greenKey
  > grab 4thRingNote
  n [
    Suggests there should be a 4th ring...
  ]

> jaunt SS_LeftSide::main
x up
  n at: SS_LeftNook::main
  ta grab
    At gain RexSoftWiskipediaDiskRing

ta menuTeleport
  n at: BP_SecondTeleporterComplex::main

t topRight
  n at: BP_TeleportersComplex::topHalf

t tpBotLeft
  n at: SS_TeleportersComplexAccess::rightSide 
t left
  n at: SS_TeleportersComplexAccess::connector

x left
  n at: SS_TeleportersComplexAccess::leftSide
  > grab YingYangBadge
  n [ Unlocks speed run and zen modes. ]
  n [
    This is the last item in the Galaxy! But what about stuff I saw
    during the escape?
  ]

ta menuTeleport
  n at: BP_SecondTeleporterComplex::main

> jaunt BP_MetalHallways::lockedRoom
  n [ I trigger the escape sequence again. This time I'll annotate it. ]
  ta grab
    At { [ lose blueKey; lose redKey; lose yellowKey; lose wallPhaser ] }
  Xt doEscape
  @t down
    q !galaxySelfDestruct:on
  @ @

x up BP_EscapePath::main

x teleport SS_EscapePair::main -
  gt teleport
  zz SateliteColony

x teleport WW_EscapePath::main -
  gt teleport
  zz WaterWorld

x teleport WW_EscapePath::main -
  gt teleport
  zz WaterWorld

x teleport BP_SecondEscapeVisit::main -
  gt teleport
  zz BarrenPlanet

x teleport SS_SecondEscapePair::main -
  gt teleport
  zz SateliteColony

x teleport RP_EscapePath::main -
  gt teleport
  zz RockyPlanet
  n [ There's a non-functional teleporter here as a distraction. ]

x teleport BP_ThirdEscapeVisit::main -
  gt teleport
  zz BarrenPlanet

x teleport SC_EscapeDiagonal::main -
  gt teleport
  zz SateliteColony

x teleport SS_InnerRing::main -
  gt teleport
  zz SpaceStations

x teleport WW_EscapeMoon::main -
  gt teleport
  zz WaterWorld
  n [ Here again what appear to be options are in fact broken or inaccesible. ]

x teleport BP_FourthEscapeVisit::main -
  gt teleport
  zz BarrenPlanet

x teleport WP_EscapeTreeTop::main -
  gt teleport
  zz WelcomingPlanet
  m door:closed
  o door lockedNook leave
    qb blueKey|door:open
x left leftTree right
  At set door:open
  o teleport
    gt teleport
t right
  n at: WP_EscapeTreeTop::main
x door
  n at: WP_EscapeTreeTop::lockedNook
  > grab animals
  n [ Save the animals! ]
t leave
t left
  n at: WP_EscapeTreeTop::leftTree

x teleport RP_FinalEscape::main -
  zz RockyPlanet

Ea victory
  gt blastOff
