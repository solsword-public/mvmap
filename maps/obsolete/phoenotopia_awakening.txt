[Gails_Room]
< START
> Left

[Orphanage]
< Door_3
% Kazu
% Mika
? Door_1
? Door_2
? stairs
> Door_4

[Teddy_Room]
< Right
. <berry_fruit>
> Right

[Orphanage]
< Door_4
> Door_2

[Blueprints_Room]
< Left
> Left

[Orphanage]
< Door_2
> Door_1

[Model_Dragon_Room]
< Right
> Right

[Orphanage]
< Door_1
? Bottom_Left
- stairs
> Top_Left

[Cozy_Room]
< Door
> Door

[Orphanage]
< Top_Left
> Bottom_Left

[Dining_Hall]
< Mid_Door
!% Nana * fetch_kids
% Kitt
? Left_Door
? Right_Door
> Mid_Door

[Orphanage]
< Bottom_Left
% Kazu
% Mika
" Note: These aren't the kids I
" need to fetch I guess? Same
" dialogue as before...
> Bottom_Left

[Dining_Hall]
< Mid_Door
> Left_Door
() Right_Door

[Panselo]
< Orphanage
. <dandelion>
" On top of the orphanage
% Clem @ {kids went star hunting}
% Nairi
. <perro_egg>
>< Perro_Coop [. <perro_egg>]
? East
% Tina
% Paul
% Ari
" Finally heard the name of the
" town
% Bertie
x gate (closed) ? West
> Dojo

[Dojo]
< Left
% Cadell
% Donnell
% Jayani
>< Door [Caddells_Room]
x practice (no_weapon)
< Left

[Panselo]
< Dojo
> Market

[Market]
< Main_Door
% Gladys
% Tao
>< Door_1 [Carving_Room]
> Door_2

[Sues_Room]
< Left
% Sue @ {Ren went to the forest}
> Left

-Market
[Panselo]
< Market
> Tower

[Tower]
< Bottom_Door
x armory (no_weapon) 
x chest (no_weapon)
> Bottom_Door

[Panselo]
< Tower
> East

[Panselo_East]
< West
% Adar
? garden
> Warehouse

[Warehouse]
< Door
. <Wooden_Bat>
~ {Dragging crates}
~ {Breaking things}
. <money>
> Door

[Panselo_East]
< Warehouse
> West

[Panselo]
< East
> Dojo

[Dojo]
< Left
- practice
~ {Attacks}
> Left

[Panselo]
< Dojo
> Tower

[Tower]
< Door
- chest
> Door

[Panselo]
< Tower
- gate
> East

[Panselo_East]
< West
% Carol
% Mia
% Jon . <panselo_potato>
~ {Eating}
~ {Camera}
% Synthia
% Farnea ~ {Switches}
? Farmhouse
? Lab
? East
> Tower

[East_Tower]
< Door
x up (tall)
- chest
- down
- boxes
- up
. <Heart_Ruby>
> Door

[Panselo_East]
< Tower
> Lab

[Lab]
< Door
x blast_door (high_switch)
> Door

[Panselo_East]
< Lab
> Farmhouse

[Farmhouse]
< Lower_Door
% Annie
% Rusty
~ {Cooking}
> Upper_Door

[Farmhouse_Attic]
< Left
% Mauve {brother} {orphan} {Ava}
> Left

-Farmhouse
[Panselo_East]
< Farmhouse
% Stan
> West

[Panselo]
< East
> West

[Panselo_Plains]
< Panselo_West
> Panselo_East

[Panselo_East]
< East
x gate (closed)
> East

[Panselo_Plains]
< Panselo_East
> Panselo_West

[Panselo]
< West
> East

[Panselo_East]
< West
% Robot
% Rutea
- gate
> East

[Panselo_Plains]
< Panselo_East
# Pig
" Ran away; they were scary!
... Gails_Room
# Toad
# Slime
" Beat em!
... Gails_Room
> Doki_Forest

[Doki_Forest_Entrance]
< Left
# berry_fruit
% Felix+Lewis
> Right

[Doki_Forest_Gultch]
< Left
# Frog
# Slime
- jump ~ {Leaping}
? Right
> Hole

[Doki_Forest_Cave]
< Hole
# Steelworm
. <doki_herb>
> Hole

[Doki_Forest_Gultch]
< Hole
> Right

[Doki_Forest_Pools]
< Left
% Ernest ~ {Water}
% Ren
% Rita
- fish_pool
- berries_jump
> Right

[Doki_Forest_Cliff]
< Left
~ {Crawling}
- chest
> Right

[Doki_Forest_Temple_Entrance]
< Left
% Ella
% Shelby ~ {Flint} {Cooking}
> Right

[Doki_Forest_Temple_View]
< Left
x right (water)
- up
!% Alex+Seth+Amanda
. <Slingshot>
- fall
" Oops. But I figured out that there is no "right"
() right
> Right

[Anuri_Temple_Entrance]
< Left
x gate (orb)
. <anuri_pearlstone>
x blocks (strong)
- gate
> Right

[Anuri_Temple_Antechamber]
< Left
? Bird_Door
? Chevron_Door
x Chest_Door (orb)
> Right

[Anuri_Temple_Throne_Room]
< Left
? Chest_Door
? Chevron_Door
x gate (orb x3)
> Left

[Anuri_Temple_Antechamber]
< Right
> Bird_Door

[Anuri_Temple_Shrine]
< Left
(campfire)
(save)
> Left

[Anuri_Temple_Antechamber]
< Bird_Door
> Chevron_Door

[Anuri_Temple_Ruined_Shaft]
< Top_Door
# Bat
? down
> Right

[Anuri_Temple_Maze]
< Left
- maze . <anuri_pearlstone>
> Left

[Anuri_Temple_Ruined_Shaft]
< Right
- down
x cracked_wall (strong)
? Bottom_Door
< Left

[Anuri_Temple_Pot_Gate]
< Right
- pot_button
> Left

[Anuri_Temple_Pines]
< Right
- switches . <anuri_pearlstone>
> Right

-Anuri_Temple_Pot_Gate
[Anuri_Temple_Ruined_Shaft]
< Left
> Bottom_Door

[Anuri_Temple_Chess_Hall]
< Up_Chevron_Door_West
? Frog_Door
? down
? across
> Left

[Anuri_Temple_Frog_Statues]
< Right
- switch
> Left

[Doki_Forest_Temple_Entrance]
< Bottom_Right
x blocks (strong)
> Bottom_Right

-Anuri_Temple_Frog_Statues
[Anuri_Temple_Chess_Hall]
< Left
> Frog_Door

[Anuri_Temple_Swordsview_Pool]
< Left
- pot_switch . <Energy_Gem>
> Left

[Anuri_Temple_Chess_Hall]
< Frog_Door
- across
? Diamond_Door
? Up_Chevron_Door_East
? Right
- down
x blocks (strong) ? Crown_Door
> Diamond_Door

[Anuri_Temple_Pot_Warehouse]
< Left
- stacking . <moonstone>
> Left

[Anuri_Temple_Chess_Hall]
< Diamond_Door
> Up_Chevron_Door_East

[Anuri_Temple_Blocked_Shaft]
< Bottom_Chevron_Door
x crawlspace (high+short)
> Bottom_Chevron_Door

[Anuri_Temple_Chess_Hall]
< Up_Chevron_Door_East
> Right

[Anuri_Temple_Retracting_Columns]
< Left
- jumping_puzzle . <anuri_pearlstone>
> Left

-Anuri_Temple_Chess_Hall
-Anuri_Temple_Ruined_Shaft
[Anuri_Temple_Antechamber]
< Chevron_Door
> Chest_Door

[Anuri_Temple_Lighbulb_Puzzle]
< Left
- puzzle . <Heart_Ruby>
> Left

-Anuri_Temple_Antechamber
[Anuri_Temple_Throne_Room]
< Left
> Chest_Door

[Anuri_Temple_Toads_Chamber]
< Left
# Rock_Toad
" These are armored
. <lunar_vase>
> Left

[Anuri_Temple_Throne_Room]
< Chest_Door
> Chevron_Door

[Anuri_Temple_Blocked_Shaft]
< Top_Door
(<) crawlspace
? Left
> Right

[Anuri_Temple_Frog_Fountain]
< Left
- switches_puzzle . <moonstone>
> Right

[Anuri_Temple_Frogs_Fountainhead]
< Left
- switches . <anuri_pearlstone>
> Left

-Anuri_Temple_Frog_Fountain
[Anuri_Temple_Blocked_Shaft]
< Right
> Left

[Anuri_Temple_Frogpots]
< Right
- switches . <anuri_pearlstone>
> Right

-Anuri_Temple_Blocked_Shaft
[Anuri_Temple_Throne_Room]
< Chevron_Door
- gate
> Right

[Anuri_Temple_Courtyard]
< Left
# Massive_Slime
!4 Massive_Slime
> Right

[Anuri_Temple_Crash_Site]
< Left
. <Mysterious_Golem_Head>
x Right (orb x2)
> Left

-Anuri_Temple_Courtyard
-Anuri_Temple_Throne_Room
-Anuri_Temple_Antechamber
-Anuri_Temple_Entrance
[Doki_Forest_Temple_View]
< Right
!% Alex+Seth+Amanda $ fetch_kids * alien_abduction
< Left

-Doki_Forest_Temple_Entrance
-Doki_Forest_Cliff
-Doki_Forest_Pools
-Doki_Forest_Gultch
[Doki_Forest_Entrance]
< Right
!% Seth+Amanda
!!

[Panselo_East]
< Right
!% Seth+Amanda
% Rita @ {nobody is around}
% Felix
% Lewis
> Left

[Panselo]
< Right
> Orphanage

[Dining_Hall]
< Right_Door
% Seth @ {Kitt is missing; everyone was kidnapped}
> Mid_Door

[Orphanage]
< Left
> Attic_Door

[Attic]
< Door
- boxes
- chest
> Door

[Orphanage]
<
>< *
" Confirmed nobody is around!
> Left

[Dining_Hall]
< Mid_Door
> Left_Door

[Panselo]
< Orphanage
% Shelby+Ella
% Ernest
% Amanda
>< Dojo
" Nobody there either
> Market

[Market]
< Main_Door
% Ren
>< *
" Nobody
> Main_Door

[Panselo]
< Market
% Alex
!? {Get kids together}
!? {Talk to kids: [Tell truth]|Camping excuse}
* scientist_in_atai
!? {Chances vs. aliens: [Have to try]|I don't know}
!!

[Orphanage]
< CUTSCENE
% Seth
> Door_3

[Gails_Room]
< Door
!? {Rest}
> Door

[Orphanage]
< Door_3
% Felix
% Shelby
% Ella
>< Door_4
>< Door_2
>< Door_1
>< Attic_Door
>< Top_Left
> Bottom_Left

[Dining_Hall]
< Mid_Door
% Amanda . <potato_lunch>
% Alex
> Left_Door

[Panselo]
< Orphanage
% Ernest ~ {Ukemi}
> Market

[Market]
< Main_Door
% Ren
% Rita
. <milk>
> Main_Door

[Panselo]
< Market
> Right

[Panselo_East]
< Left
% Seth
% Lewis
> Lab

[Lab]
< Door
- blast_door
>< Top_Door [.<Heart_Ruby>]
> Basement_Door

[Lab_Storage_Closet]
< Door
> Door

[Lab]
< Basement_Door
> Door

[Panselo_East]
< Lab
> Right

[Panselo_Plains]
< Panselo_East
# Bee . <moonstone>
? more
> Sunflower_Road

[Sunflower_Road]
< Right
. <nectar>
x climb (tall) : <Heart_Ruby>
> Left

[Honeybee_Labs_and_Inn]
< Right
% Machigo
% Hachiko
% Peace_Minister
? Left
< Door

[Honeybee_Inn]
< Door
% Hachi
% Machi
>< Cat_Room
>< Beds
. <honeydrop>
> Door

[Honeybee_Labs_and_Inn]
> Left

[Honeybee_Inn_Garden]
< Right
# Bees
... Honeybee_Inn
" Got bodied and rested again
- jumping_with_bees . <moonstone>
> Left

[Sunflower_Banks]
< Sunflower_Road
? Cave
> South_Castella_Bridge

[South_Castella_Bridge_East]
< Right
(save)
% Harmony+Forte+Alto+Cobette
- bridge . <moonstone>
x left_water (boxes+strong) : <Heart_Ruby>
> Left

[South_Castella_Bridge_Tower]
< Right
x rescue (strong)
(campfire)
x Door (sealed)
" Blue poster here...
> Left

[South_Castella_Bridge_West]
< Right
x blocks (strong) : <Energy_Gem>
(save)
> Left

[Atai_Region]
< South_Castella_Bridge
? more
> Atai_West

[Atai_West]
< Left
(save)
>< First_House
? Well
? Tailor
? Shooting_Gallery
> Hairstylist

[Sharlas_Hair]
< Left
% Sharla
> Left

[Atai_West]
> Tailor

[Tailoring_Tracys]
< Left
% Tracy . <pockets>
> Left

[Atai_West]
> Shooting_Gallery

[Shooting_Gallery]
< Door
% Gallery_Master
> Door

[Atai_West]
< Garnets_House

[Garnets_House]
< Top_Door
% Garnets_Mom
% Garnets_Dad
>< Bottom_Door [.<moonstone>]
> Top_Door

[Atai_West]
> Right

[Atai_Center]
< Left
% Guard {Ouroboros}
% Lisa * lisa_in_chains
? Palace_Side_Entrance
? right
> Palace_Main_Entrance

[Atai_Palace]
< Right
% Guard
% Rufus
% Mayor ~ {Capture Birdy}
? Dim_Door
> Bright_Door

[Atai_Legacy_Corridor]
< Right
% Aging_Aide ~ {Neri I-VI; Rhodus-Castellan antagonism
>< Mid_Door
> Top_Door

[Atai_Palace_Roof]
< Left_Tower_Door
? Storeroom_Door
? Right_Tower_Door
> Mayors_Door

[Mayors_Bedroom]
< Right
x switches (code)
(bird)
> Right

[Atai_Palace_Roof]
> Storeroom_Door

[Atai_Palace_Storeroom]
< Door
- puzzle
. <moonstone>
> Door

[Atai_Palace_Roof]
> Right_Tower_Door

[Atai_Chefs_Tower]
< Top_Door
% Palace_Chef
() Bottom_Door-Atai_Center:Palace_Side_Entrance
> Left

[Atai_Palace_Dining_Hall]
< Right
> Left

-Atai_Palace
< Dim_Door
> Right

[Atai_Center]
% Lisa ~ {skills: lock-picking, ventriloquism, forgery}
> Right

[Atai_East]
< Left
% Bo {Freerunning course}
???
% Guard
% Children {Caught a bandit}
> Prison_Door

[Atai_Prison]
< Door
% Prison_Guard {Bandits southwest; sand drakes}
% Prison_Overseer
% Jailer {Bribe}
    % Abduction_Witness
    " No options to interrogate this fellow :(
    % Ouroboros_Bandit {Needs wine}
> Top_Door

[Atai_East]
< Prison_Roof_Door
> Storage_Tower_Door

[Atai_Storage_Tower]
< Door
- darkness . <moonstone>
> Door

[Atai_East]
% People {abduction, etc.}
> Atai_Inn

[Atai_Inn]
< Door
% Innkeeper {ID card for wine}
> Top_Door

[Atai_East]
< Left

[Atai_Center]
% Lisa . <Lisas_ID>
> Left

-Atai_West
[Sharlas_Hair]
% Sharla . <brown_hair>
< Left

-Atai_West
-Atai_Center
-Atai_East
[Atai_Inn]
. <wine>
> Door

[Atai_East]
> Tall_House

[Atai_Tall_House
< Top_Door
> Prison_Crevice

[Atai_Prison]
< Prison_Crevice
% Zeke .<Bandits_Flute>
> Door

[Atai_East]
> Right

[Atai_Region]
< Atai_East


" Sand flue puzzle:
" + => O
" Corners => O.
" Dotted => O-
" Plaid => O+

" Plaid - dotted - corners - plus - x - corners
" + | - | . | O | x | .

" Sand temple hints
" cross... seek quiet wind... straight forevermore...
" darkness... seek narrow path...
" beware... gold...
" seashells... descend... reverse...
" first... ascend...
" bones... turn back...
" serenity... diagonal
" cross... seek loud wind... second treasure
