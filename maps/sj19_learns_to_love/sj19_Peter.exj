# Configuration
P on

# TODO: domain settings
domain Main

# Macros
= cutsceneHappens name note [
    a {name}
      gt forced
      g cutscene
      At disable
      n [ note ]
]
= cutsceneTo where [
    p {where}
      g cutscene
]

= claimPower what [
  a claim{what}
    gt power {what}
    At gain {what}
    At deactivate
]

= seeItem type what [
  oa get{what}
    gt item {what}
    gt itemType {type}
    e gain {what}*1
    e deactivate
]

= claimItem type what [
  a get{what}
    gt item {what}
    gt itemType {type}
    At gain {what}*1
    At deactivate
]

# TODO: Challenge notation
# TODO: Room locks notation
= boss who level [
  a {who}
    gt bossFight
    gt challengeSkill bossFight
    gt challengeLevel {level}
    At gain {who}Defeated
    At disable
]

# TODO: Skill notation
= gainedLevel kind howMany [
  A gain skill{kind}*howMany
]

= enemy which [
  gd enemy:{which}
]

= friendly which [
  gd friendly:{which}
]

= save [
  a saveGame
    gt save
]

= jaunt where [
  p {where}
    g jaunt
]

= talk who [
  a talkTo{who}
    gt character:{who}
    gt conversation
]

= shopItem type what howmuch [
  oa purchase{what}
    q money*{howmuch}
    e lose money*{howmuch}
    e gain {what}*1
    gt item {what}
    gt itemType {type}
]

# Note: Must use shopItem first...
= buy what [
  a purchase{what}
]

# Instead of tracking every SP picked up from enemies, I'll just observe
# SP when it seems relevant.
= haveSP howmuch [
  A set SP {howmuch}
  n has: SP*{howmuch}
]

# TODO: Challenge syntax!
= taxChallenge skill level price [
  gt challenge:{skill}:{level}
  gt failPrice:{price}
]

= rejectChallenge skill level price [
  gt challenge:{skill}:{level}
  gt failPrice:{price}
  gt rejectOnFail
]

# TODO: Challenge syntax!
= reciprocalTaxChallenge skill level price [
  gr challenge:{skill}:{level}
  gr failPrice:{price}
]

= reciprocalRejectChallenge skill level price [
  gr challenge:{skill}:{level}
  gr failPrice:{price}
  gr rejectOnFail
]

= seeSwitch [
  oa switch
    e deactivate
    e set switch:open
]

= note [
  oa note
    gt note
    e deactivate
]

= noted name content [
  > note
  a note
    At gain name
    nt [ content ]
]


# Start of journal
g time 2023-12-2>13:22
n [
  We pop out of a canister.
  Leadership requested we were awoken. It's time for our mission.
  We can use pods to save.
  The robot that picked up our pod to activate us explains all this.
]
S main PodRoom
  zz PinkHangar

x right main left BannerHallway
  qr X
  n [
    It glitched the first time and let me go back but upon re-entering
    (not noted here) I once more can't go back.
  ]

x right main left HallOfHeroes
  qr X
  n [
    SJ units that achieved greatness are memorialized here.
  ]

x right main left Armory
  qr X
  a killHoverbot
    e deactivate
    e set doors:open
  n [ I try to wait this out a bit but no luck. It's a tutorial. ]
  o right
    q doors:open

x right leftSide left Entrance
x right rocket left

x blastOff main returnHome LandingSite
  zz TheRocket
  qr X
  n [
    Our key target is the rebel scientist Professor Flora.
    She must be destroyed.
  ]
  > save
  a talkToFrog
    n [ We don't have a frog pass, so we should get outta here. ]
  oa passFrog
    q FrogPass
  o left
  o right

x left main right WestSteps
  > enemy frog
  o left

t right
  n at: LandingSite::main

x right main left EastBump
  > enemy frog
  o right

t left
  n at: LandingSite::main

t left
  n at: WestSteps::main

x left main right CaveEntrance
  o left
  o down

t right
  n at: WestSteps::main

t right
  n at: LandingSite::main
  n [
    All this back-and-forth is me trying to stablish a basic home base of
    known stuff, before deciding where to go. I think I'll go right.
  ]

t right
  n at: EastBump::main

x right main left EastFlats
  > enemy frog

x right leftSide left SkullCave
  n [ The giant red skull blocking our way thinkgs robots are cool. ]
  oa placateSkull
    q ?skullItem
    e deactivate
    e set skull:placated
  o right rightSide left
    qb skull:placated

> jaunt CaveEntrance::main
  n [ Guess I have to go left first. ]

x left main right SpidersNote
  > enemy spider
  > noted Log1 [
    This planet... it has forsaken us. We have no future here. Flora...
    disagres. Flora is a fool.
    * Professor Phlox
  ]

x left main right BellChamber
  a ringBell
    At deactivate
    At gain health*1
    n [
      A mini arena fight with frogs and hornets. There goes my pacifism.
    ]
  > enemy frog
  > enemy hornet

t right
  n at: SpidersNote::main

t right
  n at: CaveEntrance::main

x down main up UndergrowthTop
  zz TheUndergrowth
  > save

x down topMid up MushroomsAlley
x down main upMid
  > enemy frog
  > enemy earthworm
  > enemy spider
  o left
  o right bottomRight left
    qb ?breakVines

x upRight main down FrogWiggle

x right top left HivesAlley
  > enemy hive
  o right
  n [ Got knocked down here by a hornet... ]
x down bottom jumpUp
  qr ?higherJump
  gt unintentional
  > enemy frog
  > enemy hive
  o down
  o right
x right rightSide bottomLeft
  v topLeft top right
  o right
  n [ Sign says we can go right to the hive... ]

p UndergrowthTop::main 
  g death

t down
  n at: MushroomsAlley::topMid
x left leftSide right

x up bottomRight down SJ14Room
  > enemy hive
  > enemy earthworm
x left tvRoom right
  > talk SJ-14
  n [
    Mentions we don't have to complete our mission. Just wants to watch
    their TV show.
  ]
t right
  n at: bottomRight
  n [ The constant stream of hornets has me defeat the hives. ]
  o up top down
    q ?higherJump|?fartherJump
    n [
      Our second ability barrier. Looks like it's time to visit the
      hornet's nest. I'm a bit sad that we don't have a strong pacifist
      option given the framing of the game so far :(
    ]

> jaunt HivesAlley::rightSide

x right main left HiveSave
  > save

x right leftLedge left BigFrogRoom
x down main up
  qr ?higherJump
  o left
    qb ?squeezeThrough
  o right
    qb ?squeezeThrough
  n [ The classic "you'll be softlocked" idiom. ]
> talk BigFrog
  n [
    Is disturbed we've been killing frogs.
    We'll never get a frog pass...
  ]
  n level: bossFight 0
  > boss BigFrog 2
  n [
    Becomes a very busy boss fight! Maybe I need to mash better?
  ]

p HiveSave::main
  g death

> jaunt BigFrogRoom::main
  > gainedLevel BigFrog 1
  a BigFrog

p HiveSave::main
  g death

> jaunt BigFrogRoom::main
  a BigFrog
    At gained platform:appeared
    At gained FrogPass
    n [
      Shows frogs that we're cool...
      T_T
    ]
  n [ Third time's the charm. ]
  > gainedLevel bossFight 1
  @
    o up
      q ?higherJump|platform:appeared
  @ @
  n [ Trope broken: we didn't get morph ball here. ]

t up
  n at: BigFrogRoom::leftLedge

t left
  n at: HiveSave::main
  > save

t left
  n at: HivesAlley::rightSide
t bottomLeft
  n at: HivesAlley::bottom
  n [
    The frog pass seems to prevent frogs from aggroing on sight, but they
    still damage us :(
  ]

x down leftSide up ShroomCave
  > enemy hive
  > enemy frog
  > enemy mushroom
  o right rightSide left
    qb ?breakVines
  @ rightSide
    o down
    > enemy hive
  @ @

p HiveSave::main
  g death
  n [ A very silly death. ]

> jaunt LandingSite::main
  > save
  n [
    Time to try out the frog pass?
  ]
  a passFrog
  n [
    Seems like a warp system? But we don't have any other locations open
    yet.
  ]

> jaunt SkullCave::leftSide
  a shootSkull
    At deactivate
    At set skull:gone
    n [
      Just one shot really hurts...
    ]
    n [
      Meta: I'm trying this because I think I'm out of options down below...
    ]
  @
    o right
      qb skull:placated|skull:gone
  @ @
x right leftSide left

x right main left Entrance
  zz BoneCity
  > enemy boneWalker

x right main left BoneCitySave
  > save

x right main left BoneHallway
  > enemy skullThrower

x right main left GiantSkullChamber
  n [
    We "have to leave" because the giant red skull is paid to guard this
    place.
  ]
  > bossFight ninjaStars -1
    At set platforms:appeared
  n [
    Those were the skull's favorite ninja stars. The skull itself flies
    upwards in tears.
  ]
  o right rightSide left
    qb switch:open
  @ rightSide
    > seeSwitch
  @ @
  o up
    q platforms:appeared
t left
t left
  > save
t right
t right

x up main down DarkJunction
  > save
  o up
  o right

x up main down FloatingSkullHall
  > enemy floatingSkull
  o left
  o right

x left main right ChainsArena
  qr doors:open
  n [
    Just two simple waves... Will there be another arena to the left?
  ]
  > enemy floatingSkull
  > enemy boneWalker
  a fightEnemies
    At deactivate
    At set doors:open

x right bottom left BonesClimb
  q doors:open
  > enemy boneWalker
  > enemy skullThrower
x up top down
  > enemy floatingSkull
  o right

x left main right PortraitSave
  > save
  > noted Log2 [
    General Rex, you are hereby stripped of all titles and wealth due to
    your failure.
    * Professor Phlox
  ]

x up main down DesolateBalcony
  n [
    Someone who is presumably General Rex is here. He's not going to
    fight us, and encourages us to "do what we must." But we have no beef
    with him.
  ]

x jumpDown caveTop jumpUp SkullCave
  qr ?higherJump
x down leftSide up
  qr ?higherJump

> jaunt DarkJunction::main
  > save

x right main left BonesFrog
  o warp main toBonesFrog WarpMenu
    qb FrogPass
  @ WarpMenu::main
    o toRocketFrog main warp LandingSite
      qb FrogPass
  @ @
  n [
    Meta: Clearly would have been nice to explore this first... But I
    didn't. I wonder about the level design implications. I think for me
    partially I was expecting progress in this direciton because of the
    comeback lever below, so I avoided it initially.
  ]

t up
  n at: FloatingSkullHall::main

x right main left SkullThrowerHall
  > enemy skullThrower

x right BonesHallway
  > enemy floatingSkull
  > enemy skullThrower
  > enemy boneWalker

x right top left TwoSkullsJunction
  > enemy floatingSkull
  o right
  o down

x right main left DrillChamber
  > talk boostBot
  n [
    We get upgrades based on SP collected from enemies.
    We can also change upgrades here.
    We get to choose either the binoculars or the haunted bullets.
  ]
  > haveSP 353
  a getBinoculars
    q SP*150
    n [
      I have no idea what the thresholds are... Note that it's not a cost.
    ]
    At gain Binoculars
    At lose HauntedBullets
  oa getHauntedBullets
    q SP*150
    e gain HauntedBullets
    e lose Binoculars
  n [ The upgrade here will make fighting bosses easier. ]
  > gainedLevel bossFight 1
  n level: bossFight 2

t left
  n at: TwoSkullsJunction::top
x down bottom up
  > enemy boneWalker
  > enemy skullThrower

x left main right RedSkullDive

x down main up ChandeliersSave
  qr ?higherJump
  > save
  o right

x left rightSide right SpineGateJunction
  > enemy floatingSkull
  > seeSwitch
  a switch
x left leftSide topRight
  qb switch:open
  o bottomRight midNook left
    qb switch:open
  @ midNook
    o down
  @ @

x left GiantSkullChamber::rightSide right
  a switch
t up
  n at: DarkJunction::main
  > save

> jaunt SpineGateJunction::midNook

x down main up BonesElevator
  o right elevator left
    qb ?breakVines
  @ elevator
    o downElevator
      qb elevator
  @ @

x left main right AcidVats
  > noted Log3 [
    These poor skeletals were tossed aside after Phlox's experiment
    failed. We should not tolerate this disregard for life. * Professor
    Flora
  ]

x left main right AcidBar
  > enemy acidDrinker

x left main right AcidJunction
  o down
  n [ Sign pointing down has hive emblem on it. ]

x left main right AcidPipeRoom
  n [
    Probably a boss and I'm at half health. Would like a closer save
    point.
  ]
  oa talkSkeleton

t right
  n at: AcidJunction::main

x down main up AcidSave
  o left
    qb ?breakVines
    n [
      I'm assuming now we get acid vine-breaking powers from fighting the
      skeleton .
    ]

t up
  n at: AcidJunction::main

t left
  n at: AcidPipeRoom::main
  > talk Skeleton
  n [
    We get warned repeatedly and cast as the aggressor (which we are).
  ]
  > boss SteveTheMilkMan 3

p AcidSave::main
  g death
  n [
    Given the framing, I'm tempted to see if I can get around this
    peacefully.
  ]

> jaunt ChandeliersSave::main
  > save

x right main left BigSkullBigChamber
  > talk BigSkull
  o right
    qb ?breakVines
  n level: bossFight 2
  > boss Tokotsu 3

p ChandeliersSave::main
  g death
  n [ Not sure I want to grind out this fight either. ]

> jaunt DarkJunction
  > save

> jaunt BonesClimb::top
x right main right ChainsArena2
  qr doors:open
  > enemy floatingSkull
  > enemy boneWalker
  a fightEnemies
    At deactivate
    At set doors:open

x right main left BonesBell
  q doors:open
  a ringBell
    At deactivate
    At gain health*1
    n [ Another arena fight. ]
    # TODO: Challenge level
    At set bell:defeated

x right main left UpperBonesSave
  q bell:defeated
  > save

r down SkullThrowerHall up
  qr ?muchHigherJump
  n [
    I indeed didn't notice this before, except vaguely on the map.
  ]
  n [
    So now we need to fight at least the milkman to progress...
  ]

> jaunt AcidSave
  > save

> jaunt AcidPipeRoom::main
  n level: bossFight 2
  n boss level is 3
  a SteveTheMilkMan
    At gain Milk
    n [
      Does not break the vines... Intead increases my invuln time?
    ]
  > gainedLevel bossFight 1
  n level: bossFight 3

> jaunt ChandeliersSave::main
  > save

t right
  n level: bossFight 3
  n [ Boss level is 3. ]
  a Tokotsu
    n [
      Died to the battle sword. This is a *very* busy fight.
    ]

p ChandeliersSave**main
  g death

t right
  a Tokotsu

p ChandeliersSave**main
  g death

t right
  a Tokotsu

p ChandeliersSave**main
  g death

t right
  a Tokotsu
  At gainedLevel Tokotsu 1
  n [
    It's better to fight the adds since their death damages the boss and
    you can't skip the add upgrades by focusing just the boss.
  ]

p ChandeliersSave**main
  g death

t right
  a Tokotsu
    n [
      Got 'em!
    ]
    At gain RocketModule
    F RocketModule ?breakVines
    At set leftGate:closed
  @ .
    o left
      q leftGate:open
  @ @

t right main left TokotsusRoom
  n [
    The room is full of anime paphernalia.
  ]
  > noted Log4 [
    Hello Diary! I got a cool new job as a guard! I can even use my own
    weapons!! I hope my mom would be proud...
  ]
    At warp EntryHall::main *1
    n [
      We error out on experiencing an illegal emotion (sadness).
    ]
    n [
      Aside: it's interesting what this says about the perceived relative
      worth of frogs vs. talking skulls vs. weird milk-shooting skeletons.
    ]

n at: EntryHall::main
  zz RepairFacility
  n [
    We see a vision of Tokotsu and their mom.
  ]

x right VirusRoom::main left
  q warp:EntryHall::main *1
  n at: EntryHall::main
  n [
    We are warped back to the beginning of the room to see another
    montage about Tokotsu's childhood.
    We learn that Tokotus had Skulitis, and his mom sacrificed herself to
    save her son.
  ]

x right
  n at: VirusRoom::main
  n [
    What seems like an impossible fight here; we lose it an the error
    message says more stuff which I can't read quickly enough.
  ]
  > boss Viruses 100

p TokotsusRoom::main
  g virusDeath
  A gain Sadness

> jaunt ChandeliersSave::main
  > save

> jaunt BonesElevator::main
x right
  n at: BonesElevator::elevator

x downElevator main upElevator ShipSave
  zz Waterworks
  > save
  o left
  o right

x right main left PipesHallway
  > enemy bird
  > enemy swordfish

x right top left WaterEntrance
  o down
    q ?waterProof

x right main left DoubleBubbleTrouble
  > enemy bird
  > enemy fish

x right main left PipeworksSave
  > save

x right main left WaterValve
  a switchOff
    At deactivate
    At gain G::waterValve:off
    # TODO: Syntax for boss fight triggered by action
  n level: bossFight 3
  > boss SecuriBot-A13 3
  n [
    A hard boss fight but I fight well.
  ]

t left
  n at: PipeworksSave::main
  > save

> jaunt WaterEntrance::top
  @ .
    o down
      q ?waterProof|G::waterValve:off
  @ @
x down bottom up
  o right
  o left

x right main left TurtlesHallway
  > enemy swordfish
  > enemy turtle
  n [
    Kill them with the anchor; even rockets don't work.
  ]

x right main left LowerDrainageSave
  > noted Log6 [
    Flora this is unit 3 we are under attack! There is some sort of...
    beast in The Drainage! If I don't make it back... I love you, Flora.
    * Aster
  ]
  > save

x right LowerWaterValve
  a switchOff
    At deactivate
    At gain G::lowerWaterValve:off
    # TODO: Another boss fight trigger
  n level: bossFight 3
  > boss Kraken1 1
    n [
      Just have to damage it enough until it bashes you out of the room.
    ]
    At warp DrainageShaft::top *1

n at: DrainageShaft::top
x down bottom up
  qr ?flight
  gt forced
  n [
    I'm not noting it as thoroughly; but there's a water-rising thing with virus attacks where we gain FEAR.
  ]
  A gain Fear

x left main right DarkDrainageSave
  > save

x left main right DarkDrainageHallway
  > enemy tentacle
  > talk DashBotM
    At deactivate
    At gain DashBotM

t right
  > save

t left
  n at: DarkDrainageHallway::main
x left leftSide right
  qr KrakenDefeated
  q DashBotM
  n [
    The beast is chasing us again. We use the dash to get through.
  ]
  > enemy tentacle
  o left
    q KrakenDefeated
    gt unsure
    n [
      It almost seems like there's a way left here if a tentacle weren't
      blocking it. Maybe we can come back to explore? But quite possibly
      not.
    ]

x down main upRight LowerChaseTunnels
  qr higherJump
  > enemy tentacle
  n [
    More chase sequence.
  ]
x left leftSide right
  qr KrakenDefeated

x upLeft main downRight SecondDarkSave
  qr KrakenDefeated
  > save

x left bottom right WetKrakenClimb
  n [
    More chase
  ]
  > enemy tentacle
x up top down
  q DashBotM
  qr KrakenDefeated&?waterProof

x left main right WaterChaseSave
  qr KrakenDefeated&?waterProof
  > save

x left main right KrakenChamber
  qr KrakenDefeated
  o left
    q KrakenDefeated
  n level: bossFight 4
  > boss Kraken
  n [ Otto the Octo. ]

p WaterChaseSave::main
  g death

t left

  g time 2023-12-2>16:15
w
  g time 2023-12-2>17:05

  a Kraken
    n [
      Took a break here as car service was done.
    ]
    At gain ChargePack
    n [
      We're very mean!
    ]

t right
  > save

t left
  n at: KrakenChamber::main

x left rightSide right TurtlePool
  qr X
  n [ No going back that way. ]
  > talk TurtleBoater
  n [
    Rather than just the shut door, the boat ride & dialogue explicitly
    tell us that we can't revisit Otto's area.
  ]
x crossWater leftSide -
  qr X
  > save

x up main down AnchorsLedge
  > enemy turtle

x left main right LowerDrainageElevator
  > save

x elevatorUp main elevatorDown DrainageElevatorRoom
  qb elevator
  o left

x right main left SplitLevelHallway
  > enemy frog
  > enemy hive

r right main left AcidSave

> jaunt ShipSave
  > save

x left main right TurtleTutorial
  > enemy turtle

x left top right LeftWaterEntrance
  > noted Log5 [
    Our unit has been in The Drainage for weeks now and something is NOT
    right. There are loud bangs echoing through halls every night!
  ]
  o down
    q ?waterProof|G::waterValve:off

x left main right DrainageFrog
  o warp main toDrainageFrog WarpMenu
    qb FrogPass

t right
  n at: LeftWaterEntrance::top
x down bottom up

x right leftSide left BubblesHallway
x across rightSide across
  qr G::waterValve:off
  n [
    The bubblers here would make going through the other way pretty
    costly. I'm not going to bother trying to estimate the damage cost...
  ]

x right main left DrainageBell
  a ringBell
  a ringBell
    At deactivate
    At gain health*1
    n [
      A bubble bullet hell.
    ]

x right main left DrainageBoost
  > talk boostBot
  > haveSP 727
  a getBucketOfIce
    q SP*500
    n [
      I have no idea what the thresholds are... Note that it's not a cost.
    ]
    At gain BucketOfIce
    At lose RocketSteroids
  oa getRocketSteroids
    q SP*500
    e gain RocketSteroids
    e lose BucketOfIce

r right bottom left WaterEntrance

> jaunt LowerDrainageSave::main
  > save
  n [
    Thought I might as well see what the status is. The right exit is
    collapsed.
  ]
  @
    o right
      qb X
  @ @

> jaunt DrainageElevatorRoom

x left rightSide right ShroomCave
x down UndergrowthBoost
  > talk boostBot
  > haveSP 795
  a getShootingGloves
    q SP*750
    n [
      I have no idea what the thresholds are... Note that it's not a cost.
    ]
    At gain ShootinGloves
    At lose Plutonium
  oa getPlutonium
    q SP*750
    e gain ShootinGloves
    e lose Plutonium
  # TODO: Upgrade menu at each boost bot...

x left UndergrowthFrog
  o warp main toUndergrowthFrog WarpMenu
    qb FrogPass

> jaunt MushroomsAlley::main
x right
  n at: MushroomsAlley::bottomRight

x down rightSide up BigBrownVine
  > noted Log8 [
    The corrruption is spreading further everyday. Soon the lab will be
    blocked. I need to speak with Aster, she will know what to do.
    * Professor Flora.
  ]
  o left
    qb ?traverseBigVine

> jaunt SJ14Room::bottomRight
  F DashBotM ?fartherJump
x up top down
x downLeft main up SpidersHallway
  > enemy spider
  > enemy earthworm
x left main right CorruptJunction
  > talk DoomedProfessor
  n [
    The corruption spread to them!
    Their partner is still down this path, where their research shows the
    corruption originates.
  ]
  o up
  o left
  n [
    I want to save the companion!
  ]
  > save

x left main right BrownEntrance
  > noted Log7 [
    The undergrowth is filled with the essence of life. If I can find a
    way to exract it... it will be an extremely efficient and clean
    energy source!
    * Professor Phlox
  ]

x left main right CorruptFlowerRoom
  qr ?higherJump
  n [
    A flower sprouts from a lab-coated body here...
  ]
  > gainedLevel bossFight 1
  n [ Two upgrades since the last boss fight... ]
  n level: bossFight 5
  > boss CorruptFlower 6

p CorruptJunction::main
  g death

> jaunt CorruptFlowerRoom::main
  a CorruptFlower

p CorruptJunction::main
  g death

> jaunt CorruptFlowerRoom::main
  a CorruptFlower
    At gain LastStand
    At gain G::corruption:restored

x left main right SyringeRoom
  a breakSyringe
    At gain joy
    n [
      Another rebooting interactive cutscene.
    ]
    At gain CorruptFlowerRoom::platforms:appeared

t right
  n at: CorruptFlowerRoom::main
  @ .
    o right
      q ?higherJump|platforms:appeared
  @ @

t right
t right
  n at: CorruptJunction::main
  n [
    The professor is dead now, and asks us to leave their body alone so
    we don't become corrupted.
  ]
  > save

x up main down UnderGrowthTJunction
  o right
    q ?higherJump
    n [
      You can glitch this with the dash but I think I'd softlock if I
      fought the bell fight so I won't.
    ]

x left main right ThreeSpiders
  > enemy spider
  > enemy frog

x left bottom right TrickyClimb
  > enemy hive
  > enemy frog
  > enemy mushroom
  > enemy spider

x up top down
  q DashBotM|?higherJump

x right main left WormConnector
  > enemy frog
  > enemy earthworm

x right leftLedge left StagBeetleRoom
  o down main upLeft
    qr ?higherJump
  @ main
    o upRight rightLedge down
      q ?higherJump
  @ @
  n [
    So... clearly I'm going to get the jump upgrade here, right?
    This is second "you'll be trapped" idiom. Maybe platforms just
    appear?
  ]
  > enemy stagBeetle
  a fightStagBeetle
    At deactivate
    At set platforms:appeared
    n [ Well what do you know... ]
    n [ Not even a boss... ]
  @ .
    o upRight
      q ?higherJump|platforms:appeared
  @ @
x right
  n at: StagBeetleRoom::rightLedge

x down main up StagBeetleBell
  qr ?flight

o left UnderGrowthTJunction::main right
  q platforms:appeared
  a ringBell
    At deactivate
    At gain health*1
    n [
      Three waves of enemies, including dual stag beetle.
    ]
    At gain platforms:appeared

> jaunt CorruptJunction::main
  > save

> jaunt BigBrownVine::rightSide
  F G::corruption:restored ?traverseBigVine
x left leftSide right
  q DashBotM&G::corruption:restored

x left main right Foyer
  zz TheLab
  > enemy ceilingTurret

x left main right Reception
  > enemy ceilingTurret
  > enemy knivesSpinner

x left main right LabSave
  > save

x left main right ComputerStation
  > cutsceneHappens meetFlora [ The killbot is here! ]
  n level: bossFight 5
  > boss Flora 5
  n [
    After a few shots I wait for a long time to see if I can time this
    out. But it seems I can't.
  ]

p LabSave::main
  g death
  n [ Time for a break. ]
  g time 2023-12-2>18:10

w
  g time 2023-12-2>21:09

p ComputerStation::main
  > gainedLevel Flora 1
  a Flora
  n [
    Okay not such a hard boss. We get EMPd and Flora limps away.
  ]

x left main right TestTubesSave
  > save

x left main topRight LabNexus
  o topLeft
  o bottomRight
  o bottomLeft
  o down

x topLeft rightEdge right VatBell
  o left main right
    qb bell:rung
  @ main
    oa ringBell
      e deactivate
      e gain health*1
      e gain bell:rung
    > note
    o left
  @ @

t right
  n at: LabNexus::main

x bottomRight main left SniperHallway
  > enemy blooper
  > enemy knivesSpinner
  > enemy ceilingTurret
  n [
    Aster is also shooting at us as we advance.
  ]

x right main left SniperHallway2
  > enemy blooper
  > enemy knivesSpinner
  n [ Aster is still shooting at us. ]

x right main left LabPlantsSave
  > save

x right main left Infirmary
  n [
    Aster points a gun at us while Flora lies in critical condition on
    the bed. She won't let us take Flora's Soul Power (SP). She loves
    Flora. Aster dumps a bunch of EMPs, but it doesn't take us down (and
    instead we take her soul power). We sustain a LOVE error, and becuase
    we've had too many errors, a system wipe is engaged.
  ]
  n level: bossFight 5
  > boss SystemWipe 5
    n [
      We fight a giant robot, then when it self-destructs to kill us, we
      un-fight Tokotsu, Otto, and Flora & Aster, shooting them to fill back
      up their health bars. We gain Love and disconnect from HQ.
    ]
    At gain Love
  > talk Aster
  > talk Tokotsu
  > talk BigFrog
  n [
    The other bosses we defeated like BigFrog and SteveTheMilkMan are
    also here, along with many enemies.
  ]

E LearnedToLove

p LabPlantsSave::main
  g load
  n [
    Want to finish out the map.
  ]

> jaunt LabNexus::main

x down LabBoost
  > talk boostBot
  > haveSP 1303
  a getTrust
    q SP*1000
    n [
      I have no idea what the thresholds are... Note that it's not a cost.
    ]
    At gain Trust
    At lose Amity
  oa getAmity
    q SP*1000
    e gain Trust
    e lose Amity
  # TODO: Upgrade menu at each boost bot...

t up
  n at: LabNexus::main

x bottomLeft main right KnivesArena
  n [
    Fight 3 waves of knivesSpinners.
  ]
  > enemy knivesSpinner

x left main right TurretsArena
  n [
    Fight off two waves of knivesSpinners + ceilingTurrets.
  ]
  > enemy knivesSpinner
  > enemy ceilingTurret

x left main right BeakersJunction
  o left
  o up
  > noted Log9 [
    At last! This pure energy from the Undergrowth combined with my
    research has created perfection. An emotionless unwavering KILLBOT.
    * Professor Phlox
  ]

x left main right LabFrog
  o warp main toLabFrog WarpMenu
    qb FrogPass

t right
  n at: BeakersJunction::main

x up main down BeamersCorner
  > enemy ceilingTurret
  > enemy blooper

x right main left LabTopArena
  > enemy knivesSpinner
  > enemy blooper

x right main left VatBell
  > noted Log10 [
    The day has come. We have confirmed that a killbot unit from Phlox's
    operation is on its way here. How can we stop it Aster...
    * Professor Flora
  ]
  a ringBell

r right
t right
  n at: LabNexus::main

> jaunt LabPlantsSave::main
  > save

t right
  a SystemWipe

E LearnedToLove
