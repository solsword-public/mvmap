P on

= locked n name [
  oa unlock_{name}
    q fruit*{n}
    e lose fruit*{n}
    e gain unlocked_{name}
    e deactivate
  o {name}
    q unlocked_{name}
]

= locked_visible n name dest recip [
  oa unlock_{name}
    q fruit*{n}
    e lose fruit*{n}
    e gain unlocked_{name}
    e deactivate
  o {name} {dest} {recip}
    q unlocked_{name}
]

= get n [
  oa get_fruit
    gt fruit
    e gain fruit*{n}
    e deactivate
  a get_fruit
]

= see n [
  oa get_fruit
    gt fruit
    e gain fruit*{n}
    e deactivate
]

= jars [
  oa break_jars
    gt jars
    e deactivate
    q noisemaker
]

= get_jars n [
  # TODO: Redundant action/observation stuff
  a break_jars
    At gain fruit*{n}
    At deactivate
    q noisemaker
]

= frozen [
  o break_ice_block
    gt ice_block
    e deactivate
    q noisemaker
]

= pinwheel [
  oa pinwheel
    gt pinwheel
    e deactivate
    q noisemaker
]

= spin_pinwheel n [
  a pinwheel
    At gain fruit*{n}
]

= character who [
  oa talk_{who}
    gt conversation
]

= talk who [
  a talk_{who}
    gt conversation
]

= item what [
  oa item_{what}
    e gain {what}
    e deactivate
]

# Should only use take AFTER item!
= take what [
  a item_{what}
]

= flower [
  gd flower
]

= mailbox [
  oa stuff_mailbox
    gt mailbox
    e deactivate
    e gain fruit*7
    q mailbag
]

= stuff_mailbox [
  a stuff_mailbox
]

= wickerling [
  oa wickerling
    gt wickerling
    e gain wickerling*1
    e deactivate
]

# repeatable farming
= farm n [
  a farm
    gt farm
    At gain *1 fruit*{n}
    At gain fruit*1
]

# One-time farming
= farm_once n [
  a farm_once
    gt farm
    At gain fruit*{n}
    At deactivate
]

# Same, but specifically for set tables.
= play_table n [
  a play_table
    gt farm
    gt table_play
    At gain fruit*{n}
    At deactivate
]

= chest [
  a chest
    gt chest
    e deactivate
]

= see_chest [
  oa chest
    gt chest
    e deactivate
]

= exploration_disposition what [
  g disposition {what}
]
= quest where text [
  g quest_to {where}
  n [
    Quest text: {text}
  ]
]
= quest_for item text [
  g quest_for {item}
  n [
    Quest text: {text}
  ]
]

= jaunt where [
  p {where}
    g jaunt
]

S beach Beach
  > quest none none
  > exploration_disposition explore
  zz Mokumana_Shore
  # TODO: Split up the beach?
  zzz Forest_Region
  o left
  o right
x left lagoon right Mokumana_Lagoon
  o down
    q dive
x left pinwheel_island right
  > get 3
  > pinwheel
  > exploration_disposition progress
t right
  n at: Mokumana_Lagoon::lagoon
t right
  n at: Beach::beach
x right beach_edge left
  > character Posterodactyl
  > talk Posterodactyl
  > quest Mokumana_Village [
    Deliver the Old Postal Badge to Nim the Translator
    in the Village
  ]
  At deactivate
  n The Posterodactyl flies away
  n Map zooms to show delivery location
  > item old_postal_badge
  > take old_postal_badge
  > mailbox
  > get 5
x right first_flower under_bumpers Beach_Edge
  @ Beach_Edge::first_flower under_bumpers
    # TODO: Let this be "er gain" instead...
    e gain *1 fruit*7
  @ @
t under_bumpers
  n Grabbing that fruit. This is also a bumpers tutorial.
t right
  n at: Beach_Edge::first_flower
  > flower

x bump_right forest_ledge fall_left Forest_Edge
  zz Mokumana_Forest
  > locked 30 climb_tree
  > see 24
  n [
      I have 15 fruit right now. Interestingly, if I hadn't gone back left
      from first_flower, and/or hadn't visited the outer island, I would be
      1 fruit short of 30! That's a mini fruit-harvesting
      tutorial/encouragement.
  ]
  o hop_down_to_ledge
    gt hidden
x hop_down_to_ledge tree_roots hop_up
  qr X
  a wickerling  # not using macro because first one is a surprise
    gt wickerling
    At gain wickerling*1
    At deactivate
    # TODO: Let this just be written as "first_flower"...
r fall_left Beach_Edge::first_flower bump_to_ledge
  qr X
t bump_right
  n at: Forest_Edge::forest_ledge
  a get_fruit  # set up by macro
    nt Getting this fruit wakes Fosfor
  o right
    qb satisfy_Fosfor
  > character Fosfor
  > talk Fosfor
  oa give_Fosfor_mushrooms
    q juicy_cove_mushrooms*1
    e lose juicy_cove_mushrooms*1
    e deactivate
    e gain satisfy_Fosfor
  > quest_for satisfy_Fosfor [ Find a Cove Mushroom ]
  a unlock_climb_tree
x climb_tree first_tree_ledge fall_down
  o roll_right treasure_tree_ledge roll_left
    qb push_peg

  @ Forest_Edge::treasure_tree_ledge
  > see_chest
  oa push_peg
    e gain push_peg
    e deactivate
  @ @

x roll_left second_tree_ledge hop_up
  qr X
v fall_down forest_ledge warp_up
  qr X
x bump_left first_squiggle bump_right
  At gain *1 fruit*8
x bump_left main bump_out_right Greenheart_Table
  gt timed
  n This is a bumper passing tutorial
  gd table
  o bump_left
  > play_table 28
x bump_left mid_tree_base bump_right Mid_Tree
  gt timed  # not the final act but getting in position for it
  > wickerling
  a wickerling
  o bump_up
    gt ice_blocked
r fall_left Greenheart_Table::main warp_up
  qr X
t bump_left

  @ mailbox_ledge
  > mailbox
  > see 4
  o bump_right
    e gain *1 fruit*4
  @ @

x bump_up main bump_down Butterfly_Rock_Ledge
  At gain *1 fruit*5
  > pinwheel
  o right
  o left
  > character Baldino
    q wake_up_Baldino  # sleeping
  > jars
  > locked_visible 30 bump_up_left chest_ledge fall_right

  @ chest_ledge
  > see_chest
  > frozen
  @ @

  a unlock_bump_up_left
  n Now I have 21 fruit left
x bump_up_left
  a chest
    At gain noisemaker
    F noisemaker wake_up_Baldino
  n The noisemaker ability to wake up Baldino is pretty obvious
  a break_ice_block
    At gain fruit*7
t fall_right
r fall_left Mid_Tree::mailbox_ledge warp_up
  qr X
  gt non-obvious
  gt momentum
  a get_fruit
  n [
      I tried the noisemaker on the mailbox since I didn't remember what
      was needed to stuff it.
  ]
x bump_right above_mid_ledge undo
  qr X
  o right
    e gain *1 fruit*4
    # TODO: Let this just be "mid_tree_base"...
r left Mid_Tree::mid_tree_base hop_up
  e gain *1 fruit*3
  gt hidden
  qr X
  n Note the noisemaker can unhide this...
t bump_up
  > talk Baldino
  n learn about Poison Toadstools
  > get_jars 5
t fall_left
t bump_right
r right Forest_Edge::first_squiggle warp_up
  qr X
t bump_left
t bump_left
t bump_up
t bump_up_left
r fall_left Greenheart_Table::main warp_way_up
  qr X
  gt non-obvious
  gt momentum
  n [
    Puts you on top of the table actually but your only option is to go back
    in and bump out.
  ]
t bump_left
t bump_up
  > spin_pinwheel 5
  > flower
  > get 2
x right below_covetop_lookout hop_up
  qr X
  > frozen
  o left
    gt stonering
  > jars
  o right
    q noisemaker
    e gain *1 fruit*5
x bump_up covetop_lookout fall_right
  > jars
  > get_jars 2
  > pinwheel
  > spin_pinwheel 5
  n [
    Note the fruit from this pinwheel are actually added to the fall_left
    transition...
  ]
  # TODO: Annotate that more precisely?

r fall_left Butterfly_Rock_Ledge::main hop_up_right
  qr X
  t right
    n at: below_covetop_lookout
  > exploration_disposition scrutinize
  n video "yokus-1.mkv" t = 9:46
  > exploration_disposition explore
x left main top_right HornflamesTable
  gd table
  > play_table 18
  n This table is a dead end.
t top_right 
t bump_up
  n at: covetop_lookout
  n Not sure why I did this, but okay.
t fall_right
  n at: below_covetop_lookout
  > get_jars 2
x right top bump_up_left LookoutTunnels
  @ LookoutTunnels::top bump_up_left
    e gain *1 fruit*3
  @ @
  o fall_left
  > locked 30 shoot_right
    e gain *1 fruit*5
  a unlock_shoot_right
x fall_left bottom bump_up
  > locked 30 shoot_right
  a unlock_shoot_right
r shoot_right Forest_Edge::treasure_tree_ledge bump_up
  gt tracks
  a chest
    At gain fruit*30
  a push_peg
  > locked 30 bump_left
  a unlock_bump_left
x bump_left wickerling_protrubance fall_right
  > wickerling
  a wickerling
t fall_right
  n at: Forest_Edge::treasure_tree_ledge
t bump_up
  n at: LookoutTunnels::bottom
t bump_up
  n at: LookoutTunnels::top
x shoot_right lower_ledge shoot_left CoveHeights
  At gain *1 fruit*2
  o right bottom left
    gt stonering
    n We get a table preview cutscene as we approach the stone ring.
    qr X
    e gain *1 fruit*4
  > exploration_disposition scrutinize
  n I check carefully for a fall-off to the left here.
  > exploration_disposition explore
x right
  z CoveMushroomTable
  n have: fruit*39
  gd table
  > play_table 10
x up top down
  gt timed
  n have: fruit*49
  gd table
  > play_table 10
  o swing_right
    q leashed_sootling
x up CoveHeights::top_ledge down
  a get_mushroom
    At deactivate
    At gain juicy_cove_mushrooms*1
    n Map is updated
x right hidden_cave left
  gt hidden
  a get_mushroom
    At deactivate
    At gain poison_toadstool*1
r shoot_left LookoutTunnels::top X
  gt stonering
  At gain *1 fruit*6
  qr X
  n have: fruit*65
  > exploration_disposition collect
t shoot_right
  n at: CoveHeights::lower_ledge
t right
  n at: CoveMushroomTable::bottom
t up
  n at: CoveMushroomTable::top
  > play_table 37
  n Note: my wallet cap is 100 right now... How to represent that?
  n have: fruit*100
t up
  n at: CoveHeights::top_ledge
r shoot_left LookoutTunnels::top fly_right
  qr X
t fall_left
  n at: LookoutTunnels::bottom
t shoot_right
  n at: Forest_Edge::treasure_tree_ledge
t roll_left
  n at: Forest_Edge::first_tree_ledge
t fall_down
  n at: Forest_Edge::forest_ledge
  a give_Fosfor_mushrooms
  n quest_complete
x right west_entrance left West_Entrance
  zz Mokumana_Glades
  e gain *1 fruit*4
  n have: fruit*100
  n Wallet is still full at this point.
  n video "yokus-1.mkv" t = 19:17
  > locked 30 bump_up
  a unlock_bump_up
  n have: fruit*70
  > flower
  > get_jars 11
  n have: fruit*81
x bump_up hillock_top fall_left
  > get 3
  n have: fruit*84
  > wickerling
  a wickerling
  o fall_across_right
  o fall_down_right hillock_roots_top hop_far_up
    qr X
x fall_across_right hillock_top_right hop_left
  qr X
  o through_door
    q glade_hillock_door_key1
    q glade_hillock_door_key2
    q glade_hillock_door_key3
  o fall_across_left
  o fall_down_left hillock_roots_top hop_mid_up
    qr X
x fall_across_left first_key_nook hop_across
  qr X
  a grab_key
    At gain glade_hillock_door_key1
    At deactivate
x fall_down hillock_roots_top hop_low_up
  qr X
  v fall_left West_Entrance::west_entrance hop_up_right
    qr X
x fall_right hillock_roots_right hop_up
  qr X
  oa grab_key
    e gain glade_hillock_door_key2
    e deactivate
  o left
r bump_up West_Entrance::west_entrance fly_back
  qr X
> jaunt West_Entrance::hillock_roots_right
  a grab_key
  n [
    Camera pans to show the key increasing activation of the door; this
    is our keys tutorial.
  ]
x left hillock_roots_left right
  At gain *1 fruit*3
  a grab_key
    At gain glade_hillock_door_key3
    At deactivate
> jaunt hillock_top_right
x through_door big_tree_base left Mokey_Statues_Clearing
  n video "yokus-1.mkv" t = 20:54
  > flower
  > locked 30 bump_up
  a unlock_bump_up
  n have: fruit*59
  > chest
    At gain Wallet_Upgrade*1
    n Now we can cary 150 fruit!
    # TODO: Annotations for wallet cap...
  > mailbox
  > character Tweepers
  > talk Tweepers
  n [
    They want us to deal with the sootlings that are crowding their nest
    (they'll give us something good).
  ]
  o right
    q deal_with_sootlings
  o bump_down
  n video "yokus-1.mkv" t = 21:29

? destinations
