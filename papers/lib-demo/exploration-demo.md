---
title: 'Representing Exploration in Metroidvania Games'
subtitle: 'A demo of the `exploration` Python library'
keywords: [exploration, graphs, player experience, metroidvania]
abstract: |
  Exploration is a core gameplay element in many games, but it has a
  special central place in the Metroidvania genre, where hidden powerups,
  backtracking, and map-based navigation are genre-defining elements.
  At the same time, space in Metroidvania games can be quite well represented
  by an abstract topology (i.e., a graph of connections between rooms),
  without needing to deal with the details of a grid-based or survey-type
  models of spatial relationships. These two properties give rise to
  unique challenges and opportunities in understanding the player
  experience of exploration in these games. This demo will walk the
  audience through `exploration`, an open-source Python library available
  on PyPI which includes data structures for representing exploration
  processes in discrete decision spaces including Metroidvania games, as
  well as parsing routines for a journal format to easily express
  exploration data by hand.
bibliography: refs.bib
documentclass: acmart
classoption: [sigconf]
biblio-style: ACM-Reference-Format
anonymous: true
link-citations: true
...

# Metroidvania Games and Exploration

"Metroidvania" is a game genre based on the operational logics of *Super Metroid* [@super_metroid] and *Castlevania: Symphony of the Night* [@castlevania_symphony_of_the_night]:[^1] an explorable space that includes barriers which can be passed after discovering permanent upgrades, secret areas with optional upgrades, and an in-game map to aid in exploration.
In contrast to "open-world" games, space is constrained to a series of rooms with specific paths between them; it can thus be cleanly abstracted into a topological representation, although spatial relationships and metrics are still important for using the map system.

[^1]: Note that the original *Metroid* is at the edge of the genre, whereas the original *Castlevania* is not even part of the genre. See [@metroidvania_com] for an explanation of the origins of the term.

In these games and more recent examples like *Metroid Prime* (in 3D) [@metroid_prime] and *Ori and the Blind Forest* (which does away with room transitions) [@ori_and_the_blind_forest] the excitement of exploration and discovery is central, even though combat and platforming challenges are also important.
For many such games, fans have also created *randomizers* [@jewett2021game] which scramble the item locations or in some cases room connections, which pose their own mapping challenges.
As groundwork for future research into human exploration processes in Metroidvania games to support principled level generation, we have developed a library titled `exploration` to represent exploration processes.

Existing work on exploration in games has focused on open-ended 2D and 3D environments, usually using maps to represent space (see e.g., [@osborn2021mappyland; @si2017believable; @stahlke2020synthesizing @dahlskog2015patterns]).
At the same time, some work in both procedural content generation and reinforcement learning has emphasized graph-based topological representations, although these works have not taken the human exploration process as their main focus (see e.g., [@dormans2010adventures; @smith2018graph; @zhan2018taking]).
The visualization system of [@moura2011visualizing] is close to what we have created, because it visualizes player movement through a continuous space as movement on a discrete graph of regions, although it does not reason about player exploration processes.

In a similar dichotomy, the literature on the psychology of exploration makes a distinction between route-based and survey-like spatial knowledge, often assuming that route-based knowledge is an earlier stage of knowledge development than survey-like knowledge (see [@jamshidi2021narrative] for a review and [@peer2021structuring] on the route/survey question).
The lack of an accessible and established format for capturing discrete player exploration traces led to the creation of our library.
Although the formats used by [@dormans2010adventures; @brown2016boss] for representing game spaces are close, they do not capture the process of exploration (only the structure of the map), and in any case they do not come with open-source tools to support creating and reasoning about maps.


## The `exploration` Library

Our demo will show off the capabilities of [the `exploration` Python library](https://pypi.org/project/exploration/), which is open-source under a 3-clause BSD license and available via the Python Package Index (PyPI).
Running:

`python -m pip install exploration`

should install a copy if you have Python version 3.7 or later.
The library includes three main features:

1. An interactive representation of Metroidvania-style maps as multi-digraphs with specialized node and edge labels. This is implemented by the `core.DecisionGraph` class.
2. A abstract representation of an exploration process as a sequence of such decision graphs, implemented by the `core.Exploration` class.
3. A parser for a plain-text journal format which can produce `Exploration` objects given a human read- and write-able journal of observations during play. This is implemented by the `journal.JournalObserver` class.


The `Exploration` class stores a list of discrete time steps, with a decision graph, a position in that graph, a transition taken, and a game state corresponding to each time step.
The individual decision graphs support tags and annotations as well as requirements for and effects of transitions, allowing rich representation of Metroidvania staples like powerups that allow traversal of previously-blocked transitions, and mechanisms or world events which likewise open up or close off certain transitions.
In fact, with the addition of a planned visualization layer and a bit of interactivity, the map format itself could act as a kind of game engine, although things like combat or platforming challenges would only be represented via shallow abstractions.[^2]

[^2]: The game *Maptroid* (which does not have any platforming or combat) provides an example of what could be achieved by adding simple graphics and basic controls to the map format [@maptroid].

The design choices that we hope to discuss are:

1. The choice of a graph-based rather than grid-based map format.
    - This opens up new perspectives for analysis.
    - It can be unwieldy when dealing with open spaces where routes exist
      between most locations.
2. The choice of a multi-digraph as the graph format, and considerations of how spatial relations can be retained in this format.
    - Multiple connections between nodes can be used to represent
      multiple paths (possible with different restrictions) between
      locations.
    - Self-loops represent actions that can be taken within a location.
    - Spatial relations can be annotated, but this annotation is
      potentially onerous.
3. The choice to represent unexplored spaces as tagged nodes in the graph.
    - This allows annotating things like multiple connections which you
      know lead to the same (still-not-explored) location.
4. The choice to record a sequence of entire graphs, rather than simply tracking position information in a single canonical graph.
    - This allows for transitions which alter the graph structure (e.g.,
      a cutscene where the level layout changes).
    - It also supports annotating mistaken beliefs that are later
      corrected.

Each of these design decisions has far-reaching consequences for a system that
hopes to represent exploration traces, and several of those are things we've
learned the hard way during the development process.

## Acknowledgements

This work was done on land stolen from the Massachusett, Wampanoag,
Nipmuck, Uypi, Amah Mutsun, and Chumash peoples, and the authors are
grateful for their stewardship of that land in the past, in the present,
and in the future. The history of this land is important because this
work could not have happened without its use. We acknowledge that we
directly benefit from this ongoing injustice.
