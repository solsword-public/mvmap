---
title: 'The Image of the Open World Game'
author:
- Kitty Boakye
- Kaitlyn Tsien
- Nissi Awosanya
- Peter Mawhorter
keywords: [exploration, player experience, landmarks, navigation]
abstract: |
  Open-world games like The Legend of Zelda: Breath of the Wild present
  expansive virtual environments and use exploration of these
  environments as a central activity. How do players understand these
  worlds and organize their mental representations of their spaces? Kevin
  Lynch theorized that inhabitants of a city understand the "image" of
  that city in terms of five key elements: landmarks, paths, edges,
  nodes, and districts. We investigated whether Lynch's categories can be
  applied to open-world game environments using thematic analysis of
  interviews in which players were asked to describe how they would
  navigate through these environments. We found that Lynch's elements are
  applicable to game environments, but that a sixth element type:
  "characters" stands out as a persistent theme in player's descriptions
  of these games. The addition of "characters" to Lynch's categories
  makes sense in virtual worlds, since the typical fixed routines of
  non-player characters make them more relevant to navigation than in the
  real world.
bibliography: refs.bib
documentclass: acmart
classoption: [sigconf,anonymous=true]
biblio-style: ACM-Reference-Format
anonymous: true
link-citations: true
...

# The Experience of Exploration in Open-World Games

Open-world games present expansive gaming environments to explore and use exploration as a core dynamic. How do players understand these spaces and what features do they use to navigate through them? A theoretical framework that explains player conceptualization of open-world spaces would be useful for analyzing player behavior and understanding how elements of game spaces affect the player experience. Thankfully, Kevin Lynch's work on "The Image of the City" [@image_of_the_city] provides just such a framework, although it was developed based on real-world experiences of cities rather than virtual experiences of open-world games. Through the analysis of numerous interviews with city-dwellers in multiple U.S. cities, Lynch developed a theory of five categories to organize the elements which contributed to one's image of a city: landmarks, paths, edges, nodes, and districts. Our central question is: Does Lynch's framework apply to open-world games, despite the differences between real-world cities and virtual play spaces?

To answer this question, we...

Using participant experiments from anonymous 5 participants playing open-world games: A Short Hike and Sable. 

We found that...

We find that using coding and thematic analysis, key themes appear in how players talk about their exploration through similar places.

# Related Work

- [@lynch1964image] (maybe also Lynch's later follow-up [@lynch1984reconsidering])

- We used thematic analysis [@braun2006using]
    * What is it?
    * Deductive vs. inductive (we used deductive which is theory-based)

- Others have proposed frameworks for understanding exploration in open-world games... Exploration in Open-World Games (The Witcher 3 )[@vickery2023exploration]
    * How is our work different?
    * Is this framework right? wrong? useful?
        * Most likely is it right but differently useful...

- Others have studied open-world games with a broader focus[@hughes2023understanding]
    * What did Hughes have to say about exploration?

- Others have studied exploration in games broadly [@kagen2022wandering]
    * What's most interesting/relevant from this book?
    * Can skim intro + a few paragraphs here and there to cite

- More citations?

# Methodology

- Explain research protocol here, preface with reiteration of why we used
  this method & what we hoped to find.

# Results

- Explain the themes we found, and give examples of each

# Discussion 

- Explain that we *do* think Lynch's categories apply
    * Did any categories not have robust representation? Why?

- Explain that we identified "characters" as an additional category.
    * Were there other new categories or themes? Why are/aren't we
      proposing these as new categories?
    * How to our categories match up against [@vickery2023exploration]?

# Future Work

- How can others use this result?
- What else might we want to do next?

# Bibliography
