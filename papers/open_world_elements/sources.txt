
- Article on fact sheet: https://www.gamespot.com/articles/minecraft-reached-140-million-monthly-users-and-generated-over-350-million-to-date/1100-6490962/
- Fact sheet: https://news.xbox.com/en-us/wp-content/uploads/sites/2/2021/04/Minecraft-Franchise-Fact-Sheet_April-2021.pdf
- charity: water event info (fight Illagers in the event...): https://www.minecraft.net/en-us/article/download-free-map--help-charity-water
    * Gameplay video of event: https://www.youtube.com/watch?v=ICazI5JhuFA
    * Event includes a defender of the village achievement
    * Humanizes villagers, but uses Illagers as mobs to slaughter

