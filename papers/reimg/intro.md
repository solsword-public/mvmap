## Introduction

What does *Minecraft*, currently the best-selling game of all time, have in common with the *Civilization* franchise, *No Man's Sky*, the indie Pico-8 game *The Explorers*, and countless other games, both large and small [@mojang2011minecraft; @microsoft2021minecraft, p. 3; @microprose1991civilization; @hellogames2016no; @devaux2022explorers]?
Each of these games has an operational logic of exploration, and depicts regions of uncharted and uninhabited land, which the player is able to discover (and perhaps claim or inhabit).
In doing so, they participate to some degree in the creation and maintenance of a mythology of "*terra nullius*" (uninhabited land) which serves a justificatory function within settler colonial societies.

Settler-colonialism is a perspective which either explicitly or implicitly justifies and valorizes colonial domination.
Especially in places where settler-colonial domination is ongoing, such as North America and Australia, justifying colonialism is an important part of societies that want to see themselves as "good" despite their ongoing manifestly unjust relationships with the original inhabitants of the land that they occupy.
Accordingly, such societies produce and consume media which serve the societal function of promoting and teaching colonialism [@kestila2017educational; @decter2021dis; @hampton2017we].
Besides textbooks, novels, and movies, games can also serve this purpose, communicating colonialist values and narratives [e.g., @garcia2017privilege; @magnet2006playing; @douglas2002you].

"*Terra nullius*" (which means "empty land") is a central part of these
narratives, but is more than just the myth (and legal doctrine) that
colonized lands were previously uninhabited or unclaimed: it is a
constellation of contradictory arguments which each attempt to justify
the unjustifiable.
When faced with evidence that the land was inhabited, the proponent of
settler colonialism must then minimize the importance of the inhabitants,
and above all must diminish the legitimacy of their claims to the land.
The historical distortions that this gives rise to form a settler colonial mythology which is then taken up in popular culture, including games, and we are interested in understanding it manifests at the level of operational logics, using *Minecraft* as our primary example.

*Minecraft* is an important game for several reasons, in part because of its broad audience (at more than 238 million copies, *Minecraft* is currently the best-selling game of all time), but particularly because it is marketed towards and popular among elementary-school-aged children [@microsoft2021minecraft, p. 3], thus potentially being part of children's media diet at an age when they are still forming core beliefs about their relationship to the society they live in and that society's relationship to others [see e.g., @levstik1994they; @spiegler2018ethnic][^1].
In fact, the colonialist messages of *Minecraft* have already been the subject of critical attention [@dooghan2016digital; @lopez2019dont]; our aim here is to complement these scholars' analyses with an in-depth examination of how *Minecraft*'s operational logics derive from and thus perpetuate the myths of "*terra nullius*."

[^1]: Note that the authors of [@levstik1994they] "...limited [their] selections to post-contact American history in order both to simplify our task and to insure that participants would have some familiarity with their content," which provides some insight into the state of historical erasure in 1994, and is the other side of a quote from [@haskie2014Hózhó] on teaching history at a Tribal College: "American history usually commences with the founding of America, as if the people and land prior to their arrival did not exist." Such erasure is still present in current curricula [@lopez2019dont; @sabzalian2021standardizing; @kestila2017educational].
