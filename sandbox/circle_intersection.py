import math     #importing math so that we can use certain functions

#The function below calculates the distance between two points, using the distance formula.
def distance(a,b):
    x1, y1 = a
    x2, y2 = b

    return(math.sqrt(((x2-x1)**2) + ((y2-y1)**2)))


#The function below checks and makes sure the triangle created is a valid triangle.
def isatriangle(x, y ,z):
    return ((x + y> z) and (y+z >x) and (z+x > y))

#The function below calculates the intersection(s) between two circles.
def circle_intersection(a, b, a_radius, b_radius):
    x1, y1 = a
    x2, y2 = b

    r1 = a_radius
    r2 = b_radius

    d = distance(a,b)

    if d > r1 + r2:
        return []  
    elif d < abs(r1 - r2):
        return [] 

    a = (r1**2 - r2**2 + d**2) / (2 * d)
    h = math.sqrt(r1**2 - a**2)

    x0 = x1 + a * (x2 - x1) / d
    y0 = y1 + a * (y2 - y1) / d

    x3 = round(x0 + h * (y2 - y1) / d)
    y3 = round(y0 - h * (x2 - x1) / d)

    x4 = round(x0 - h * (y2 - y1) / d)
    y4 = round(y0 + h * (x2 - x1) / d)

    return [(x3, y3), (x4, y4)]

#ab_distance = distance(a,b)
#bc_distance = distance(a,b)
#ca_disatnce = distance(a,b)

#The function below calculates the midpoint from a set of two tuples (coordinates).
def mid(a):
    x1, y1 = a[0]
    x2, y2 = a[1]

    return((x1+x2)/2 ,(y1+y2)/2)
    
#The function below is our main where we are first testing whether or not we have a triangle, and then calculating the circle intersections and then the midpoint.
def main(a,b,c, a_radius, b_radius, c_radius):
    if isatriangle(distance(a,b),distance(c,b),distance(a,c)) == False:
        return ("Not a Triange")

    x1,y1 = mid(circle_intersection(a, b, a_radius, b_radius))
    x2,y2 = mid(circle_intersection(c, b, c_radius, b_radius))
    x3,y3 = mid(circle_intersection(a, c, a_radius, c_radius))

    return ((round((x1+x2+x3)/3)), (round((y1+y2+y3)/3)))

#print(circle_intersection((3,5),(4,7),5,6))
#print(circle_intersection((0,0),(4,7),7,6))
#print(circle_intersection((3,5),(0,0),5,7))
print(main((0,0),(1,1),(2,2),5,6,7))
